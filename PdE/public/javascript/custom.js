function redefineSenha(action) {
  $("#modalLogin").modal("hide");
  $("#modalPass").modal("show");
}

function redefinir() {
  $.ajax({
    url:base_url + 'usuario/redefinePass' ,
    type:"POST",
    dataType:"JSON",
    data:{senhaOld:$("#modalPass").find('#senhaOld').val(), senhaNew:$("#modalPass").find('#senhaNew').val(), rsenhaNew:$("#modalPass").find("#rsenhaNew").val()},
    beforeSend:function(){

    },
    success:function(response){
      if(response.status == 'success'){
        alertify.success(response.alertify.mensagem);
        if(response.reload){
          setTimeout(function(){ location.reload(); }, 2000);
        }
      }else{
        alertify.error(response.alertify.mensagem);
        if(response.clear){
          $("#modalPass input").val("");
        }
      }
    }
  });
}

function newPlano(opt) {
  if (opt == "new") {
    $("#modalNewPlano").modal("show");
    // setTimeout(function(){ location.href=base_url+'plano/plano/new'; }, 2000);
  }else if(opt == "copy"){
    setTimeout(function(){ location.href=base_url+'plano/plano/copy'; }, 2000);
  }else if(opt == "delete"){
    setTimeout(function(){ location.href=base_url+'plano/plano/delete'; }, 2000);
  }
}

function plano(opt,id_plano) {
    if(opt == 'new'){
      sendTo(base_url+'plano/plano/new', {nome_plano:$("#modalNewPlano").find("#nomePlano").val(), id_disciplina:$("#modalNewPlano").find("#disciplina").val(), id_periodo:$("#modalNewPlano").find("#periodo").val()}, "POST");
    }
    else if(opt == 'copy'){
      alert('COPIAR PLANO');
    }
    else if (opt == 'delete') {
      alert("DELETAR PLANO");
    }else if (opt == 'editar') {
      sendTo(base_url+'plano/plano/editar',{id_plano:id_plano},"POST");
    }
}
function sendTo(url,data,type) {
  $.ajax({
    url:url,
    type:type,
    dataType:"JSON",
    data:data,
    beforeSend:function(){
      $("#modalLoading").modal("show");
    },
    success:function (ret) {
      $("#modalLoading").modal("hide");
      if(ret.status == 'success'){
        alertify.success(ret.alertify.mensagem);
        if(ret.reload){
          setTimeout(function(){ location.reload(); }, 2000);
        }
        if(ret.redirect){
          setTimeout(function(){ location.href=ret.redirect; }, 2000);
        }
      }else{
        alertify.error(ret.alertify.mensagem);
        if(ret.reload){
          setTimeout(function(){ location.reload(); }, 2000);
        }
      }
    }
  });
}
