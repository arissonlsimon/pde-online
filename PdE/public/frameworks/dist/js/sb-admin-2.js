/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
$(function() {
    $('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent();

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
});


$(function(){
    $("form").submit(function(){
        var envia = true,
            form = $(this),
            btnSend = $(this).find('button[type=submit]:enabled');

        if(form.attr('data-button-live') == "" || typeof form.attr('data-button-live') == 'undefined') {
            var btnActStatus = true;
        } else {
            var btnActStatus = false;
        }
        // btnSend.addClass('sendActivated').button('loading');

        $('input, textarea, select', this).each(function() {
            if($(this).attr('data-required') != undefined && $(this).prop('disabled') == false) {
                if(((
                    $(this).attr('type') == 'radio'
                    || $(this).attr('type') == 'checkbox')
                    && !$("input[name='"+ $(this).attr('name') +"']:checked").length)
                    || (
                        $(this).attr('type') == 'text'
                        ||$(this).attr('type') == 'file'
                        ||$(this).attr('type') == 'number'
                        ||$(this).prop('type') == 'textarea'
                        ||$(this).prop('type') == 'select-one'
                        ||$(this).prop('type') == 'password'
                        ||$(this).prop('type') == 'email'
                       )
                    && $(this).val() == ''
                ) {
                    $('.sendActivated').removeClass('sendActivated').button('reset');
                    inputError($(this), $(this).attr('data-required'));
                    envia = false;
                    return false;
                }
            }
        });
        console.log("envia : "+ envia);
        if(envia) {

            if(form.attr('data-ajax') == 'true') {
                if(form.attr('progressbar')) {
                    send(form, { progressbar : form.attr('progressbar') });
                } else {
                    send(form);
                }
                if(form.attr('data-reset') == "true") {
                    form[0].reset();
                }
            } else {
                $(this).unbind('submit');
                $(this).removeAttr('onsubmit');
                $(this).submit();

                if(btnActStatus) {
                    setTimeout(function(){ $('.sendActivated').removeClass('sendActivated').button('reset'); }, 8000);
                } else {
                    setTimeout(function(){ $('.sendActivated').removeClass('sendActivated').button('reset'); }, 2000);
                }
            }
        }

    });

});

/**
 * Mostra mensagem de erro e descata os campos
 *
 * Função utilizada para destacar campos obrigatórios que não foram preenchidos.
 *
 * @function
 * @name inputError
 * @param {Object} obj Objeto que receberá o destaque do erro.
 * @param {string} msg Mensagem de Erro.
 */
function inputError(obj, msg) {

    obj.closest('.form-group').addClass('has-error');
    obj.closest('.form-group').find('.glyphicon').removeClass('hide');
    obj.focus();
    alertify.error(msg);

    $("html, body").delay(500).animate({ scrollTop: obj.offset().top-120 }, 500);

}

function send(form, options) {
    var setup   = {},
        configs = {
            'action'      : '',
            'method'      : "POST",
            'callback'    : false,
            'progressbar' : { element : null, delay   : 2000 }
        };

    if(typeof(options) === "object") $.extend(true, configs, options);

    setup.url = configs.action;
    setup.method = configs.method;

    if(form.constructor.name == "FormData" || $(form).prop("tagName") !== undefined) {
        setup.processData = false;
        setup.contentType = false;
        setup.data = form;

        if($(form).prop("tagName") !== undefined){
            setup.method = $(form).attr("method");
            setup.url = $(form).attr("action");
            $(form).attr("enctype", "multipart/form-data");
            setup.data   =  new FormData($(form).get(0));
        }

    } else if(typeof form == "object")
        setup.data = form;
    else
        setup.data = {};

    if(!setup.url.length) {
        console.log("action is not set");
        return false;
    }




    setup.success = function(data) {
        try {
            data = JSON.parse(data);
        } catch (err) {
            json         = data;
            data         = {};
            data.message = json;

            if(json == "true")
                data.success = true;
            else
                data.success = false;
        }

        if(data.return) {
            if(data.alertfy) alertify.success(data.alertfy.mensagem);
        } else {
            if(data.alertfy) alertify.error(data.alertfy.mensagem);
        }

        if(data.redirect == "reload") {
            setTimeout(function(){ location.reload(); }, 2000);
        } else if(data.redirect == "back") {
            setTimeout(function(){ history.back(1); }, 2000);
        } else if(data.redirect) {
            setTimeout(function(){ location.href = data.redirect; }, 2000);
        }

        if(configs.callback) configs.callback.call(this, data);
        if(data.callback) data.callback;
        setTimeout(function(){ $('.sendActivated').removeClass('sendActivated').button('reset'); }, 8000);
    };

    if(configs.progressbar != null) {
        $(configs.progressbar).css('display', 'block').find(".progress").slideDown();

        setup.xhr = function() {
            var xhr = new window.XMLHttpRequest();

            try {
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable)
                        $(configs.progressbar).find(".progress-bar").css("width", Math.round(evt.loaded/evt.total*100)+"%");
                }, false);

                xhr.onload = function (ev) {
                    $(configs.progressbar).find(".progress").delay(2000).slideUp(function() {
                        $(configs.progressbar).find(".progress-bar").css("width", "0");
                    });
                };

                return xhr;
            }
            catch(err) {}
        };
    }

    setup.error = function(data) {
        $('.sendActivated').removeClass('sendActivated').button('reset');
        console.log(data);
    };

    try {
        $.ajax(setup);
    } catch(err) {
        console.log(err);
    }
}
