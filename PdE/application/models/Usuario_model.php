<?php
	class Usuario_model extends CI_Model {

		function __construct() {

		}

		public function listar()
		{
			$usuarios = $this-> db -> select('u.id_usuario,u.nome,u.email,u.status,r.nome_regra')
				-> FROM('usuario as u') 
				-> join('regra_usuario as ru','u.id_usuario = ru.fk_usuario_id_usuario')
				-> join('regra as r','ru.fk_regra_id_regra=r.id_regra')
				-> get() -> result_array();
			if($usuarios){
				$retorno['dados'] = $usuarios;
			}else{
				$retorno['dados'] = 'Não possui usuários cadastrados ainda';
			}

			$retorno['msg'] = 'Pesquisa Concluida';
			$retorno['reload'] = false;
			$retorno['flag'] = true;

			return $retorno;
		}

		public function cadastra($POST)
		{
			if (!empty($POST)) {
				try {
					$this -> db -> insert('usuario',array('nome'=>$POST['nome'],'email'=>$POST['email'],'status'=>$POST['status'], 'senha'=>md5('ifrsosorio')));
					$this -> db -> insert('regra_usuario',array('fk_usuario_id_usuario'=>$this->db->insert_id(),'fk_regra_id_regra'=>$POST['regra']));
					$retorno['alertify']['mensagem'] = 'Usua
					rio cadastrado com sucesso';
					$retorno['redirect'] = 'reload';
					$retorno['return'] = true;
				
				} catch (Exception $e) {
					$retorno['alertify']['mensagem'] = 'Erro ao cadastrar o usuário.';
					$retorno['return'] = false;
				}

			}else{
				$retorno['alertify']['mensagem'] = 'Erro ao processar solicitação de Cadastro de usuário.';
				$retorno['return'] = false;
			}
			return $retorno;
		}
		
		public function alterar($POST){
			if (!empty($POST)) {
				try {
					$this -> db ->where('id_usuario',$POST['id_usuario']) -> update('usuario',array('nome'=>$POST['nome'],'email'=>$POST['email'],'status'=>$POST['status']));
					$this -> db ->where('fk_usuario_id_usuario',$POST['id_usuario'])-> update('regra_usuario',array('fk_regra_id_regra'=>$POST['regra']));
					$retorno['alertify']['mensagem'] = 'Usuário alterado com sucesso';
					$retorno['redirect'] = 'reload';
					$retorno['return'] = true;
				
				} catch (Exception $e) {
					$retorno['alertify']['mensagem'] = 'Erro ao alterar o usuário.';
					$retorno['return'] = false;
				}

			}else{
				$retorno['alertify']['mensagem'] = 'Erro ao processar solicitação de alteração de usuário.';
				$retorno['return'] = false;
			}
			return $retorno;
		}

		public function alterarStatus($POST)
		{
			if(!empty($POST)){
				if($POST['statusAtual'] == 'Ativo'){
					try {
						$this -> db -> update('usuario', array('status'=>'I'), array('id_usuario'=>$POST['idUsuario']));
						$retorno['alertify']['mensagem'] = 'Status trocado com sucesso';
						$retorno['redirect'] = 'reload';
						$retorno['return'] = true;
					} catch (Exception $e) {
						$retorno['alertify']['mensagem'] = 'Erro ao trocar de status.';
						$retorno['return'] = false;
					}
				}else{
					try {
						$this -> db -> update('usuario', array('status'=>'A'), array('id_usuario'=>$POST['idUsuario']));
						$retorno['alertify']['mensagem'] = 'Status trocado com sucesso';
						$retorno['redirect'] = 'reload';
						$retorno['return'] = true;
					} catch (Exception $e) {
						$retorno['alertify']['mensagem'] = 'Erro ao trocar de status.';
						$retorno['return'] = false;
					}
				}
			}else{
				$retorno['alertify']['mensagem'] = 'Erro ao processar solicitação de troca de status.';
				$retorno['return'] = false;
			}

			return $retorno;
		}


		public function listarRegras()
		{
			$regras = $this -> db -> get("regra") ->result_array();
			if($regras){
				$retorno['regras'] = $regras;
			}else{
				$retorno['regras'] = "Não foi encontrado nenhuma regra";
			}

			return $retorno;
		}


		public function cadastraRegra($POST)
		{
			if (!empty($POST)) {
				try {
					$this -> db -> insert('regra',array("nome_regra"=>$POST['nome'], "descricao"=>$POST['descricao']));
					$this-> load -> model("functions/Functions_model","fn");
					$this -> fn -> logs('ins',$this -> session -> id_usuario,'Cadastro de nova regra '.$POST['nome'].' feito por '.$this -> session -> id_usuario);
					$retorno['alertify']['mensagem'] = 'Regra cadastrada com sucesso';
					$retorno['redirect'] = 'reload';
					$retorno['return'] = true;
				} catch (Exception $e) {
					$retorno['alertify']['mensagem'] = 'Erro ao cadastrar regra.';
					$retorno['return'] = false;
				}
			}else{
				$retorno['alertify']['mensagem'] = 'Erro ao processar solicitação de cadastro de regra.';
				$retorno['return'] = false;
			}
			return($retorno);
		}


		public function removeRegra($post)
		{
			if (!empty($post)) {
				try {
					$this -> db -> where('id_regra',$post['id_regra']) -> delete('regra');
					$this-> load -> model("functions/Functions_model","fn");
					$this -> fn -> logs('ins',$this -> session -> id_usuario,'Cadastro de nova regra '.$post['id_regra'].' feito por '.$this -> session -> id_usuario);
					$retorno['alertify']['mensagem'] = 'Regra removida!!';
					$retorno['status'] = 'success';
					$retorno['reload'] = true;
				} catch (Exception $e) {
					$retorno['alertify']['mensagem'] = 'Erro ao remover regra.';
					$retorno['return'] = false;
				}
			} else {
				$retorno['alertify']['mensagem'] = 'Erro ao processar solicitação de exclusão de regra';
				$retorno['return'] = false;
			}
			return $retorno;
		}


		public function login($POST)
		{
			if ($dados = $this -> db -> where("email",$POST['email'])->get('usuario')-> row_array())  {
				if (($POST['senha'] == ('ifrsosorio')) && ($dados['senha'] == md5('ifrsosorio')))
				{
					$retorno['redirect'] = 'rSenhaNew';
					// unset($_SESSION);
					// $_SESSION["usuario"]['id_usuario'] = $dados['id_usuario'];

					$this -> session -> set_userdata("id_usuario",$dados['id_usuario']);
					$this-> load -> model("functions/Functions_model","fn");
					$this -> fn -> logs('sel',$dados['id_usuario'],'Primeiro acesso do '.$dados['nome'].' realizado.');
				}else{
					if ($dados['senha'] == md5($POST['senha'])) {
						$regras = $this -> db -> select("r.id_regra,r.nome_regra") -> from("regra_usuario as ru") -> join("regra as r","ru.fk_regra_id_regra = r.id_regra") -> where("ru.fk_usuario_id_usuario",$dados['id_usuario'])->get()->result_array();
						$todasRegras = array();
						if ($regras) {
							foreach ($regras as $key => $value) {
								$todasRegras[$value['id_regra']] = $value['nome_regra'];
							}
							$this -> session -> unset_userdata(array('nome', 'id_usuario', 'nome_regra'));
							$this -> session -> set_userdata("nome",$dados['nome']);
							$this -> session -> set_userdata("id_usuario",$dados['id_usuario']);
							$this -> session -> set_userdata("nome_regra",$todasRegras);
							
							$retorno['alertify']['mensagem'] = 'Login efetuado com sucesso.';
							$retorno['status'] = 'success';
							// $retorno['reload'] = true;
							$retorno['redirect'] = base_url().'avaliacoes';
							$this-> load -> model("functions/Functions_model","fn");
							$this -> fn -> logs('sel',$this -> session -> id_usuario,'Login no sistema feito por '.$this->session->nome);

						}else{
							$retorno['alertify']['mensagem'] = 'ERROR ao efetuar login! Consulte o desenvolvedor.';
							$retorno['status'] = 'error';
						}
					}else{
						$retorno['alertify']['mensagem'] = 'EMAIL ou SENHA não conferem!';
						$retorno['status'] = 'error';
					}
				}
			}else{
				$retorno['alertify']['mensagem'] = 'EMAIL ou SENHA não conferem!!';
				$retorno['status'] = 'error';
			}
			;


			return $retorno;

		}


	public function redefinePass($post)
	{
		// echo $this -> session -> userdata("id_usuario")."#OC#";
		// if(!empty($this -> session -> userdata("id_usuario")))
		// 	die('oka');
		// else {
		// 	die('fudeu');
		// }
		if(empty($post)){
			$retorno['alertify']['mensagem'] = 'Error senha não enviada para troca!.';
			$retorno['status'] = 'error';
		}else{
			if ($post['senhaOld'] === ('ifrsosorio')) {
				if ($post['senhaNew'] === $post['rsenhaNew']) {
					if (empty($this -> session -> userdata("id_usuario"))) {
						$retorno['alertify']['mensagem'] = 'Erro (00) ao trocar senha! Consulte o desenvolvedor!';
						$retorno['status'] = 'error';
					}else {
						if(
								$this -> db -> set('senha',md5($post['senhaNew'])) -> where('id_usuario',$this -> session -> userdata("id_usuario")) -> update('usuario')
							)
							{
								$retorno['alertify']['mensagem'] = 'Senha trocada com sucesso!';
								$retorno['status'] = 'success';
								$retorno['reload'] = true;

							}else{
								$retorno['alertify']['mensagem'] = 'Erro (01) ao trocar senha! Consulte o desenvolvedor!';
								$retorno['status'] = 'error';
							}

					}
				}else{
					$retorno['alertify']['mensagem'] = 'Senhas não conferem!';
					$retorno['status'] = 'error';
					$retorno['clear'] = true;
				}
			}else{
				$retorno['alertify']['mensagem'] = 'Senha antiga não confere!';
				$retorno['status'] = 'error';
				$retorno['clear'] = true;
			}
		}
		return $retorno;
	}

	public function logout()
	{
		$this -> session -> sess_destroy();
		return "index";
	}

	/**
	* Funcao para realizar operacoes de busca de usuarios em geral
	*
	* @param string $tipo - Valor que sera usado no case para diferenciar o tipo de execucao da funcao
	* @param int $POST['busca'] - (opcional vairando o tipo informado) informe o valor da busca na tabela usuarios podendo ser nome, email, ou codigo do usuario
	* @return string JSON - Em caso de falha retornara um string json com false
	*/

	public function busca($POST, $tipo = null) {

		switch($tipo) {

			case 'tokenInput':

				if(!empty($POST['busca'])) {
					$POST = $POST['busca'];
				} else {
					$POST = $POST;
				}

				if(is_numeric($POST)) {
					$this -> db -> select('*') -> from('usuario') -> where_in('id_usuario', $POST);
				} else {
					$this -> db -> select('*') -> from('usuario') -> where("(nome LIKE '%".$POST."%' OR (email LIKE '%".$POST."%'))") -> limit(10);
				}

				$this -> db -> where('status', 'A');
				
				$query_usuario = $this -> db -> get();
				
				// < Verifica se existem usuario
				if($query_usuario -> num_rows() > 0) {
					// < Percorre usuario
					foreach($query_usuario -> result_array() as $usuario) {
						$retorno[] = $usuario;
					}
					// > Percorre usuario
				} else {
					$retorno = false;
				}
				// > Verifica se existem usuario

				return(json_encode($retorno));

			break;

			default:

				if(!empty($POST['busca'])) {
					$POST = @$POST['busca'];
				} else {
					$POST = $POST;
				}

				$this -> db -> select('*') -> from('usuario') -> where('status', 'A');

				if(is_array($POST) || is_numeric($POST)) {
					$this -> db -> where_in('id_usuario', $POST);
				} else {
					$this -> db -> where("nome LIKE '%".$POST."%' OR (email LIKE '%".$POST."%')");
				}
				
				$query_usuario = $this -> db -> get();

				// < Verifica se existem usuario
				if($query_usuario -> num_rows() > 0) {
					// < Percorre usuario
					foreach($query_usuario -> result_array() as $usuario) {
						$retorno[$usuario['id_usuario']] = $usuario;
					}
					// > Percorre usuario
				} else {
					$retorno = false;
				}
				// > Verifica se existem usuario

				return($retorno);

			break;

		}
			
	}

	public function getPerfil()	{
		$dados_session = $this->session->userdata();

		$dados = array(
			'user_info' 	=> $this->db->select('u.nome,u.email,u.status')->from('usuario as u')->where('u.id_usuario',$dados_session['id_usuario'])->get()->row_array()
			,'disciplina_info'	=> $this->db->select('d.nome_disciplina,d.cargahoraria_disciplina')->from('disciplina as d')->join('disciplina_usuario as du','du.fk_disciplina_id_disciplina = d.id_disciplina')->where('du.fk_usuario_id_usuario',$dados_session['id_usuario'])->get()->result_array()
			,'regra_info'	=>	$this->db->select('r.nome_regra,r.descricao')->from('regra as r')->join('regra_usuario as ru','ru.fk_regra_id_regra = r.id_regra')->where('ru.fk_usuario_id_usuario',$dados_session['id_usuario'])->get()->row_array()
			,'curso_info' => $this->db->select('c.nome_curso,c.categoria_curso')->from('curso as c')->where('c.coord_curso',$dados_session['id_usuario'])->get()->result_array()
		);

		return $dados;
		
	}


}
