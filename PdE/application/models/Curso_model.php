<?php
	class Curso_model extends CI_Model {

		function __construct() {
			//@session_start();
		}

		public function listar()
		{
			$cursos = $this-> db -> select('c.id_curso,c.nome_curso,c.codigo_curso,c.categoria_curso,u.nome') -> FROM('curso as c') -> join('usuario as u','c.coord_curso = u.id_usuario') -> order_by("c.id_curso",'ASC') -> get() -> result_array();
			if($cursos){
				$retorno['dados'] = $cursos;
			}else{
				$retorno['dados'] = 'Não possui cursos cadasrados ainda';
			}

			$retorno['msg'] = 'Pesquisa Concluida';
			$retorno['reload'] = false;
			$retorno['flag'] = true;

			return $retorno;
		}

		public function getCursos($var = null)
		{
			if(empty($var)){
				return $this -> db -> get("curso") -> result_array();
			}else{
				// die("B : ".$var);
				if (is_numeric($var)) {
					$retorno['curso'] = $this -> db -> where('id_curso',$var)->get('curso')->row_array();
					if(!$retorno['curso']){
						$retorno['curso'] = "Não pode achar o curso";
					}else{
						$retorno['curso']['coord_curso'] = $this->db->where('id_usuario',$retorno['curso']['coord_curso'])->get('usuario')->row_array();
					}
					return $retorno;
				}else{
					switch ($var) {
						case 'esm':
						$cursos = $this-> db -> select('c.id_curso,c.nome_curso,c.categoria_curso,u.nome') -> FROM('curso as c') -> join('usuario as u','c.coord_curso = u.id_usuario') ->  where('categoria_curso','Ensino Médio') -> get() -> result_array();
						foreach($cursos as $key => $val){
							$cursos[$key]['disciplinas'] = $this -> db 
								-> where('cd.fk_curso_id_curso',$val['id_curso']) 
								-> from('curso_disciplina as cd') 
								-> join('disciplina as d','d.id_disciplina = cd.fk_disciplina_id_disciplina	')
								-> get() 
								-> result_array();
						}
						$retorno['dados'] = $cursos;
							if(!$retorno['dados']){
								$retorno['dados'] = "Não há cursos";
							}
							return $retorno;
							break;
						case 'gra':
							$cursos = $this-> db 
								-> select('c.id_curso,c.nome_curso,c.categoria_curso,u.nome') 
								-> FROM('curso as c') 
								-> join('usuario as u','c.coord_curso = u.id_usuario') 
								-> where('categoria_curso','Graduação') 
								-> get() 
								-> result_array();
							foreach($cursos as $key => $val){
								$cursos[$key]['disciplinas'] = $this -> db 
									-> where('cd.fk_curso_id_curso',$val['id_curso']) 
									-> from('curso_disciplina as cd') 
									-> join('disciplina as d','d.id_disciplina = cd.fk_disciplina_id_disciplina	')
									-> get() 
									-> result_array();
							}
							$retorno['dados'] = $cursos;
							if(!$retorno['dados']){
								$retorno['dados'] = "Não há cursos";
							}
							return $retorno;
							break;
						default:
							header("Location:".base_url());
							break;
					}
				}
			}
		}


		/*
			Cadastra um novo curso 
			@param array com as informações
			@return array info
		*/
		public function cadastra_curso($post)
		{	

			if ($this->db->insert('curso',array('nome_curso' => $post['nome_curso'], 'codigo_curso' => $post['codigo_curso'], 'categoria_curso' => $post['categoria_curso'], 'coord_curso' => $post['coord_curso'], 'data_criacao_curso' =>  time() ))) {
				$ret['status'] = 'success';
				$ret['alertify']['mensagem'] = 'Curso cadastrado !!';
				$ret['reload'] = true;
				$ret['redirect'] = false;
			}else{
				$ret['alertify']['mensagem'] = 'Erro ao cadastrar o curso. Verifique a consistencia dos dados.';
				$ret['status'] = 'error';
				$ret['reload'] = false;
				$ret['redirect'] = false;
			}
			return $ret;
			// var_dump($ret);
			// die();
		}

		/**
		*
		*	REMOVE O CURSO
		*/
		public function removeCurso($id_curso)
		{
			if($this -> db -> where('id_curso',$id_curso) -> delete('curso')){
				$ret['status'] = 'success';
				$ret['alertify']['mensagem'] = 'Curso cadastrado !!';
				$ret['reload'] = true;
				$ret['redirect'] = false;
			}else{
				$ret['alertify']['mensagem'] = 'Erro ao cadastrar o curso. Verifique a consistencia dos dados.';
				$ret['status'] = 'error';
				$ret['reload'] = false;
				$ret['redirect'] = false;
			}
			return $ret;
		}


		/**
		* Buscar cursos
		*
		* Chamada deste metodo realiza as devidas consultas no banco para retornar todos as
		* informações do curso solicitado
		*
		* @access public
		* @return array em JSON 
		* @param $POST = {array}, $tipo = 'tokenInput'
		*/

		public function busca($POST, $tipo = null) {
			switch($tipo) {
				case 'tokenInput':
					// < Token input					
					$this -> db -> select('*') -> from('curso') -> where("codigo_curso = '".$POST['busca']."' OR (nome_curso ILIKE '%".$POST['busca']."%')") -> limit(10);
					$query_curso = $this -> db -> get();

					// < Verifica se existem usuarios
					if($query_curso -> num_rows() > 0) {
						foreach($query_curso -> result_array() as $curso) {
							$cursos[] = $curso;
						}
					}
					// > Verifica se existem usuarios
					return(@json_encode($cursos));
					// > Token input
				break;

				default:

					// < Lista padrao
					$this -> db -> select('*') -> from('cursos') -> where("codigo_curso = '".$POST['busca']."' OR (nome_curso LIKE '%".$POST['busca']."%')") -> limit(10);
					$query_curso = $this -> db -> get();

					// < Verifica se existem usuarios
					if($query_curso -> num_rows() > 0) {
						foreach($query_curso -> result() as $curso) {
							$cursos[] = $curso;
						}
					}

					return(@json_encode($cursos));
					// > Lista padrao

				break;

			}
				
		}


		public function alterar($post)
		{
			$id_curso = $post['id_curso'];
			unset($post['id_curso']);
			if ($this -> db -> update('curso',$post,array('id_curso'=> $id_curso))) {
				$ret['status'] = 'success';
				$ret['alertify']['mensagem'] = 'Curso Alterado !!';
				$ret['reload'] = false;
				$ret['redirect'] = base_url()."curso/lista";
			}else{
				die();
				$ret['alertify']['mensagem'] = 'Erro ao alterar o curso. Verifique a consistencia dos dados.';
				$ret['status'] = 'error';
				$ret['reload'] = false;
				$ret['redirect'] = false;
			}
			return $ret;
		}




	}	
