<?php
	class Datas_model extends CI_Model {

		function __construct() {

    }

    public function getPeriodos()
    {
      $dados = $this -> db -> get("periodo") -> result_array();
      foreach ($dados as $key => $value) {
        $dados[$key]['data_inicio_periodo'] = date('d/m/Y',($value['data_inicio_periodo']));
        $dados[$key]['data_final_periodo'] = date('d/m/Y',($value['data_final_periodo']));
      }
      return $dados;
    }

    public function getPeriodo($id_periodo)
    {
      return $this -> db -> where('id_periodo',$id_periodo) -> get('periodo') -> row_array();
    }

    public function cadastraPeriodo($post)
    {
      $controle = false;
      $post['data_inicio_periodo'] = strtotime($this->functions->date_converter($post['data_inicio_periodo']));
      $post['data_final_periodo'] = strtotime($this->functions->date_converter($post['data_final_periodo']));
      if($this -> db -> insert('periodo',$post)){
        $id_periodo = $this -> db -> insert_id();
        for ($i=0; strtotime(' +'.$i.' days',$post['data_inicio_periodo']) <= ($post['data_final_periodo']) ; $i++) { 
          $dataConsecutiva = strtotime(' +'.$i.' days',$post['data_inicio_periodo']);
          if((int)date('N',$dataConsecutiva) == 6){
            $dados = array('id_periodo' => $id_periodo, 'dia_semana_calendario' => 6, 'observacao'=> 'sabado', 'data_calendario' => $dataConsecutiva); 
            if($this -> db -> insert('calendario',$dados)){$controle = true;}
          }
        }
        if($controle){
          $data['alertfy']['mensagem'] = 'Período cadastrado';
          $data['redirect'] = "reload";
          $data['return'] = true;      
        }
      }else{
        $data['alertfy']['mensagem'] = 'Erro no cadastrado do periodo';
        $data['redirect'] = "reload";
        $data['return'] = false;      
      }
      return $data;
    }

    public function showDatas($idPeriodo)
    {
      $periodo = $this -> db -> where('id_periodo',$idPeriodo['idPeriodo']) -> get('periodo') -> row_array();

      for ($i=0; strtotime(' +'.$i.' days',$periodo['data_inicio_periodo']) <= ($periodo['data_final_periodo']) ; $i++) { 
        $dataConsecutiva = strtotime(' +'.$i.' days',$periodo['data_inicio_periodo']);
        $dates["datas"][] = array(
          'dia_semana' => (int)date('N',$dataConsecutiva),
          "dia_mes" => $this -> functions -> trducaoMes(date('m',$dataConsecutiva)),
          "data_integral" => date('d/m/Y',$dataConsecutiva),
          "data_feriado" => $this -> isFeriado($idPeriodo['idPeriodo'],$dataConsecutiva)
        );
        $dates['datas']['nome'] = $periodo['nome_periodo'];
        $dates['datas']['id_periodo'] = $periodo['id_periodo'];
      }
      // $this -> functions -> pre($dates,true);
      return $dates;
    }
  
    public function isFeriado($idPeriodo,$data)
    {
      if (!empty($idPeriodo) && !empty($data)) {
        if($this -> db -> where('id_periodo',$idPeriodo) -> where('data_calendario',$data) -> get('calendario') -> num_rows()){
          return 'true';
        }else{
          return 'false';
        }
      }else{
        return 'false';
      }
    }

    public function feriado($action,$post)
    {
      switch ($action) {
        case 'add':
          if ($this -> db -> insert('calendario',
              array(
                'id_periodo' => $post['id_periodo'],
                'data_calendario' => strtotime($this->functions->date_converter($post['data_calendario'])),
                'dia_semana_calendario' => date('N',strtotime($this->functions->date_converter($post['data_calendario'])))
              ))
          ){
            $ret['status'] = 'success';
            $ret['alertify']['mensagem'] = 'Feriado Inserido!!';
            $ret['reload'] = false;
            $ret['redirect'] = false;
          }else{
            die();
            $ret['alertify']['mensagem'] = 'Erro ao inserir o feriado.';
            $ret['status'] = 'error';
            $ret['reload'] = false;
            $ret['redirect'] = false;
          }
          return $ret;
          break;
        case 'remove':
          if ($this -> db -> where('id_periodo',$post['id_periodo']) -> where('data_calendario',strtotime($this->functions->date_converter($post['data_calendario']))) -> delete('calendario')
          ) {
            $ret['status'] = 'success';
            $ret['alertify']['mensagem'] = 'Feriado Removido!!';
            $ret['reload'] = false;
            $ret['redirect'] = false;
          }else{
            die();
            $ret['alertify']['mensagem'] = 'Erro ao remover o feriado.';
            $ret['status'] = 'error';
            $ret['reload'] = false;
            $ret['redirect'] = false;
          }
          return $ret;
          break;
        default:
          # code...
          break;
      }
    }

  public function changeDatas($post)
  {
    $dates = array();
    $dados = $this -> db -> where('id_periodo',$post['id_periodo']) -> get('periodo') -> row_array();
    //die(print_r($dados));
    for ($i=0; strtotime(' +'.$i.' days',$dados['data_inicio_periodo']) <= ($dados['data_final_periodo']); $i++)
    { 
      $dataConsecutiva = strtotime(' +'.$i.' days',$dados['data_inicio_periodo']);
      $dataFeriado = $this -> isFeriado($post['id_periodo'],$dataConsecutiva);
      if(date('N',$dataConsecutiva) == $post['dia_semana']){
        if ($dataFeriado == 'false') {
          $dates[] = (  date('d/m/Y',$dataConsecutiva));
        }
      }
    }
    return $dates;
  }

  /*
    Função para remoção do periodo
  */
  public function deletarPeriodo($id_periodo){
    if($this->db->where('id_periodo',$id_periodo)->get('plano')->num_rows() == 0){
      if($this->db->where('id_periodo',$id_periodo)->delete('calendario')){
        if($this -> db -> where('id_periodo',$id_periodo) -> delete("periodo")){
          $ret['alertify']['mensagem'] = 'Período removído!!!';
          $ret['status'] = 'success';
          $ret['reload'] = true;
          $ret['redirect'] = false;
        }else{
          $ret['alertify']['mensagem'] = 'Erro ao remover o período';
          $ret['status'] = 'error';
          $ret['reload'] = false;
          $rer['redirect'] = false;
        }
      }else{
        $ret['alertify']['mensagem'] = 'Erro ao remover o período';
        $ret['status'] = 'error';
        $ret['reload'] = false;
        $rer['redirect'] = false;
      }
    }else{
      $ret['alertify']['mensagem'] = 'Não é possível remover o período pois existem planos vinculados a ele!!';
      $ret['status'] = 'error';
      $ret['reload'] = false;
      $rer['redirect'] = false;
    }
    return $ret;
  }

  public function altera($post){
    $controle = false;
      $post['data_inicio_periodo'] = strtotime($this->functions->date_converter($post['data_inicio_periodo']));
      $post['data_final_periodo'] = strtotime($this->functions->date_converter($post['data_final_periodo']));
      $id = $post['id_periodo'];
      unset($post['id_periodo']);
      if($this -> db ->where('id_periodo',$id) -> update('periodo',$post)){
          $data['alertfy']['mensagem'] = 'Período alterado';
          $data['redirect'] = "reload";
          $data['return'] = true;      
        
      }else{
        $data['alertfy']['mensagem'] = 'Erro no alterar o periodo';
        $data['redirect'] = "reload";
        $data['return'] = false;      
      }
      return $data;
  }

} # Fim do model Datas_model
