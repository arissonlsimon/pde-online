<?php
class Plano_model extends CI_Model {

	function __construct() {
		//@session_start();
	}

	public function listar()
	{
		if($this -> session -> has_userdata("id_usuario")){
			$this-> db -> select('p.id_plano, p.revisao, p.nome_plano, d.nome_disciplina, p.publicado_plano, p.data_criacao_plano, p.avaliado_coord, p.avaliado_pedagogico, p.data_alteracao_plano, d.nome_disciplina, m.nome_modelo, p.locked, p.submit_plano') ;
			$this -> db -> FROM('plano as p') ;
			$this -> db -> join('disciplina as d','p.id_disciplina = d.id_disciplina');
			$this -> db -> join('modelo as m','p.id_modelo = m.id_modelo')
			->join('curso as c','p.id_curso = c.id_curso');
			if($this->functions->checkPermissao(array('Professor'),$this->session->nome_regra)){
				$this -> db -> where('id_usuario',$this -> session -> userdata("id_usuario")); 
			}
			if(in_array('Coordenador',$this -> session -> userdata('nome_regra'))){
				$this ->db->where('c.coord_curso',$this->session->id_usuario);
			}
			
			$planos = $this -> db -> order_by('p.id_plano','ASC') -> get() -> result_array();
			
			if($planos){
				$retorno['dados'] = $planos;
			}else{
				$retorno['dados'] = 'Não Possui Planos relacionados com o usuário';
			}
			$retorno['msg'] = 'Pesquisa Concluida';
			$retorno['reload'] = false;
			$retorno['flag'] = true;
		}else{
			$retorno['msg'] = 'Erro! Usuário inesistente';
			$retorno['reload'] = false;
			$retorno['flag'] = false;
		}

		return $retorno;
	}

	public function novoPlano($post)
	{
		if ($this -> session -> has_userdata("id_usuario")) {
			$data = array();
			$data['id_usuario'] = $this ->  session -> id_usuario;
			$data['id_disciplina'] = $post['disciplina'];
			$data['id_periodo'] = $post['periodo'];
			$data['nome_plano'] = $post['nome_plano'];
			$data['data_criacao_plano'] = time();
			if($this -> getModeloAtual('id_modelo',$post['categoria'])){
				$data['id_modelo'] = $this -> getModeloAtual('id_modelo',$post['categoria']);
			}else{
				$retorno['status'] = 'error';
				$retorno['alertify']['mensagem'] = "Erro ao criar novo plano! Modelo não encontrado!!";
				$this -> session -> set_userdata('erro_cria_plano','Não foi possível criar o novo plano, pois não existem modelos cadastrados para a categoria selecionada!<br/>Entre em contato com o administrador para cadastrar um modelo.');
				return $retorno;
			}
			$data['id_curso'] = $post['curso'];
			if($this -> db -> insert("plano",$data)){
			// if(true){
				$retorno['status'] = 'success';
				$retorno['alertify']['mensagem'] = "Plano novo criado! ";
				$retorno['redirect'] = base_url("/plano/EditarPlano/".$this -> db -> insert_id());
			}else{
				$retorno['status'] = 'error';
				$retorno['alertify']['mensagem'] = "Erro ao criar novo plano! ";
				$this -> session -> set_userdata('erro_cria_plano','Erro ao criar um novo plano');
			}
			return $retorno;
		}
	}

	/*
		Copia Plano par um novo
		input dados _POST
		output array
	*/
	public function copiaPlano($dados){
		// $this -> functions -> pre($dados,true);
		$plano_old = $this -> db -> where('id_plano',$dados['plano_old']) -> get('plano') -> row_array();
		$array_novo_plano = array();
		$array_novo_plano = $plano_old;
		unset($array_novo_plano['id_plano']);
		$array_novo_plano['nome_plano'] = $dados['nome_plano'];
		$array_novo_plano['id_disciplina'] = $dados['disciplina'];
		$array_novo_plano['id_modelo'] = $this -> getModeloAtual('id_modelo',$dados['categoria']);
		$array_novo_plano['id_periodo'] = $dados['periodo'];
		$array_novo_plano['data_criacao_plano'] = time();
		unset($array_novo_plano['data_alteracao_plano']);
		$array_novo_plano['publicado_plano'] = 0;
		$array_novo_plano['avaliado_coord'] = 0;
		$array_novo_plano['avaliado_pedagogico'] = 0;
		$array_novo_plano['submit_plano'] = 0;
		$array_novo_plano['locked'] = 0;
		$array_novo_plano['revisao'] = 0;
		$array_novo_plano['id_curso'] = $dados['curso'];
		unset($array_novo_plano['observacao']);
		$arr_aux = json_decode($plano_old['dados_plano']);
		$arr_aux_modelo = json_decode($this -> getModeloAtual('ar_configuracao',$dados['categoria']));
		$arr_key_oldPlano = array();
		$arr_key_modeloOn = array();
		// $this -> functions -> pre($value);
		foreach ($arr_aux_modelo as $key => $value) {
			// array_push($arr_key_modeloOn ,$value->iddiv);
			if(array_key_exists($key,($arr_aux))){
				if($key != "cronograma"){
					$arr_aux_modelo->$key = $arr_aux->$key;
				}else{
					$this -> load -> model('Datas_model');
					$get_datas = $this -> Datas_model -> showDatas(array('idPeriodo' => $dados['periodo']));
					$int_dia_semana = $this -> functions -> changeDayWeekInt($dados['diasemana']);
					// $this -> functions -> pre($int_dia_semana);
					unset($get_datas['datas']['nome']);
					unset($get_datas['datas']['id_periodo']);
					$ar_aux_datas = array();
					foreach ($get_datas['datas'] as $datas) {
						if($datas['dia_semana'] == $int_dia_semana && $datas['data_feriado'] == 'false'){
							for ($i=0; $i < $value -> colnumber; $i++) { 
								if($i == 0){
									$xxx[$i] = $datas['data_integral'];
								}else{
									$xxx[$i] = '';
								}
							}
							$ar_aux_datas[] = $xxx;
							$arr_aux->$key->dados->val_row = $ar_aux_datas;
							$arr_aux_modelo -> $key = $arr_aux -> $key;
						}
					}
					// $this -> functions -> pre($ar_aux_datas);
					// die('sss');			
				}
			}else{
				if($value->tipo == 'tabela'){
					$arr_aux_modelo->$key->dados->nome_col = $value->coltitle;
					unset($arr_aux_modelo->$key->coltitle);
				}
				// $this->functions->pre($arr_aux_modelo,true);
			}
		}
		// die('fim');

		$array_novo_plano['dados_plano'] = json_encode($arr_aux_modelo);
		if($this->db->insert('plano',$array_novo_plano)){
			$retorno['status'] = 'success';
			$retorno['alertify']['mensagem'] = "Plano novo criado! ";
			$retorno['redirect'] = base_url("/plano/EditarPlano/".$this -> db -> insert_id());
		}else{
			$retorno['status'] = 'error';
			$retorno['alertify']['mensagem'] = "Erro ao criar novo plano! ";
			$this -> session -> set_userdata('erro_cria_plano','Erro ao criar um novo plano');
		}
		return $retorno;
	}



	public function getPlano($plano)
	{
		$dado_plano = $this -> db -> where('id_plano',$plano)->get('plano')->row_array();
		return $dado_plano;
	}

	public function listaModelos($idModelo=null)
	{
		if(!$idModelo){
			if($modelos = $this -> db -> query("select m.*,count(p.id_modelo) as num_plano from modelo m left join plano p on m.id_modelo = p.id_modelo group by p.id_modelo,m.id_modelo order by id_modelo") -> result_array()){
				return ($modelos);
			}else{
				return "Não existe modelos ainda!";
			}
		}else{
			if($modelos = $this -> db -> query("select m.*,count(p.id_modelo) as num_plano from modelo m left join plano p on m.id_modelo = p.id_modelo where m.id_modelo = ".$idModelo." group by p.id_modelo,m.id_modelo") -> result_array()){
				return ($modelos);
			}else{
				return "Não existe modelos ainda!";
			}
		}
	}

	public function removeModelo($idModelo){
		if($this->db->where('id_modelo',$idModelo)->get('plano')->num_rows()>0){
			$retorno['status'] = 'error';
			$retorno['alertify']['mensagem'] = "Erro!! Existem planos cadastrados com esse plano ";
		}else{
			try {
				$this->db->where('id_modelo',$idModelo)->delete('modelo');
				$retorno['status'] = 'success';
				$retorno['alertify']['mensagem'] = "Modelo removido! ";
				$retorno['reload'] = true;
			} catch (\Throwable $th) {
				$retorno['status'] = 'error';
				$retorno['alertify']['mensagem'] = "Erro ao criar novo plano! ";
			}
		}
		return $retorno;
	}

	public function cadastraModelo($post)
	{
		$dados = $post;
		$dados['ar_configuracao'] = json_encode($post['ar_configuracao']);
		$dados['data_criacao'] = time();
		if($this -> db -> insert('modelo',$dados)){
			$retorno['status'] = 'success';
			$retorno['alertify']['mensagem'] = "Modelo criado! ";
			$retorno['redirect'] = base_url("/plano/modelo");
			$this-> load -> model("functions/Functions_model","fn");
			$this -> fn -> logs('ins',$this -> session -> id_usuario,'Cadastra modelo de plano '.$this->db->insert_id().' feita por '.$this->session->nome);
		}else{
			$retorno['status'] = 'error';
			$retorno['alertify']['mensagem'] = "Erro ao criar novo plano! ";
		}
		return $retorno;
	}

	public function extracModelo($id_plano)
	{
		$id_modelo = $this -> db -> select('id_modelo') -> where('id_plano',$id_plano) -> get('plano') ->row_array();
		$modelo = $this -> db -> where('id_modelo',$id_modelo['id_modelo']) -> get('modelo') -> row_array();
		$modelo['ar_configuracao'] = json_decode($modelo['ar_configuracao']);
		return $modelo;
	}

	public function getModelo($idModelo)
	{		
		$modelo = $this -> db -> where('id_modelo',$idModelo) -> get('modelo') -> row_array();
		if ($modelo) {
			$modelo['ar_configuracao'] = json_decode($modelo['ar_configuracao']);
			return $modelo;
		}else{
			return "Erro de processamento do modelo!!";
		}
	}

	public function getModeloAtual($select=null,$categoria = null)
	{
		if($select){
			$aux = $this -> db -> select($select) -> where('usual',1) -> where('categoria',$categoria) -> get('modelo') -> row_array();
			if($aux){
				return $aux[$select];
			}else{
				return false;
			}
		}else{
			return $this -> db -> where('usual',1) -> get('modelo') -> row_array();
		}
	}


	public function setActiveModelo($idModelo,$categoria)
	{	
		$id_modelo_old = $this -> getModeloAtual('id_modelo',$categoria);

		$id_modelo_new = $idModelo;

		$this -> db -> update('modelo',array('usual'=>0),array('id_modelo'=>$id_modelo_old));
		$this -> db -> update('modelo',array('usual'=>1),array('id_modelo'=>$id_modelo_new));
		$retorno['status'] = 'success';
		$retorno['alertify']['mensagem'] = "Alteração realizada! ";
		$retorno['reload'] = true;
		$this-> load -> model("functions/Functions_model","fn");
		$this -> fn -> logs('upd',$this -> session -> id_usuario,'Alteração do modelo ativo de . '.$id_modelo_old.' para '.$id_modelo_new.' feita por '.$this->session->nome);
		return $retorno;
	}
	/*
		Função que salva o plano no db
	*/
	public function gravaPlano($post)
	{
		// $ar_info['nome_plano'] = $post['nome_plano'];
		$ar_info['dados_plano'] = json_encode($post['ar_plano']);
		// $ar_info['id_usuario'] = $this -> session -> id_usuario;
		// $ar_info['id_disciplina'] = $post['id_disciplina'];
		$ar_info['id_periodo'] = $post['id_periodo'];
		// $ar_info['data_criacao_plano'] = time();
		// $ar_info['id_modelo'] = $this -> getModeloAtual('id_modelo');
		if($this -> db ->where("id_plano",$post['id_plano']) -> update('plano',$ar_info)){
			$retorno['status'] = 'success';
			$retorno['alertify']['mensagem'] = "Plano Salvo! ";
			$retorno['redirect'] = base_url("/plano/lista");
			$this-> load -> model("functions/Functions_model","fn");
			$this -> fn -> logs('upd',$this -> session -> id_usuario,'Salva o plano '.$post['id_plano'].' feita por '.$this->session->nome);
		}else{
			$retorno['status'] = 'error';
			$retorno['alertify']['mensagem'] = "Erro ao gravar novo plano! ";
		}
		return $retorno;
	}

	/*
			Função para retornar todoso os planos publicados de uma disciplina
			@input identificador de uma disciplina
			@output array de dados dos planos da disciplina de entrada.
	*/
	public function getPlanosPublicados($id_disciplina)
	{
		$dados = $this -> db 
									-> select('plano.nome_plano,periodo.nome_periodo,plano.id_plano') 
									-> from('plano') 
									->join('periodo','plano.id_periodo = periodo.id_periodo')
									-> where('id_disciplina', $id_disciplina) 
									-> where('publicado_plano','1') 
									-> get() 
									-> result_array();
		if($dados)
			return $dados;
		else
			return 'Ainda não existem planos publicados para essa disciplina';
	}

	public function getPlanos()
	{
		if($this->functions->checkPermissao(array('Administrador'))){
			return $this -> db -> get('plano') -> result_array();
		}else{
			return $this -> db -> where('id_usuario',$this -> session -> id_usuario) -> get('plano') -> result_array();
		}
	}
	
	
/*
			Função responsável para retornar as informações dos planos
	*/
	public function getInfos($tipo=null,$permissao=null)
	{
		if($tipo && !$permissao){
			switch ($tipo) {
				case 'sem_avaliacao':
					$this -> db 
								-> select('p.id_plano, p.nome_plano, d.id_disciplina, d.nome_disciplina, u.nome,p.data_alteracao_plano')
								-> from('plano as p')
								-> join('disciplina as d','p.id_disciplina = d.id_disciplina')
								-> join('curso as c', 'p.id_curso = c.id_curso')
								-> join('usuario as u','p.id_usuario = u.id_usuario')
								-> where('p.avaliado_pedagogico','0')
								-> where('p.avaliado_coord','0')
								-> where('p.submit_plano','1');
					if(in_array('Pedagógico',$this -> session -> userdata('nome_regra')) || in_array('Coordenador',$this -> session -> userdata('nome_regra'))){
						$this -> db -> where('c.coord_curso',$this->session->id_usuario);
					}
					return	$this -> db -> get('') -> result_array();
					break;
				case 'publicados':
					return $this -> db 
								-> select('p.id_plano, p.nome_plano, d.id_disciplina, d.nome_disciplina, u.nome,p.data_alteracao_plano')
								-> from('plano as p')
								-> join('disciplina as d','p.id_disciplina = d.id_disciplina')
								-> join('usuario as u','p.id_usuario = u.id_usuario')
								-> where('p.publicado_plano','1')
								-> get('')
								-> result_array();
				break;
				case 'pendentes':
					if(in_array('Pedagógico',$this -> session -> userdata('nome_regra')) || in_array('Administrador',$this -> session -> userdata('nome_regra'))){
						return $this -> db 
									-> select('p.id_plano, p.nome_plano, d.id_disciplina, d.nome_disciplina, u.nome,p.data_alteracao_plano')
									-> from('plano as p')
									-> join('disciplina as d','p.id_disciplina = d.id_disciplina')
									-> join('usuario as u','p.id_usuario = u.id_usuario')
									-> where('p.publicado_plano','0')
									-> where('p.avaliado_pedagogico','0')
									-> where('p.avaliado_coord','1')
									-> where('p.submit_plano','1')
									-> get('')
									-> result_array();
					}else{
						$this -> db 
									-> select('p.id_plano, p.nome_plano, d.id_disciplina, d.nome_disciplina, u.nome,p.data_alteracao_plano')
									-> from('plano as p')
									-> join('disciplina as d','p.id_disciplina = d.id_disciplina')
									-> join('usuario as u','p.id_usuario = u.id_usuario')
									-> join('curso as c', 'p.id_curso = c.id_curso')
									-> where('p.avaliado_pedagogico','0')
									-> where('p.avaliado_coord','0')
									-> where('p.submit_plano','1');
						if(in_array('Coordenador',$this -> session -> userdata('nome_regra'))){
							$this -> db -> where('c.coord_curso',$this->session->id_usuario);
						}
						return $this -> db -> get('')
									-> result_array();
					}
				break;
				case 'em_criacao':
					return $this -> db 
								-> select('p.id_plano, p.nome_plano, d.id_disciplina, d.nome_disciplina, u.nome')
								-> from('plano as p')
								-> join('disciplina as d','p.id_disciplina = d.id_disciplina')
								-> join('usuario as u','p.id_usuario = u.id_usuario')
								-> where('p.publicado_plano','0')
								-> where('p.avaliado_coord','0')
								-> where('p.avaliado_pedagogico','0')
								-> where('p.submit_plano','0')
								-> get('')
								-> result_array();
				break;
				default:
					# code...
					break;
			}
		}
		if($tipo && $permissao){
			switch ($tipo) {
				case 'sem_avaliacao':
					return $this -> db 
									-> query("
										select  p.id_plano, p.nome_plano, p.revisao,d.id_disciplina, d.nome_disciplina, u.nome, p.data_alteracao_plano
										from plano as p
										join disciplina as d on p.id_disciplina = d.id_disciplina
										join usuario as u on p.id_usuario = u.id_usuario
										where 
											p.submit_plano = 1
										and 
											p.id_usuario = ".$this->session->id_usuario."
										and
											(p.avaliado_pedagogico = '0' or p.avaliado_coord = '0')
									")
									-> result_array();
					break;
				case 'publicados':
					return $this -> db 
								-> select('p.id_plano, p.nome_plano, d.id_disciplina, d.nome_disciplina, u.nome, p.revisao ,p.data_alteracao_plano')
								-> from('plano as p')
								-> join('disciplina as d','p.id_disciplina = d.id_disciplina')
								-> join('usuario as u','p.id_usuario = u.id_usuario')
								-> where('p.publicado_plano','1')
								-> where('p.id_usuario',$this->session->id_usuario)
								-> get('')
								-> result_array();
				break;
				case 'pendentes':
						return $this -> db 
									-> select('p.id_plano, p.nome_plano, d.id_disciplina, d.nome_disciplina, u.nome, p.revisao, p.data_alteracao_plano')
									-> from('plano as p')
									-> join('disciplina as d','p.id_disciplina = d.id_disciplina')
									-> join('usuario as u','p.id_usuario = u.id_usuario')
									-> where('p.revisao','1')
									-> where('p.id_usuario',$this->session->id_usuario)
									-> get('')
									-> result_array();
					
				break;
				case 'em_criacao':
					return $this -> db 
								-> select('p.id_plano, p.nome_plano, d.id_disciplina, d.nome_disciplina, u.nome, p.revisao')
								-> from('plano as p')
								-> join('disciplina as d','p.id_disciplina = d.id_disciplina')
								-> join('usuario as u','p.id_usuario = u.id_usuario')
								-> where('p.publicado_plano','0')
								-> where('p.avaliado_coord','0')
								-> where('p.avaliado_pedagogico','0')
								-> where('p.submit_plano','0')
								-> where('p.revisao','0')
								-> where('p.id_usuario',$this->session->id_usuario)
								-> get('')
								-> result_array();
				break;
				default:
					# code...
					break;
			}
		}
	}

	/*
		Função que submete o plano para avaliação
	*/
	public function SubmitPlano($post = null)	{
		if($post){
			if(empty($post['ar_plano'])){
				if(empty($this->db->where('id_plano',$post['id_plano'])-> get('plano')->row_array()['dados_plano'])){
					$retorno['status'] = 'error';
					$retorno['alertify']['mensagem'] = "Erro submeter o plano! Plano sem conteúdo!! ";
					return $retorno;
				}else{
					if($this -> db -> where('id_plano',$post['id_plano']) 
						-> set('submit_plano','1') 
						-> set('locked','1') 
						// -> set('dados_plano',json_encode($post['ar_plano'])) 
						-> set('avaliado_coord','0') 
						-> set('avaliado_pedagogico','0') 
						-> set('revisao','0') 
						-> update('plano')){
							$retorno['status'] = 'success';
							$retorno['alertify']['mensagem'] = "Plano Submetido! ";
							$retorno['redirect'] = base_url("/plano/lista");
							$this-> load -> model("functions/Functions_model","fn");
							$this -> fn -> logs('upd',$this -> session -> id_usuario,'Submete o plano '.$post['id_plano'].' para avliação ');
							$this -> db -> where('id_plano',$post['id_plano'])->set('data_alteracao_plano',time())->update('plano');
					}else{
						$retorno['status'] = 'error';
						$retorno['alertify']['mensagem'] = "Erro submeter o plano! ";
					}
					return $retorno;
				}
			}
			if($this -> db -> where('id_plano',$post['id_plano']) 
										-> set('submit_plano','1') 
										-> set('locked','1') 
										-> set('dados_plano',json_encode($post['ar_plano'])) 
										-> set('avaliado_coord','0') 
										-> set('avaliado_pedagogico','0') 
										-> set('revisao','0') 
				-> update('plano')){
				$retorno['status'] = 'success';
				$retorno['alertify']['mensagem'] = "Plano Submetido! ";
				$retorno['redirect'] = base_url("/plano/lista");
				$this-> load -> model("functions/Functions_model","fn");
				$this -> fn -> logs('upd',$this -> session -> id_usuario,'Submete o plano '.$post['id_plano'].' para avliação ');
				$this -> db -> where('id_plano',$post['id_plano'])->set('data_alteracao_plano',time())->update('plano');
			}else{
				$retorno['status'] = 'error';
				$retorno['alertify']['mensagem'] = "Erro submeter o plano! ";
			}
			return $retorno;
		}
	}

	/*
		Função que retorna aos professores os planos para correções
	*/
	public function Correcoes($post)
	{
		if($post){
			if ($this -> db -> where('id_plano',$post['id_plano']) -> set('locked','0') -> set('submit_plano','0') -> set('revisao','1') -> set('observacao',json_encode($post['ar_coment'])) -> update('plano')) {
				$retorno['status'] = 'success';
				$retorno['alertify']['mensagem'] = "Correções submetidas! ";
				$retorno['redirect'] = base_url("avaliacoes");
				$this-> load -> model("functions/Functions_model","fn");
				$this -> fn -> logs('upd',$this -> session -> id_usuario,'Envio de correção do plano '.$post['id_plano'].' para revisão feita por '.$this->session->nome);
				$this -> db -> where('id_plano',$post['id_plano'])->set('data_alteracao_plano',time())->update('plano');
			}else{
				$retorno['status'] = 'error';
				$retorno['alertify']['mensagem'] = "Erro ao submeter correções! ";
			}
			return $retorno;
		}
	}

	/*
		Função para aprovação dos planos 
	*/
	public function aprovarPlano($post)
	{
		if($this->functions->checkPermissao(array('Coordenador','Administrador'),$this->session->nome_regra)){
			if($this-> db -> where('id_plano',$post['id_plano']) -> set('avaliado_coord','1') -> update('plano')){
				$retorno['status'] = 'success';
				$retorno['alertify']['mensagem'] = "Ação realizada! ";
				$retorno['redirect'] = base_url("avaliacoes");
				$this-> load -> model("functions/Functions_model","fn");
				$this -> fn -> logs('upd',$this -> session -> id_usuario,'Plano avaliado pela cordenação. '.$post['id_plano'].' feita por '.$this->session->nome);
			}else{
				$retorno['status'] = 'error';
				$retorno['alertify']['mensagem'] = "Erro ao realizar ação! ";
			}
			if($this -> db -> where('id_plano',$post['id_plano'])->where('avaliado_pedagogico','1')->get('plano')->row_array()){
				$this -> db -> where('id_plano',$post['id_plano']) -> set('publicado_plano','1')->set('data_alteracao_plano',time())->update('plano');
				$this -> fn -> logs('upd','','O plano '.$post['id_plano'].' foi publicado');
			}
			return $retorno;
		}
		if($this->functions->checkPermissao(array('Pedagógico','Administrador'),$this->session->nome_regra)){
			if($this-> db -> where('id_plano',$post['id_plano']) -> set('avaliado_pedagogico','1') -> update('plano')){
				$retorno['status'] = 'success';
				$retorno['alertify']['mensagem'] = "Ação realizada! ";
				$retorno['redirect'] = base_url("avalicoes");
				$this-> load -> model("functions/Functions_model","fn");
				$this -> fn -> logs('upd',$this -> session -> id_usuario,'Plano avaliado pelo pedagógico. '.$post['id_plano'].' feita por '.$this->session->nome);
			}else{
				$retorno['status'] = 'error';
				$retorno['alertify']['mensagem'] = "Erro ao realizar ação! ";
			}
			if($this -> db -> where('id_plano',$post['id_plano'])->where('avaliado_coord','1')->get('plano')->row_array()){
				$this -> db -> where('id_plano',$post['id_plano']) -> set('publicado_plano','1')->set('data_alteracao_plano',time())->update('plano');
				$this -> fn -> logs('upd','','O plano '.$post['id_plano'].' foi publicado');				
			}
			return $retorno;
		}
	}


}
