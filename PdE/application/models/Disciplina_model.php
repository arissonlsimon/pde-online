<?php
	class Disciplina_model extends CI_Model {

		function __construct() {
			//@session_start();
		}

		public function listar()
		{
			$planos = $this-> db -> select('d.id_disciplina,d.nome_disciplina,u.nome,d.cargahoraria_disciplina,c.nome_curso') 
			-> FROM('disciplina as d') 
			-> join('disciplina_usuario as du','d.id_disciplina = du.fk_disciplina_id_disciplina') 
			-> join('usuario as u', 'du.fk_usuario_id_usuario = u.id_usuario') 
			-> join('curso_disciplina as cd','cd.fk_disciplina_id_disciplina = d.id_disciplina')
			-> join('curso as c','c.id_curso = cd.fk_curso_id_curso')
			-> get() -> result_array();
			if($planos){
				$retorno['dados'] = $planos;
			}else{
				$retorno['dados'] = 'Não possui disciplinas cadastradas ainda';
			}

			$retorno['msg'] = 'Pesquisa Concluida';
			$retorno['reload'] = false;
			$retorno['flag'] = true;

			return $retorno;
		}


		public function getDisciplinas($values = null)
		{
			if(empty($values)){
				if($this -> functions -> checkPermissao(array('Administrador'),$this->session->nome_regra)){
					return $this -> db -> get('disciplina as d') -> result_array();
				}else{
					$this -> db -> select('*') -> from('disciplina as d') 
					-> join('disciplina_usuario as du','d.id_disciplina=du.fk_disciplina_id_disciplina') ;
					if(!empty($this->session->id_usuario)){
						$this -> db -> where('du.fk_usuario_id_usuario',$this->session->id_usuario) ;
					}
					return $this -> db -> get() -> result_array();
				}
			}else{
				if(is_array($values)){

				}else{
					if(is_numeric($values)){
						$ret['disciplina'] = $this -> db -> where('id_disciplina', $values)  -> join('disciplina_usuario as du','du.fk_disciplina_id_disciplina=d.id_disciplina') 
						-> join('curso_disciplina as cd','d.id_disciplina=cd.fk_disciplina_id_disciplina') 
						-> join('usuario as u','u.id_usuario=du.fk_usuario_id_usuario') 
						-> join('curso as c','cd.fk_curso_id_curso=c.id_curso')  -> get('disciplina as d') -> row_array();
						if($ret['disciplina']){
							// $ret['disciplina']['curso'] = $this -> db -> where('id_curso',$ret['disciplina']['id_curso']) -> get('curso') ->row_array();
							// $ret['disciplina']['professor'] = $this -> db -> where('id_usuario',$ret['disciplina']['id_usuario']) -> get('usuario') -> row_array();
						}else{
							$ret['disciplina'] = "OPAAAAAAAAAAAAAAA<br> ERRO NAS CONSULTAS DE DISCIPLINA EM !!!";
						}

						return $ret;
					}else{

					}
				}
			}
		}


		public function cadastra($post)
		{	
			$insert = false;
			
			if($this->db->insert('disciplina',array("nome_disciplina" => $post['nome_disciplina'], "cargahoraria_disciplina" => $post['cargahoraria_disciplina']))){
				$id_new_disciplina  = $this->db->insert_id();
				if($this->db->insert("disciplina_usuario",array("fk_disciplina_id_disciplina" => $id_new_disciplina, "fk_usuario_id_usuario" => $post['professores']))){
					if($this -> db -> insert("curso_disciplina", array("fk_disciplina_id_disciplina" => $id_new_disciplina, "fk_curso_id_curso" => $post["cursos"])) ){
						$insert = true;
						$this-> load -> model("functions/Functions_model","fn");
						$this -> fn -> logs('ins',$this -> session -> id_usuario,'Cadastro de nova disciplina '.$post['nome_disciplina'].' feito por '.$this -> session -> id_usuario);
					}else{
						$err_id = 'Error_03';
					}
				}else{
					$err_id = 'Error_02';
				}
			}else{
				$err_id = 'Error_01';
			}

			if ($insert) {
				$ret['status'] = 'success';
				$ret['alertify']['mensagem'] = 'Disciplina Cadastrada';
				$ret['redirect'] = false;
				$ret['reload'] = true;
			}else{
				$ret['status'] = 'error';
				$ret['alertify']['mensagem'] = 'Erro ao cadastrar Disciplina -> Error: '.$err_id;
				$ret['redirect'] = false;
				$ret['reload'] = false;
			}

			return $ret;
		}

		public function remove($id_disciplina){
			if($this->db->where('id_disciplina',$id_disciplina)->get('plano')->num_rows() == 0){
				try {
					$this -> db -> where('fk_disciplina_id_disciplina',$id_disciplina)->delete('disciplina_usuario');
					try {
						$this -> db -> where('fk_disciplina_id_disciplina',$id_disciplina)->delete('curso_disciplina');
						try {
							$this -> db -> where('id_disciplina',$id_disciplina)->delete('disciplina');
							$ret['status'] = 'success';
							$ret['alertify']['mensagem'] = 'Disciplina Removida';
							$ret['redirect'] = false;
							$ret['reload'] = true;
						} catch (\Throwable $th) {
							//throw $th;
							$ret['status'] = 'error';	
							$ret['alertify']['mensagem'] = 'Erro ao remover disciplina';
							$ret['redirect'] = false;
							$ret['reload'] = false;
						}
					} catch (\Throwable $th) {
						$ret['status'] = 'error';
						$ret['alertify']['mensagem'] = 'Erro ao remover disciplina';
						$ret['redirect'] = false;
						$ret['reload'] = false;
					}
				} catch (\Throwable $th) {
					//throw $th;
					$ret['status'] = 'error';
					$ret['alertify']['mensagem'] = 'Erro ao remover disciplina';
					$ret['redirect'] = false;
					$ret['reload'] = false;
				}
			}else{
				$ret['status'] = 'error';
				$ret['alertify']['mensagem'] = 'Não é possível remover a disciplinas, existem planos vinculados a ela';
				$ret['redirect'] = false;
				$ret['reload'] = false;
			}
			return $ret;
		}

		public function getDisciplinaByCourse($course_id)
		{
			$permission = $this -> functions -> checkPermissao(array('Administrador'),$this -> session -> nome_regra);
			if($permission){
				return $this -> db -> select('d.id_disciplina,d.nome_disciplina') 
							-> from('disciplina as d') 
							-> join('curso_disciplina as cd','d.id_disciplina = cd.fk_disciplina_id_disciplina') 
							-> where('cd.fk_curso_id_curso',$course_id) 
							-> get()
							-> result_array();
			}else{
				return $this -> db -> select('d.id_disciplina,d.nome_disciplina') 
							-> from('disciplina as d') 
							-> join('curso_disciplina as cd','d.id_disciplina = cd.fk_disciplina_id_disciplina')
							-> join('disciplina_usuario as du','d.id_disciplina = du.fk_disciplina_id_disciplina') 
							-> where('cd.fk_curso_id_curso',$course_id) 
							-> where('du.fk_usuario_id_usuario',$this -> session -> id_usuario)
							-> get()
							-> result_array();
			}
		}

		public function altera($post = null){
			if(empty($post)){
				$ret['status'] = 'error';
				$ret['alertify']['mensagem'] = 'Erro ao alterar os dados da Disciplina';
				$ret['redirect'] = false;
				$ret['reload'] = false;
			}else{
				// $this->functions->pre($post,true);
				if($this->db->set("nome_disciplina", $post['nome_disciplina'])->set("cargahoraria_disciplina", $post['cargahoraria_disciplina'])->where('id_disciplina',$post['id_disciplina'])->update('disciplina')){
					$id_cd = $this->db->where('fk_disciplina_id_disciplina',$post['id_disciplina'])->get('curso_disciplina')->row_array();
					if($this -> db ->set("fk_curso_id_curso", $post["cursos"])->where("id_curso_disciplina", $id_cd['id_curso_disciplina'])-> update("curso_disciplina") ){
						$id_du = $this->db->where('fk_disciplina_id_disciplina',$post['id_disciplina'])->get('disciplina_usuario')->row_array();
						if($this->db->set("fk_usuario_id_usuario", $post['professores'])->where("id_disciplina_usuario",$id_du['id_disciplina_usuario'])->update("disciplina_usuario")){
							$this-> load -> model("functions/Functions_model","fn");
							$this -> fn -> logs('upt',$this -> session -> id_usuario,'Atualização da disciplina '.$post['nome_disciplina'].' feito por '.$this -> session -> id_usuario);
							$ret['status'] = 'success';
							$ret['alertify']['mensagem'] = 'Disciplina Alterada';
							$ret['redirect'] = base_url()."disciplina/lista";
							$ret['reload'] = true;
							return $ret;
						}else{
							$err_id = 'Error_03';
						}
					}else{
						$err_id = 'Error_02';
					}
				}else{
					$err_id = 'Error_01';
				}
			}
		}

	}
