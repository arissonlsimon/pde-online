<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Avaliacoes extends CI_Controller{
  function __construct(){
    parent::__construct();
    if(!$this -> session -> has_userdata("id_usuario")){
      redirect(base_url());
    }
  }

  public function index()
  {
    $this -> load -> model('Plano_model');

    $this -> load -> view('default/top');
    $this -> load -> view('default/menu_bar');
    if ($this -> functions -> checkPermissao(array('Pedagógico','Coordenador','Administrador'),$this->session->nome_regra)) {
      $this -> load -> view('revisor/init',array('sem_avaliacao'=> $this -> Plano_model -> getInfos('sem_avaliacao'), 
                                                  'publicados'=> $this -> Plano_model->getInfos('publicados'),
                                                  'pendentes' => $this -> Plano_model -> getInfos('pendentes'),
                                                  'em_criacao' => $this -> Plano_model -> getInfos('em_criacao')
                                                ));
    }else{
      $this -> load -> view('painel_professor', array('submetidos'=> $this -> Plano_model -> getInfos('sem_avaliacao','pf'), 
                                                      'publicados'=> $this -> Plano_model->getInfos('publicados','pf'),
                                                      'pendentes' => $this -> Plano_model -> getInfos('pendentes','pf'),
                                                      'desenvolvimento' => $this -> Plano_model -> getInfos('em_criacao','pf')
                                                    ));
    }
    $this -> load -> view('default/bot');
  }

  

}