<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	function __construct() {
		parent::__construct();
		require_once BASEPATH.'libraries/mpdf60/mpdf.php';
	}

	public function index()
	{
		$this->load->view('default/top');
		$this -> load -> view('default/menu_bar');
		$this->load->view('welcome_message');
		$this->load->view('default/bot');
	}


	public function getCursos($var)
	{
		$this -> load -> model('Curso_model');

		$this->load->view('default/top');
		$this -> load -> view('default/menu_bar');
		$this -> load -> view('lista_cursos_view',$this -> Curso_model -> getCursos($var));
		$this -> load -> view('default/bot');
	}

	public function login()
	{
		// die("AQ");
		if (!empty($_POST)) {
			$this -> load -> model("Usuario_model");
			exit(json_encode($this -> Usuario_model -> login($_POST)));
		}
	}

	public function listaPlano($id_disciplina)
	{
		$this -> load -> model("Plano_model");
		$this -> load -> model("Disciplina_model");

		$this -> load -> view('default/top');
		$this -> load -> view('default/menu_bar');

		$this -> load -> view('lista_planos_view',array("planos" => $this -> Plano_model -> getPlanosPublicados($id_disciplina), "nome_disciplina" => $this -> Disciplina_model -> getDisciplinas($id_disciplina)['nome_disciplina']));

		$this -> load -> view('default/bot');
	}

	public function impressaoPlano($id_plano = null){
		// die('oasdlas');
		if(!empty($id_plano)){
				# Carregamento dos models
				$this -> load -> model("Plano_model");
				$this -> load -> model("Curso_model");
				$this -> load -> model("Datas_model");
				$this -> load -> model("Disciplina_model");
				# Carregamento dos models end
	
				$dados['plano'] = $this -> Plano_model -> getPlano($id_plano);
				$dados['id_plano'] = $id_plano;
				$dados['periodo'] = $this -> Datas_model -> getPeriodo($dados['plano']['id_periodo']);
				$dados['disciplina'] =  $this -> Disciplina_model -> getDisciplinas($dados['plano']['id_disciplina']);
				$dados['modelo'] = $this -> Plano_model -> extracModelo($id_plano);
				$dados['curso'] =  $this -> Curso_model -> getCursos($dados['plano']['id_curso']);
				if($dados['plano']['publicado_plano']){
					$this -> session -> unset_userdata('erro_print');
					$this -> load -> view('imprimir_plano_view',$dados);
				}else{
					$this -> session -> set_userdata('erro_print','Erro de requisição!');
					$var = "<script>javascript:history.back(-2)</script>";
					echo $var;
				}
			
		}else{
			die('fon');
		}
	}

}
