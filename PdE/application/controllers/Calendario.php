<?php
defined('BASEPATH') OR exit('No direct script access allowed');


	class Calendario extends CI_Controller {

		function __construct() {
			parent::__construct();
			//@session_start();

			// < Verifica se o usuario esta logado
			if(!$this->session->has_userdata("id_usuario")) {
				redirect(base_url());
			}
		// > Verifica se o usuario esta logado
		}

		public function index()
		{   
			$this->load->model('Datas_model');
			$this->load->view('default/top');
			$this->load->view('default/menu_bar');
			$this->load->view('admin/calendario_view',array('periodo'=>$this->Datas_model->getPeriodos()));
			$this->load->view('default/bot');
		}

    public function cadastraPeriodo()
    {
      if(!empty($_POST)){
        $this -> load -> model('Datas_model');
        exit(json_encode($this -> Datas_model -> cadastraPeriodo($_POST)));
      }
    }

    
    public function explorarPeriodo()
    {
    	if (!empty($_POST)) {
    		$this -> load -> model('Datas_model');
    		

				$this->load->view('default/top');
				$this->load->view('default/menu_bar');
				$this->load->view('admin/list_datas_view',$this -> Datas_model -> showDatas($_POST));
				$this->load->view('default/bot');
    	}
    }

    public function feriado()
    {
    	$this -> load -> model('Datas_model');
    	if(!empty($_POST)){
    		if($_POST['func'] == 'addFeriado'){
    			exit(json_encode($this -> Datas_model -> feriado('add',$_POST)));
    		}
    		if ($_POST['func'] == 'removeFeriado') {
    			exit(json_encode($this -> Datas_model -> feriado('remove',$_POST)));
    		}
    	}
    }

    public function changeDatas()
    {
        if(!empty($_POST)){
            $this -> load -> model('Datas_model');
            exit(json_encode($this -> Datas_model -> changeDatas($_POST)));
        }
    }

		/*	
			Função para remover o periodo
			input $_post identificador do periodo
			output confirmação da remoção do periodo JSON
		*/
		public function deletarPeriodo()
		{
			if(!empty($_POST)){
				$this -> load -> model('Datas_model');
				exit(json_encode($this -> Datas_model -> deletarPeriodo($_POST['id_periodo'])));
			}
		}

	public function alterar(){
		if(!empty($_POST)){
			$this->load->model('Datas_model');
			exit(json_encode($this->Datas_model->altera($_POST)));
		}
	}

} # Fim da Classe Calendario