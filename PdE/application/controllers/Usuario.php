<?php
defined('BASEPATH') OR exit('No direct script access allowed');


	class Usuario extends CI_Controller {

		function __construct() {
			parent::__construct();
			//@session_start();

			// < Verifica se o usuario esta logado
			if(!$this->session->has_userdata("id_usuario")) {
				redirect(base_url());
			}
		// > Verifica se o usuario esta logado
		}

		public function index()
		{
			die('usuario');
		}

		public function lista()
		{
			$this->load->model('Usuario_model');
			$this->load->view('default/top');
			$this->load->view('default/menu_bar');
			$this->load->view('admin/lista_usuarios_view', array_merge($this -> Usuario_model -> listar(),$this -> Usuario_model -> listarRegras()));
			$this->load->view('default/bot');
		}

		public function cadastra()
		{
			$this -> load -> model("Usuario_model");
			exit(json_encode($this -> Usuario_model -> cadastra($_POST)));
		}

		public function altera()
		{
			die('altera curso');
		}


		public function alterarStatus()
		{
			if (isset($_POST)) {
				$this -> load -> model("Usuario_model");
				exit(json_encode($this->Usuario_model->alterarStatus($_POST)));
			}
		}


		public function alterar()
		{
			if (isset($_POST)) {
				$this -> load -> model("Usuario_model");
				exit(json_encode($this->Usuario_model->alterar($_POST)));
			}
		}


		public function regra()
		{
			$this->load->model('Usuario_model');
			$this->load->view('default/top');
			$this->load->view('default/menu_bar');
			$this->load->view('admin/lista_regras_view', ($this->Usuario_model->listarRegras()));
			$this->load->view('default/bot');
		}


		public function cadastraRegra()
		{
			$this->load->model('Usuario_model');
			exit(json_encode($this->Usuario_model->cadastraRegra($_POST)));
		}

		public function removeRegra()
		{
			$this->load->model('Usuario_model');
			exit(json_encode($this->Usuario_model->removeRegra($_POST)));	
		}

		public function redefinePass()
		{
			if (!empty($_POST)) {
				$this -> load -> model("Usuario_model");
				exit(json_encode($this->Usuario_model->redefinePass($_POST)));
			}
		}

		public function logout()
		{
			$this -> load -> model("Usuario_model");
			if($this -> Usuario_model -> logout() == "index"){
				header("Location:".base_url());
			}
		}


		/**
		* Busca pro usuarios
		*
		* @param string $_POST - Valores de entrada para filtrar a busca
		* @param string $tipo - tipo que sera executado posteriormente dentro do modle
		*/
		public function buscar($tipo = null) {
			$this -> load -> model("Usuario_model");
			exit($this -> Usuario_model -> busca($_POST, $tipo));
		}

		public function getPerfil(){
			$this->load->model("Usuario_model");
			
			$this->load->view('default/top');
			$this->load->view('default/menu_bar');
			$this->load->view('perfil_view', ($this->Usuario_model->getPerfil()));
			$this->load->view('default/bot');
		}

	}
