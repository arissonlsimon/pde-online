<?php

defined('BASEPATH') OR exit('No direct script access allowed');


	class Disciplina extends CI_Controller {

		function __construct() {
			parent::__construct();
			//@session_start();

			// < Verifica se o usuario esta logado
			if(!$this->session->has_userdata("id_usuario")) {
				redirect(base_url());
			}
		// > Verifica se o usuario esta logado
		}


		public function index()
		{
			# code...
		}

		public function remove(){
			if(!empty($_POST)){
				$this->load->model("Disciplina_model");
				exit(json_encode($this->Disciplina_model->remove($_POST['id_disciplina'])));
			}
		}

		public function cadastra()
		{
			if (!empty($_POST)) {
				$this->load->model("Disciplina_model");
				exit(json_encode($this->Disciplina_model->cadastra($_POST)));
			}

			$this -> load -> view('default/top');
			$this -> load -> view('default/menu_bar');
			$this -> load -> view('admin/cadastra_disciplina_view');
			$this -> load -> view('default/bot');
		}

		public function altera()
		{
			$this->load->model('Disciplina_model');
			exit(json_encode($this->Disciplina_model->altera($_POST)));
		}

		public function lista($idDisciplina = null)
		{
			$this->load->model('Disciplina_model');
			if(empty($idDisciplina)){
				$this->load->view('default/top');
				$this->load->view('default/menu_bar');
				$this->load->view('admin/lista_disciplinas_view', ($this->Disciplina_model->listar()));
				$this->load->view('default/bot');
			}else{
				extract($this->Disciplina_model->getDisciplinas($idDisciplina));
				$disciplina['id_disciplina'] = $idDisciplina;

				$this->load->view('default/top');
				$this->load->view('default/menu_bar');
				$this->load->view('admin/editar_disciplina_view', array('disciplina'=>$disciplina));
				$this->load->view('default/bot');
			}

		}

		public function getDisciplinaByCourse()
		{
			if(!empty($_POST)){
				$this -> load -> model('Disciplina_model');
				exit(json_encode($this -> Disciplina_model -> getDisciplinaByCourse($_POST['id_curso'])));
			}
		}

}
