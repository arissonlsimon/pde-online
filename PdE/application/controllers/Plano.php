<?php
defined('BASEPATH') OR exit('No direct script access allowed');


	class Plano extends CI_Controller {

		function __construct() {
			parent::__construct();
			//@session_start();

			// < Verifica se o usuario esta logado
			if(!$this->session->has_userdata("id_usuario")) {
				redirect(base_url());
			}
		// > Verifica se o usuario esta logado
		}

		public function index()
		{
			die('plano');
		}

		public function lista()
		{
			$this->load->model('Plano_model');
			$this->load->view('default/top');
			$this->load->view('default/menu_bar');
			$this->load->view('plano/lista_planos_view', ($this->Plano_model->listar()));
			$this->load->view('default/bot');
		}

		public function menuPlano()
		{
			$this->load->model("Disciplina_model");
			$this -> load -> model("Datas_model");
			$this->load->view('default/top');
			$this->load->view('default/menu_bar');
			$this->load->view('plano/menu');
			$this->load->view('default/bot');
		}

		public function plano($value='view')
		{
			if (!empty($value)) {
				$this -> load -> model("Plano_model");
				switch ($value) {
					case 'new':
						$this -> EditarPlano();
						break;
					case 'copy':
						$this -> Plano_model -> copiaPlano($_POST);
						break;
					case 'view':
						$this -> Plano_model -> verPlano($_POST);
						// code...
						break;
					case 'deletar':
						$this -> Plano_model -> deletarPlano($_POST);
						// code...
						break;
					case 'editar':
						$this -> EditarPlano($this -> Plano_model -> getPlano($_POST));
						break;
					default:
						// code...
						break;
				}
			}
		}
		
		/*
		* Função responsável por editar os planos de ensino
		* input(opcional) id do plano a ser editado
		*/
		public function EditarPlano($plano = null)
		{
			# Carregamento dos models
			$this -> load -> model("Plano_model");
			$this -> load -> model("Curso_model");
			$this -> load -> model("Datas_model");
			$this -> load -> model("Disciplina_model");
			# Carregamento dos models

			if(!empty($plano)){
				$dados['plano'] = $this -> Plano_model -> getPlano($plano);
				$dados['id_plano'] = $plano;
				$dados['periodo'] = $this -> Datas_model -> getPeriodo($dados['plano']['id_periodo']);
				$dados['disciplina'] =  $this -> Disciplina_model -> getDisciplinas($dados['plano']['id_disciplina']);
				$dados['modelo'] = $this -> Plano_model -> extracModelo($plano);
				$dados['curso'] =  $this -> Curso_model -> getCursos($dados['plano']['id_curso']);
				$this -> load -> view('default/top');
				$this -> load -> view('default/menu_bar');
				$this -> load -> view('plano/editar_plano_view',$dados);
				$this -> load -> view('default/bot');
			}
			if(!empty($_POST)){
				exit(json_encode($this -> Plano_model -> gravaPlano($_POST)));
			}
		}

	
	public function modelo($cadastra=null)
	{
		$this -> load -> model("Plano_model");
		if(!$cadastra){
			$this -> load -> view('default/top');
			$this -> load -> view('default/menu_bar');
			$this -> load -> view('plano/listar_modelo_plano_view',array('modelo' => $this -> Plano_model -> listaModelos()));
			$this -> load -> view('default/bot');
		}else{
			$this -> load -> view('default/top');
			$this -> load -> view('default/menu_bar');
			$this -> load -> view('plano/cadatra_modelo_view');
			$this -> load -> view('default/bot');
		}
		if (!empty($_POST)) {
			exit(json_encode($this->Plano_model->cadastraModelo($_POST)));
		}
	}

	public function getModelo($idModelo)
	{		
		if(!empty($idModelo)){
			$this -> load -> model('Plano_model');
			exit(json_encode($this -> Plano_model -> getModelo($idModelo)));
		}else{
			redirect(base_url());
		}
	}

	public function removeModelo(){
		if(!empty($_POST)){
			$this -> load -> model('Plano_model');
			exit(json_encode($this->Plano_model->removeModelo($_POST['id'])));
		}
	}

	public function setActiveModelo()
	{	
		$this -> load -> model('Plano_model');
		exit(json_encode($this -> Plano_model -> setActiveModelo($_POST['id_modelo'], $_POST['categoria'])));
	}

	/*
		Função para retornar form dos planos de ensino
	*/
	public function getForm($tipo = null)
	{
		if($tipo){
			if($tipo == 'novo'){
				$this -> load -> model("Curso_model");
				$this -> load -> model("Datas_model");

				$dados['curso'] = $this -> Curso_model -> getCursos();
				$dados['periodo'] = $this -> Datas_model -> getPeriodos();
				exit(json_encode($this-> load -> view("plano/form_novo_plano_view",$dados,true)));
			}
			if ($tipo == 'copia') {
				$this -> load -> model("Datas_model");
				$this -> load -> model("Plano_model");
				$this -> load -> model("Curso_model");
				
				$dados['curso'] = $this -> Curso_model -> getCursos();
				$dados['periodo'] = $this -> Datas_model -> getPeriodos();
				$dados['plano'] = $this -> Plano_model -> getPlanos();
				exit(json_encode($this-> load -> view("plano/form_copia_plano_view",$dados,true)));
			}
		}
	}

	/*
		Função que pré-registra um novo plano vazio
		@input dados para criação de um novo plano (id_usuario,id_disciplina,id_periodo,categoria,nome do plano)
		@output array json para redirecionamento;
	*/
	public function novoPlano($plano = null)
	{
		if(!empty($_POST)){
			$this -> load -> model('Plano_model');
			$resp = $this -> Plano_model -> novoPlano($_POST);
			if($resp['status'] == 'success'){
				header("Location:".$resp['redirect']);
			}else{
				header("Location:".base_url()."plano/menuPlano");
			}
		}
		if(!empty($plano)){
			# Carregamento dos models
			$this -> load -> model("Plano_model");
			$this -> load -> model("Curso_model");
			$this -> load -> model("Datas_model");
			$this -> load -> model("Disciplina_model");
			# Carregamento dos models
			$this -> load -> view('default/top');
			$this -> load -> view('default/menu_bar');
			$this -> load -> view('plano/novo_plano_view', array(
				'plano' => $this -> Plano_model -> getPlano($plano),
				'curso'=> $this -> Curso_model -> getCursos(), 
				'periodo'=> $this -> Datas_model -> getPeriodos(), 
				'disciplina' => $this -> Disciplina_model -> getDisciplinas(),
				'modelo'  => $this -> Plano_model -> extracModelo($plano)
			));
			$this -> load -> view('default/bot');	
		}
	}

	public function copiaPlano(){
		if (!empty($_POST)) {
			$this -> load -> model('Plano_model');
			
			$resp = $this -> Plano_model -> copiaPlano($_POST);
			if($resp['status'] == 'success'){
				header("Location:".$resp['redirect']);
			}else{
				header("Location:".base_url()."plano/menuPlano");
			}
		}
	}

	/*
		Função para submição do plano para avaliação 
	
	*/
	public function SubmitPlano()
	{
		if(!empty($_POST)){
			$this -> load -> model('Plano_model');
			exit(json_encode($this -> Plano_model -> SubmitPlano($_POST)));
		}
	}

	/*
		Função que irá aplicar devolver ao professor o plano para que sejam aplicadas as correções descritas pelos colaboradores
	*/
	public function Correcoes()
	{
		if(!empty($_POST)){
			$this -> load -> model('Plano_model');
			exit(json_encode($this -> Plano_model -> Correcoes($_POST)));
		}
	}

	/*
		Função para aprovar o plano descrito pelo professor
	*/
	public function aprovarPlano()
	{
		if(!empty($_POST)){
			$this -> load -> model('Plano_model');
			exit(json_encode($this -> Plano_model -> aprovarPlano($_POST)));
		}
	}


	public function imprimir($id_plano = null)
	{
		// die('printer');
		if(!empty($id_plano)){
			# Carregamento dos models
			$this -> load -> model("Plano_model");
			$this -> load -> model("Curso_model");
			$this -> load -> model("Datas_model");
			$this -> load -> model("Disciplina_model");
			require_once BASEPATH.'libraries/mpdf61/mpdf.php';
			# Carregamento dos models end

			$dados['plano'] = $this -> Plano_model -> getPlano($id_plano);
			$dados['id_plano'] = $id_plano;
			$dados['periodo'] = $this -> Datas_model -> getPeriodo($dados['plano']['id_periodo']);
			$dados['disciplina'] =  $this -> Disciplina_model -> getDisciplinas($dados['plano']['id_disciplina']);
			$dados['modelo'] = $this -> Plano_model -> extracModelo($id_plano);
			$dados['curso'] =  $this -> Curso_model -> getCursos($dados['plano']['id_curso']);
			// $this -> functions->pre($dados,true);
			$this -> load -> view('plano/printer_view',$dados);

		}
	}


}
