<?php
defined('BASEPATH') OR exit('No direct script access allowed');


	class Periodo extends CI_Controller {

		function __construct() {
			parent::__construct();
			//@session_start();

			// < Verifica se o usuario esta logado
			if(!$this->session->has_userdata("id_usuario")) {
				redirect(base_url());
			}
		// > Verifica se o usuario esta logado
		}


		public function index()
		{
			$this -> load -> model("Datas_model");

			$this -> load -> view('default/top');
			$this -> load -> view('default/menu_bar');
			$this -> load -> view('admin/periodo_view',array('periodo' => $this -> Datas_model -> getPeriodos()));
			$this -> load -> view('default/bot');
		}


		public function getPeriodos()
		{
			$this -> load -> model("Datas_model");

			var_dump( $this -> Datas_model -> getPeriodos());
		}

	}