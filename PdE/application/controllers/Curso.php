<?php
defined('BASEPATH') OR exit('No direct script access allowed');


	class Curso extends CI_Controller {

		function __construct() {
			parent::__construct();
			//@session_start();

			// < Verifica se o usuario esta logado
			if(!$this->session->has_userdata("id_usuario")) {
				redirect(base_url());
			}
		// > Verifica se o usuario esta logado
		}


		public function index()
		{
			die('curso');
		}

		public function cadastra()
		{
			if (!empty($_POST)) {			
				$this -> load -> model("Curso_model");
				exit(json_encode($this->Curso_model->cadastra_curso($_POST)));
			}

			$this -> load -> view('default/top');
			$this -> load -> view('default/menu_bar');
			$this -> load -> view('admin/cadastra_curso_view');
			$this -> load -> view('default/bot');
		}

		public function remove()
		{
			if(!empty($_POST)){
				$this -> load -> model("Curso_model");
				exit(json_encode($this -> Curso_model -> removeCurso($_POST['id_curso'])));
			}
		}

		// lista disciplinas geral, todas as disciplinas.
		public function lista($id_curso=null)
		{
			$this->load->model('Curso_model');
			if (!$id_curso) {
				$this->load->view('default/top');
				$this->load->view('default/menu_bar');
				$this->load->view('admin/lista_cursos_view', ($this->Curso_model->listar()));
				$this->load->view('default/bot');
			}else{
				extract($this->Curso_model->getCursos($id_curso));
				$curso['id_curso'] = $id_curso;

				$this->load->view('default/top');
				$this->load->view('default/menu_bar');
				$this->load->view('admin/editar_curso_view', array("curso" => $curso));
				$this->load->view('default/bot');
			}

		}


		public function alterar()
		{
			if(!empty($_POST)){
				$this -> load -> model('Curso_model');
				exit(json_encode($this -> Curso_model -> alterar($_POST)));
			}
		}

		/**
		* Busca pro usuarios
		*
		* @param string $_POST - Valores de entrada para filtrar a busca
		* @param string $tipo - tipo que sera executado posteriormente dentro do modle
		*/

		public function buscar($tipo = null) {
			$this -> load -> model("Curso_model");
			exit($this -> Curso_model -> busca($_POST, $tipo));
		}

	}
