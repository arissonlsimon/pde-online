<?php
    if(!$this -> session -> userdata("nome_regra")){
        redirect(base_url());
    }
    $size_disciplina = count($disciplina_info);
    $size_curso = count($curso_info);
?>

<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Perfil do Usuário</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <!-- Dados USUARIO -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-user fa-fw"></i> Dados do Usuário
                    </div>
                    <div class="panel-body">
                        <?php
                            if(!empty($user_info)){
                                echo '
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Email</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>'.$user_info['nome'].'</td>
                                                <td>'.$user_info['email'].'</td>
                                                <td>'.($user_info['status'] == 'A'?'Ativo':'Suspenso').'</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                ';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <!-- Dados REGRA -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-user fa-fw"></i> Regras do Usuário
                    </div>
                    <div class="panel-body">
                        <?php
                            if(!empty($user_info)){
                                echo '
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Perfil</th>
                                                <th>Descrição</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>'.$regra_info['nome_regra'].'</td>
                                                <td>'.$regra_info['descricao'].'</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                ';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dados DISCIPLINA -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-book"></i> Disciplinas
                    </div>
                    <div class="panel-body">
                        <?php
                            if(!empty($disciplina_info)){
                                echo'
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nome</th>
                                                <th>Carga Horária</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                ';
                                $count = 1;
                                foreach ($disciplina_info as $d) {
                                    echo '<tr>';
                                    echo '
                                        <td>'.$count.'</td>
                                        <td>'.$d['nome_disciplina'].'</td>
                                        <td>'.$d['cargahoraria_disciplina'].'</td>
                                    ';
                                    echo '</tr>';
                                    $count++;
                                }
                                echo '  </tbody>
                                    </table>
                                </div>
                                ';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dados CURSO -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-book"></i> Cursos
                    </div>
                    <div class="panel-body">
                        <?php
                            if(!empty($disciplina_info)){
                                echo'
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nome</th>
                                                <th>Categoria</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                ';
                                $count = 1;
                                foreach ($curso_info as $c) {
                                    echo '<tr>';
                                    echo '
                                        <td>'.$count.'</td>
                                        <td>'.$c['nome_curso'].'</td>
                                        <td>'.$c['categoria_curso'].'</td>
                                    ';
                                    echo '</tr>';
                                    $count++;
                                }
                                echo '  </tbody>
                                    </table>
                                </div>
                                ';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>