
<?php  
if (!is_array($curso)) {
  echo '
    <div class="row">
      <div class="col-lg-12">
        <p>
          '.$curso.'      
        </p>
      </div>
    </div>
  ';
}else{
?>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-12">
              <div class="form-group col-md-8">
                  <b class="text-left" style="font-size:13px">
                    <a target="_self" data-toggle="tooltip" data-placement="top" title="Nome do curso. Dica: Procure usar o nome por extenso"><span class="glyphicon glyphicon-question-sign"></span></a>
                    <label for="tags">Nome</label>
                  </b>
                  <input type="text" class="form-control" name="nome_curso" id="nome_curso" data-required="Informe o nome do curso" value="<?php echo @$curso['nome_curso'] ?>">
              </div>
              <div class="form-group col-md-4">
                <b class="text-left" style="font-size:13px">
                    <a target="_self" data-toggle="tooltip" data-placement="top" title="Código do Curso. Ex.: ADM,ADS,PG,PED,MAT..."><span class="glyphicon glyphicon-question-sign"></span></a>
                    <label for="tags">Código do curso</label>
                  </b>
                  <input type="text" class="form-control" name="codigo_curso" id="codigo_curso" data-required="Informe o codigo do curso" value="<?php echo @$curso['codigo_curso'] ?>">
              </div>
              <div class="form-group col-md-6">
                <b class="text-left" style="font-size:13px">
                    <a target="_self" data-toggle="tooltip" data-placement="top" title="Nome do coordenador do curso. Dica: campo de pesquisa, use o email para localizar mais facilmente."><span class="glyphicon glyphicon-question-sign"></span></a>
                    <label for="tags">Coordenador</label>
                  </b>
                  <input class="form-control" name="coord_curso" id="coord_curso" data-required="Informe o coordenador deste curso">
              </div>
              <div class="form-group col-md-4">
                <b class="text-left" style="font-size:13px">
                    <a target="_self" data-toggle="tooltip" data-placement="top" title="Informe a categoriado curso"><span class="glyphicon glyphicon-question-sign"></span></a>
                    <label for="tags">Categoria</label>
                  </b>
                  <select class="form-control" name="categoria_curso" id="categoria_curso" data-required="Selecione a categoria">
                    <option value="<?php echo @$curso['categoria_curso'] ?>"><?php echo @$curso['categoria_curso'] ?></option>
                    <option value="Graduação">Graduação</option>
                    <option value="Técnico">Técnico</option>
                    <option value="PÓS">PÓS</option>
                    <option value="Ensino Médio">Ensino Médio</option>
                  </select>
              </div>
              <input type="hidden" name="idcursoalterado" id="idcursoalterado" value="<?php echo @$curso['id_curso'] ?>">
          </div> 
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $('.form-group').tooltip({
      selector: "[data-toggle=tooltip]",
      container: "body"
  });
  $(document).ready(function () {
    $("#coord_curso").tokenInput(base_url + "usuario/buscar/tokenInput", {
      tokenLimit        : 1,
      method        : 'POST',
      queryParam      : 'busca',
      searchingText     : "Procurando...",
      hintText        : "Quem você procura?",
      noResultsText     : "Nenhum resultado encontrado.",
      theme           : "facebook",
      <?php 
      if(!empty($curso['coord_curso'])) {
        echo 'prePopulate: [
                            {
                              id_usuario:'.$curso['coord_curso']['id_usuario'].',
                              nome: "'.$curso['coord_curso']['nome'].'",
                              email: "'.$curso['coord_curso']['email'].'"
                            }
                          ]
                        ';
      }
      ?>
      ,tokenFormatter    : function(usuario) {
        return '<li><div style="display:inline-block;padding-left:10px;"><div class="full_name">'+usuario.nome+'</div><div class="email>'+usuario.email+'</div></div></li>';
      },
      resultsFormatter  : function(usuario) {
        return '<li><div style="display:inline-block;padding-left:10px;"><div class="full_name">'+usuario.nome+'</div><div class="email">'+usuario.email+'</div></div></li>';
      },
      preventDuplicates : true,
      propertyToSearch : 'id_usuario',
      tokenValue : 'id_usuario'
    });
  });
  
</script>
<?php } ?>
 <?php 
      // if(!empty($curso['coord_curso'])) {
      //   echo ',prePopulate: [';
      //   foreach($curso['coord_curso'] as $key => $value) {
      //     $coordenadores[] = '{
      //         id_usuario: '.$curso['coord_curso']['id_usuario'].',
      //         nome: "'.$curso['coord_curso']['nome'].'",
      //         email: "'.$curso['coord_curso']['email'].'"
      //       }';
      //   }
      //   echo implode(',',$coordenadores).']';
      // }
?>