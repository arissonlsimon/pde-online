<?php 
  
?>

<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
    	
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Editar Curso</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <form role="form" onsubmit="return false;">
        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel panel-heading">
                Informações do curso
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-12">
                      <div class="form-group col-md-8">
                          <b class="text-left" style="font-size:13px">
                            <a target="_self" data-toggle="tooltip" data-placement="top" title="Nome do curso. Dica: Procure usar o nome por extenso"><span class="glyphicon glyphicon-question-sign"></span></a>
                            <label for="nome">Nome</label>getCursos
                          </b>
                          <input type="text" class="form-control" name="nome_curso" id="nome_curso" data-required="Informe o nome do curso" value="<?php echo @$curso['nome_curso']; ?>">
                      </div>
                      <div class="form-group col-md-4">
                        <b class="text-left" style="font-size:13px">
                            <a target="_self" data-toggle="tooltip" data-placement="top" title="Código do Curso. Ex.: ADM,ADS,PG,PED,MAT..."><span class="glyphicon glyphicon-question-sign"></span></a>
                            <label for="nome">Código do curso</label>
                          </b>
                          <input type="text" class="form-control" name="codigo_curso" id="codigo_curso" data-required="Informe o codigo do curso" value="<?php echo @$curso['codigo_curso']; ?>">
                      </div>
                      <div class="form-group col-md-6">
                        <b class="text-left" style="font-size:13px">
                            <a target="_self" data-toggle="tooltip" data-placement="top" title="Nome do coordenador do curso. Dica: campo de pesquisa, use o email para localizar mais facilmente."><span class="glyphicon glyphicon-question-sign"></span></a>
                            <label for="nome">Coordenador</label>
                          </b>
                          <input class="form-control" name="coord_curso" id="coord_curso" data-required="Informe o coordenador deste curso">
                          <input type="hidden" name="curso_id" id="curso_id" value="<?php echo $curso['id_curso'];?>">
                      </div>
                      <div class="form-group col-md-4">
                        <b class="text-left" style="font-size:13px">
                            <a target="_self" data-toggle="tooltip" data-placement="top" title="Informe a categoriado curso"><span class="glyphicon glyphicon-question-sign"></span></a>
                            <label for="nome">Categoria</label>
                          </b>
                          <select class="form-control" name="categoria_curso" id="categoria_curso" data-required="Selecione a categoria">
                            <option value="">Selecione</option>
                            <option value="Graduação">Graduação</option>
                            <option value="Técnico">Técnico</option>
                            <option value="PÓS">PÓS</option>
                            <option value="Ensino Médio">Ensino Médio</option>
                          </select>
                      </div>
                      <script>
                        $(document).ready(function() {
                          var selecteds = "<?php echo $curso['categoria_curso'];?>";
                          
                          $("#categoria_curso option").each(function(){
                            
                            if($(this).val() == selecteds){
                              $(this).attr('selected', true);
                            }
                          });
                        });
                    </script>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-body text-right">
                <button class="btn btn-success" type="submit">
                  Editar Curso
                </button>
              </div>
            </div>
          </div>
        </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<script>
  $(document).ready(function () {
    $("#coord_curso").tokenInput("<?php echo base_url(); ?>usuario/buscar/tokenInput", {
      tokenLimit        : 1,
      method        : 'POST',
      queryParam      : 'busca',
      searchingText     : "Procurando...",
      hintText        : "Quem você procura?",
      noResultsText     : "Nenhum resultado encontrado.",
      theme           : "facebook",
      <?php 
      if(!empty($curso['coord_curso'])) {
        echo 'prePopulate: [
                            {
                              "id_usuario":"'.$curso['coord_curso']['id_usuario'].'",
                              "nome": "'.$curso['coord_curso']['nome'].'",
                              "email": "'.$curso['coord_curso']['email'].'"
                            }
                          ],
                        ';
      }
      ?>
      tokenFormatter    : function(usuario) {
        return '<li><div style="display:inline-block;padding-left:10px;"><div class="full_name">'+usuario.nome+'</div><div class="email>'+usuario.email+'</div></div></li>';
      },
      resultsFormatter  : function(usuario) {
        return '<li><div style="display:inline-block;padding-left:10px;"><div class="full_name">'+usuario.nome+'</div><div class="email">'+usuario.email+'</div></div></li>';
      },
      preventDuplicates : true,
      propertyToSearch : 'id_usuario',
      tokenValue : 'id_usuario'
    });
  });
  $('.form-group').tooltip({
      selector: "[data-toggle=tooltip]",
      container: "body"
  });

  $(".btn-success").click(function(){
    var envia = true;
    // alert("FON "+ $("#nome_curso").val() +' '+ $("#codigo_curso").val() +' '+ $("#coord_curso").val() +' '+ $("#categoria_curso").val());
    $('input, textarea, select', $("form")).each(function() {
            if($(this).attr('data-required') != undefined && $(this).prop('disabled') == false) {
                if(((
                    $(this).attr('type') == 'radio' 
                    || $(this).attr('type') == 'checkbox') 
                    && !$("input[name='"+ $(this).attr('name') +"']:checked").length)
                    || (
                        $(this).attr('type') == 'text'
                        ||$(this).attr('type') == 'file'
                        ||$(this).attr('type') == 'number'
                        ||$(this).prop('type') == 'textarea'
                        ||$(this).prop('type') == 'select-one'
                        ||$(this).prop('type') == 'password'
                       )
                    && $(this).val() == ''
                ) {
                    $('.sendActivated').removeClass('sendActivated').button('reset');
                    inputError($(this), $(this).attr('data-required'));
                    envia = false;
                    return false;
                }
            }
        });
    if(envia){
      sendTo(base_url + 'curso/alterar',{id_curso:$("#curso_id").val(), nome_curso: $("#nome_curso").val(), codigo_curso: $("#codigo_curso").val(), coord_curso: $("#coord_curso").val(), categoria_curso: $("#categoria_curso").val()},'POST');
    }
  });
</script>