<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Lista as regras</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel panel-heading">
              Regras de Usuário
              <div class="pull-right">
                <button type="button" data-toggle="modal" data-target="#modalCadastraNovoRegra" class="btn btn-primary btn-xs"><span class="fa fa-plus"></span> Cadastrar</button>
              </div>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nome</th>
                      <th>Descrição</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      if (isset($regras)) {
                        if(is_array($regras)){
                          foreach ($regras as $key => $value) {
                            echo '
                              <tr>
                                <td>
                                  '.$value['id_regra'].'
                                </td>
                                <td>
                                '.$value['nome_regra'].'
                                </td>
                                <td>
                                  '.$value['descricao'].'
                                </td>
                                <td>
                                  <button href="#" data-idzao="'.$value['id_regra'].'" class="rm-permissao btn btn-link">
                                    <i class="fa fa-trash"></i>
                                  </button>
                                </td>
                              </tr>
                            ';
                          }
                        }else{
                          echo '
                          <tr>
                            <td colspan="6">'.$regras.'</td>
                          </tr>
                          ';
                        }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<script type="text/javascript">
  $(".rm-permissao").click(function(){
    // alert("oloco " + $(this).data("idzao") + " fuck");
    sendTo(base_url+'usuario/removeRegra',{id_regra:$(this).data("idzao")},'POST');
  });
</script>

<form onsubmit="return false;" name="cadastraRegra" id="cadastraRegra" method="POST" data-ajax="true" action="<?php  echo base_url();?>usuario/cadastraRegra">
  <div class="modal fade" id="modalCadastraNovoRegra" tabindex="-1" role="dialog" aria-labelledby="modalCadastraRegraLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="limparCamposModal();" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="modalCadastraRegraLabel">Cadastro Regras</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row">
              <div class="form-group col-md-12">
                <b class="text-left" style="font-size:13px">
                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Nome da regra que será cadastrada"><span class="glyphicon glyphicon-question-sign"></span></a>
                  <label for="nome">Nome</label>
                </b>
                <input type="text" class="form-control" name="nome" id="nome" data-required="Informe o nome do usuário">
              </div>
            </div>
            <div class="row">
              <div class="form-group has-feedback col-md-12">
                <b class="text-left" style="font-size:13px">
                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Insira uma descrição para futuramente ser mais facil de saber sua funcionalidade"><span class="glyphicon glyphicon-question-sign"></span></a>
                  <label for="descricao">Descrição</label>
                </b>
                <textarea data-required="Informe a descrição" rows="10" class="form-control" name="descricao" id="descricao"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" name="btnCancelar" id="btnCancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-success">Salvar</button>
        </div>
      </div>
    </div>
  </div>
</form>