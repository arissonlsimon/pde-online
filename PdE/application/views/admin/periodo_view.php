<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Períodos</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel panel-heading">
              Períodos
              <div class="pull-right">
                <button type="button" data-toggle="modal" data-target="#modalCadastraNovo" class="btn btn-primary btn-xs"><span class="fa fa-plus"></span> Cadastrar</button>
              </div>
            </div>
            <div class="panel-body">
              <div class="panel-group" id="accordion" role="tablist">
                <?php
                  if (isset($periodo)) {
                    if(is_array($periodo)){
                      foreach ($periodo as $key => $value) {
                        echo '
                          <div class="panel panel-heading" role="tab" data-toggle="collapse" data-target="#tab',$value['id_periodo'],'">
                            <h3 class="panel-title">
                              '.$value['nome_periodo'].' '.$value['abreviacao_periodo'].' <span class="fa arrow"></span>
                            </h3>
                          </div>
                          <div class="panel-collapse collapse" role="tabpanel" id ="tab'.$value['id_periodo'].'">
                            <div class="panel-body">
                              <div class="row">
                                <td>
                                  '.date('d/m/Y',$value['data_inicio_periodo']).'
                                </td>
                                <td>
                                  '.date('d/m/Y',$value['data_final_periodo']).'
                                </td>
                                <td>
                                  <button class="btn btn-link viewDatas" data-periodo="'.$value['id_periodo'].'"><i class="fa fa-pencil"></i></button>
                                </td>
                              </div>
                            </div>
                        ';
                      }  
                    }else{
                      echo '
                      <tr>
                        <td colspan="5">'.$periodo.'</td>
                      </tr>
                      ';
                    }
                  }
                ?>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<script>
  $(".viewDatas").click(function(){
    var id_periodo = $(this).data('periodo');
    alert('id: '+id_periodo);

  })
  
</script>
