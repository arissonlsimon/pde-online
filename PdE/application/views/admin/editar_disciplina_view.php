<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
    	
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Editar Disciplina</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <form role="form" onsubmit="return false;">
        
        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel panel-heading">
                Informações da disciplina
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-12">
                      <div class="form-group col-md-8">
                          <b class="text-left" style="font-size:13px">
                            <a target="_self" data-toggle="tooltip" data-placement="top" title="Nome da disciplina. Dica: Procure usar o nome por extenso"><span class="glyphicon glyphicon-question-sign"></span></a>
                            <label for="nome">Nome</label>
                          </b>
                          <input type="text" class="form-control" name="nome_disciplina" id="nome_disciplina" data-required="Informe o nome da disciplina" value="<?php echo @$disciplina['nome_disciplina'] ?>">
                      </div>
                      <div class="form-group col-md-4">
                        <b class="text-left" style="font-size:13px">
                            <a target="_self" data-toggle="tooltip" data-placement="top" title="Está disciplina possui a carga horária de quanto? Use somente números"><span class="glyphicon glyphicon-question-sign"></span></a>
                            <label for="nome">Carga horária</label>
                          </b>
                          <input autocomplete="off" type="number" class="form-control" name="cargaHoraria" id="cargaHoraria" data-required="Informe a carga horária" value="<?php echo @$disciplina['cargahoraria_disciplina'] ?>">                            
                      </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="form-group col-md-12">
                      <b class="text-left" style="font-size:13px">
                        &nbsp; <a target="_self" data-toggle="tooltip" data-placement="top" title="Procure e adicione o(os) professores desta disciplina"><span class="glyphicon glyphicon-question-sign"></span></a>
                        Professor
                      </b> 
                      <input autocomplete="off" type="text" class="form-control" name="professores" id="professores" data-required="Informe os professores deste curso">
                    </div>
                  </div> 
                  <div class="col-lg-12">
                    <div class="form-group col-md-12">
                      <b class="text-left" style="font-size:13px">
                        &nbsp; <a target="_self" data-toggle="tooltip" data-placement="top" title="Procure e adicione o(os) curso(s) que oferecem esta disciplina. Caso não apareça a disciplina cadastre-a"><span class="glyphicon glyphicon-question-sign"></span></a>
                        Cursos
                      </b> 
                      <input autocomplete="off" type="text" class="form-control" name="cursos" id="cursos" data-required="Informe a qual curso(s) esta disciplina pertence.">
                        <input type="hidden" name="disciplina_id" id="disciplina_id" value="<?php echo $disciplina['id_disciplina'];?>">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-body text-right">
                <button type="submit" class="btn btn-success">Editar</button>
              </div>
            </div>
          </div>
        </div>

      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<script>
   $('.form-group').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    });
    $(document).ready(function () {
      $("#professores").tokenInput("<?php echo base_url(); ?>usuario/buscar/tokenInput", {
        tokenLimit    :2,
        method        : 'POST',
        queryParam      : 'busca',
        searchingText     : "Procurando...",
        hintText        : "Quem você procura?",
        noResultsText     : "Nenhum resultado encontrado.",
        theme           : "facebook",
        <?php 
        if(!empty($disciplina['nome'])) {
          echo 'prePopulate: [
                              {
                                "id_usuario":"'.$disciplina['id_usuario'].'",
                                "nome": "'.$disciplina['nome'].'",
                                "email": "'.$disciplina['email'].'"
                              }
                            ],
                          ';
        }
        ?>
        tokenFormatter    : function(usuario) {
          return '<li><div style="display:inline-block;padding-left:10px;"><div class="full_name">'+usuario.nome+'</div><div class="email>'+usuario.email+'</div></div></li>';
        },
        resultsFormatter  : function(usuario) {
          return '<li><div style="display:inline-block;padding-left:10px;"><div class="full_name">'+usuario.nome+'</div><div class="email">'+usuario.email+'</div></div></li>';
        },
        preventDuplicates : true,
        propertyToSearch : 'id_usuario',
        tokenValue : 'id_usuario'
      });

      $("#cursos").tokenInput("<?php echo base_url(); ?>curso/buscar/tokenInput", {
        method        : 'POST',
        queryParam      : 'busca',
        searchingText     : "Procurando...",
        hintText        : "Qual curso você procura?",
        noResultsText     : "Nenhum resultado encontrado.",
        theme           : "facebook",
        <?php 
        if(!empty($disciplina['nome_curso'])) {
          
          echo 'prePopulate: [
                              {
                                "id_curso":"'.$disciplina['id_curso'].'",
                                "nome_curso": "'.$disciplina['nome_curso'].'"
                              }
                            ],
                          ';
        }
        ?>
        tokenFormatter    : function(curso) {
          return '<li><div style="display:inline-block;padding-left:10px;"><div class="full_name">'+curso.nome_curso+'</div></div></li>';
        },
        resultsFormatter  : function(curso) {
          return '<li><div style="display:inline-block;padding-left:10px;"><div class="full_name">'+curso.nome_curso+'</div></div></li>';
        },
        preventDuplicates : true,
        propertyToSearch : 'id_curso',
        tokenValue : 'id_curso'
      });
    });

    $(".btn-success").click(function(){
    var envia = true;
    // alert("FON "+ $("#nome_curso").val() +' '+ $("#codigo_curso").val() +' '+ $("#coord_curso").val() +' '+ $("#categoria_curso").val());
    $('input, textarea, select', $("form")).each(function() {
            if($(this).attr('data-required') != undefined && $(this).prop('disabled') == false) {
                if(((
                    $(this).attr('type') == 'radio' 
                    || $(this).attr('type') == 'checkbox') 
                    && !$("input[name='"+ $(this).attr('name') +"']:checked").length)
                    || (
                        $(this).attr('type') == 'text'
                        ||$(this).attr('type') == 'file'
                        ||$(this).attr('type') == 'number'
                        ||$(this).prop('type') == 'textarea'
                        ||$(this).prop('type') == 'select-one'
                        ||$(this).prop('type') == 'password'
                       )
                    && $(this).val() == ''
                ) {
                    $('.sendActivated').removeClass('sendActivated').button('reset');
                    inputError($(this), $(this).attr('data-required'));
                    envia = false;
                    return false;
                }
            }
        });
    if(envia){
      sendTo(base_url + 'disciplina/altera',{id_disciplina:$("#disciplina_id").val(), nome_disciplina: $("#nome_disciplina").val(), cargahoraria_disciplina: $("#cargaHoraria").val(), professores: $("#professores").val(), cursos: $("#cursos").val()},'POST');
    }
  });
</script>