<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Lista as disciplinas</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel panel-heading">
              Disciplinas
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nome</th>
                      <th>Responsável</th>
                      <th>Carga horária</th>
                      <th>Curso</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      if (isset($dados)) {
                        if(is_array($dados)){
                          foreach ($dados as $key => $value) {
                            echo '
                              <tr>
                                <td>
                                  '.$value['id_disciplina'].'
                                </td>
                                <td>
                                '.$value['nome_disciplina'].'
                                </td>
                                <td>
                                '.$value['nome'].' 
                                </td>
                                <td>
                                '.$value['cargahoraria_disciplina'].' 
                                </td>
                                <td>
                                '.$value['nome_curso'].' 
                                </td>
                                <td>
                                  <a href="lista/'.$value['id_disciplina'].'" class="btn btn-link fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Editar Disciplina"></a>
                                  <button class="btn btn-link fa fa-trash" data-toggle="tooltip" data-placement="top" data-iddisciplinaremove="'.$value['id_disciplina'].'" style="color:#d9534f" title="Remover disciplina"></button>
                                </td>
                              </tr>
                            ';
                          }
                        }else{
                          echo '
                          <tr>
                            <td colspan="5">'.$dados.'</td>
                          </tr>
                          ';
                        }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<script>
$(".fa-trash").click(function(){
  sendTo(base_url + "disciplina/remove",{id_disciplina:$(this).data('iddisciplinaremove')},"POST");
})
</script>