<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Lista os cursos</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel panel-heading">
              Cursos
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nome</th>
                      <th>Código</th>
                      <th>Formação</th>
                      <th>Coord</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      if (isset($dados)) {
                        if(is_array($dados)){
                          foreach ($dados as $key => $value) {
                            echo '
                              <tr>
                                <td>
                                  '.$value['id_curso'].'
                                </td>
                                <td>
                                '.$value['nome_curso'].'
                                </td>
                                <td>
                                '.$value['codigo_curso'].'
                                </td>
                                <td>
                                  '.$value['categoria_curso'].'
                                </td>
                                <td>
                                '.$value['nome'].' 
                                </td>
                                <td>
                                  <a href="'.base_url().'curso/lista/'.$value['id_curso'].'" class="btn btn-link fa fa-pencil" name="editarcurso" data-toggle="tooltip" data-placement="top" title="Editar Curso"></a>
                                  <button data-idcursoremove="'.$value['id_curso'].'" class="btn btn-link fa fa-trash" name="removercurso" style="color:#d9534f;" data-toggle="tooltip" data-placement="top" title="Remover Curso"></button>
                                </td>
                              </tr>
                            ';
                          }
                        }else{
                          echo '
                          <tr>
                            <td colspan="5">'.$dados.'</td>
                          </tr>
                          ';
                        }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<!-- MODAL PARA ALTERAÇÃO DE CURSO -->
    <div class="modal fade" id="modalalteracurso" role="dialog" aria-labelledby="modalalteracurso" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="row">
              <div class="col-sm-3">
                <h5 class="modal-title">CURSO</h5>
              </div>
              <div class="col-sm-9 pull-right">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div id="alterarcurso">
            </div>    
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger fa fa-close" data-dismiss="modal"> Fechar</button>
            <button type="button" class="btn btn-primary fa fa-save" id="salvaralteracoes"> Salvar</button>
          </div>
        </div>
      </div>
    </div>
<!-- MODAL ALTERAÇÃO DE CURSO END -->




<script>
  // $("[name='editarcurso']").on('click',function(){
  //   // $.post(base_url+'curso/altera',{id_curso:$(this).data('idcurso')},function(response){
  //   //     $("#alterarcurso").empty();
  //   //     $("#alterarcurso").append(JSON.parse(response));
  //   //     $("#modalalteracurso").modal('show');
  //   // });
  // });
    $("[name='removercurso']").click(function(){
      if(confirm("Deseja remover esse curso?")){
        sendTo(base_url + 'curso/remove/',{id_curso:$(this).data('idcursoremove')},'POST');
      }
    });
  $("#salvaralteracoes").on('click',function(){
    sendTo(base_url+'curso/alterar',{id_curso: $("#idcursoalterado").val(),nome_curso:$("#nome_curso").val(),codigo_curso: $("#codigo_curso").val(), coord_curso:$("#coord_curso").val(), categoria_curso: $("#categoria_curso").val()},'POST');
  });
   

</script>