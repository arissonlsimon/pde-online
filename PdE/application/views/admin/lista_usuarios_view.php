<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Lista os usuários</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel panel-heading">
              Usuários
              <div class="pull-right">
                <button type="button" data-toggle="modal" data-target="#modalCadastraNovo" class="btn btn-primary btn-xs"><span class="fa fa-plus"></span> Cadastrar</button>
              </div>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nome</th>
                      <th>Email</th>
                      <th>Regra</th>
                      <th>Status</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      if (isset($dados)) {
                        if(is_array($dados)){
                          foreach ($dados as $key => $value) {
                            echo '
                              <tr>
                                <td>
                                  '.$value['id_usuario'].'
                                </td>
                                <td>
                                '.$value['nome'].'
                                </td>
                                <td>
                                  '.$value['email'].'
                                </td>
                                <td>
                                '.$value['nome_regra'].'
                                </td>
                                <td>';
                            if ($value['status'] ==='A') {
                              echo '<button id="status" data-toggle="status" data-status="Ativo" data-iduser="'.$value['id_usuario'].'" class="btn btn-success btn-xs">Ativo</button>';
                            }else{
                              echo '<button id="status"  data-toggle="status" data-status="Suspenso" data-iduser="'.$value['id_usuario'].'" class="btn btn-danger btn-xs">Suspenso</button>';
                            }

                            echo '</td>

                              <td>
                                <button data-iduser="'.$value['id_usuario'].'" class="btn btn-link fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Editar Usuário"/>
                              </td>
                              </tr>
                            ';
                          }
                        }else{
                          echo '
                          <tr>
                            <td colspan="5">'.$dados.'</td>
                          </tr>
                          ';
                        }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->


<form onsubmit="return false;" name="cadastroUsuario" id="cadastroUsuario" method="POST" data-ajax="true" action="<?php echo base_url(); ?>usuario/cadastra">
  <input type="hidden" name="tipo" id="tipo" value="usuario">
  <div class="modal fade" id="modalCadastraNovo" tabindex="-1" role="dialog" aria-labelledby="modalCadastraRegraLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="modalCadastraRegraLabel">Cadastro de Usuário</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row">
              <div class="form-group col-md-12">
                <b class="text-left" style="font-size:13px">
                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Informe o e-mail do usuário"><span class="glyphicon glyphicon-question-sign"></span></a>
                  <label for="email">E-mail</label>
                </b>
                <input type="email" class="form-control" name="email" id="email" data-required="Informe o e-mail do usuário" value="<?php echo @$altData['email']?>" required>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-12">
                <b class="text-left" style="font-size:13px">
                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Nome do usuário que será cadastrado"><span class="glyphicon glyphicon-question-sign"></span></a>
                  <label for="nome">Nome</label>
                </b>
                <input type="text" class="form-control" name="nome" id="nome" data-required="Informe o nome do usuário" value="<?php echo @$altData['nome'] ?>">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-12">
                <b class="text-left" style="font-size:13px">
                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Defina qual o tipo de permissão esse usuário irá possuir"><span class="glyphicon glyphicon-question-sign"></span></a>
                  <label for="grupoPermissao">Grupo de permissão</label>
                </b>
                <select class="form-control" name="regra" id="regra" data-required="Selecione o grupo">
                  <option value="">Selecione</option>
                  <?php
                    if(!empty($regras)) {
                      foreach($regras as $key => $value) {
                        if(isset($altData['nome_regra']) && $altData['nome_regra'] == $value['nome_regra']){
                          echo '<option value="'.$value['id_regra'].'" selected>'.$value['nome_regra'].'</option>';
                        }
                        else
                          echo '<option value="'.$value['id_regra'].'">'.$value['nome_regra'].'</option>';
                      }
                    }
                  ?>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-12">
                <b class="text-left" style="font-size:13px">
                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Situação do cadastro do usuário, ativo ou suspenso"><span class="glyphicon glyphicon-question-sign"></span></a>
                  <label for="status">Situação:</label>
                </b>
                <ul name="situacao" style="list-style:none" class="padding-l-10">
                  <div class="radio">
                    <label>
                      <input type="radio" checked name="status" id="ativo" value="A"> Ativo
                    </label>
                    &nbsp&nbsp
                    <label>
                      <input type="radio" name="status" id="suspenso" value="S"> Suspenso
                    </label>
                  </div>
                </ul>
              </div>
            </div>
          </div>
          <input type="text" hidden="true" name="idUsuario" id="idUsuario" value="<?php echo @$altData['id_usuario'] ?>">
        </div>
        <div class="modal-footer">
          <button type="button" name="btnCancelar" id="btnCancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-success">Salvar</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script>
  $("button[data-toggle=status]").click(function(){
    var bt = $(this);
    // console.log('A ' + $(this).data("status") + " B " + $(this).data("iduser"));
    $.post("<?php echo base_url(); ?>usuario/alterarStatus",{statusAtual:$(this).data("status"), idUsuario: $(this).data("iduser")}, function(ret){
        console.log(ret);
        if(ret.return){
          alertify.success(ret.alertify.mensagem);
          if(ret.redirect == "reload") {
            setTimeout(function(){ location.reload(); }, 2000);
          } else if(ret.redirect == "back") {
            setTimeout(function(){ history.back(1); }, 2000);
          } else if(ret.redirect) {
            setTimeout(function(){ location.href = ret.redirect; }, 2000);
          }
        }else{
          alertify.error(ret.alertify.mensagem);
        }
    }, "json");
  });

  $(".fa-pencil").click(function(){
    var dados = <?php  echo json_encode($dados);?>;
    var i = $(this).data('iduser');
    $.each(dados,function(index,value){
      if(value.id_usuario == i){
        $("#cadastroUsuario").attr('action',base_url+'usuario/alterar').append('<input type="hidden" name="id_usuario" id="id_usuario" value="'+i+'" />');
        $("[name=email]").val(value.email);
        $("#nome").val(value.nome);
        $("#regra option").each(function(){
          if(value.nome_regra == $(this).text()){
            $(this).attr('selected',true);
          } 
        });
        if(value.status == 'a'){
          $("#ativo").prop('checked',true);
          $("#suspenso").prop('checked',false);
        }else{
          $("#ativo").prop('checked',false);
          $("#suspenso").prop('checked',true);
        }
        $("#modalCadastraNovo").modal('show');
      }
    })
  })


</script>
