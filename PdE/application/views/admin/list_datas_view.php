<?php 
  if(!empty($datas)){
    $nome = $datas['nome'];
    $id_periodo = $datas['id_periodo'];
    unset($datas['nome']);
    unset($datas['id_periodo']);
  }
?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Calendário</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->  
      <div class="alert alert-info">
        <span class="glyphicon glyphicon-exclamation-sign"></span> Clique na data para identificar como feriado!
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel panel-heading">
              Calendário <?php  echo @$datas['nome'];?>
              <div class="pull-right">
                <!-- <button type="button" data-toggle="modal" data-target="#modalCadastraFerias" class="btn btn-primary btn-xs"><span class="fa fa-plus"></span> Férias</button> -->
              </div>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Segunda</th>
                      <th>Terça</th>
                      <th>Quarta</th>
                      <th>Quinta</th>
                      <th>Sexta</th>
                      <th>Sábado</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      if (isset($datas)) {
                        if(is_array($datas)){
                          if($datas[0]["dia_semana"] != 1){
                            $resto = $datas[0]["dia_semana"] -1;
                            for ($i=0; $i < $resto; $i++) { 

                              $aux[$i]["dia_semana"] = ($i+1);
                              $aux[$i]["data_integral"] = '';
                              $aux[$i]["dia_mes"] = '';
                              $aux[$i]["data_feriado"] = '';
                            }
                            $datas = array_merge($aux,$datas);
                            // echo "<pre>";
                            // print_r($datas);
                            // // var_dump($datas);
                            // die();
                          }
                          $table = '';
                          foreach ($datas as $key => $value) {
                            
                            if(($value['dia_semana'] == 7)){
                              $table .= "</tr>";
                            }
                            if($value['dia_semana'] == 1){
                             $table .=  "<tr>";
                            } 
                            if ($value['dia_semana'] == 6) {
                              $table .=  '<td>';
                                if (!empty($value['data_integral'])) {
                                  if (strcmp($value['data_feriado'],"true") === 0) {
                                    $table .= '<button  class="btn btn-danger" data-valordata="'.$value['data_integral'].'">'.$value['data_integral'].'</button>';
                                  }else{
                                    $table .= '<button class="btn btn-success" data-valordata="'.$value['data_integral'].'">'.$value['data_integral'].'</button>';
                                  }
                                }
                              $table.= '</td>';
                            }
                            if($value['dia_semana'] > 0 
                                && 
                              $value['dia_semana'] < 6){
                              $table .=  '<td>';
                                if (!empty($value['data_integral'])) {
                                  if (strcmp($value['data_feriado'],"true") === 0) {
                                    $table .= '<button  class="btn btn-danger" data-valordata="'.$value['data_integral'].'">'.$value['data_integral'].'</button>';
                                  }else{
                                    $table .= '<button class="btn btn-success" data-valordata="'.$value['data_integral'].'">'.$value['data_integral'].'</button>';
                                  }
                                }else{
                                  $table .= ' ';
                                }
                              $table.= '</td>';
                            }

                          }
                          echo $table;
                        }else{
                          echo '
                          <tr>
                            <td colspan="5">'.$datas.'</td>
                          </tr>
                          ';
                        }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
  
<script>    
    $("#data_inicio_periodo").datepicker({
          showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd/mm/yy'
      });
      $("#data_final_periodo").datepicker({
          showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd/mm/yy'
      });
    $(".fa-search").on('click',function(){
      sendTo(base_url + 'calendario/explorarPeriodo',{idPeriodo:$(this).data('idperiodo')},'POST');
    });
  $("button").on('click',function(){
    if($(this).hasClass("btn-danger")){
      $(this).removeClass("btn-danger").addClass("btn-success");
      sendTo(base_url + 'calendario/feriado',{id_periodo:<?php echo($id_periodo);?>,data_calendario:$(this).data('valordata'),func:"removeFeriado"},"POST");
    }else{
      $(this).removeClass("btn-success").addClass("btn-danger");
      sendTo(base_url + 'calendario/feriado',{id_periodo:<?php echo($id_periodo);?>,data_calendario:$(this).data('valordata'),func:"addFeriado"},"POST");
    }

  });
</script>
