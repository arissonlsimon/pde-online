<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-12">
              <div class="form-group col-md-8">
                  <b class="text-left" style="font-size:13px">
                    <a target="_self" data-toggle="tooltip" data-placement="top" title="Nome da disciplina. Dica: Procure usar o nome por extenso"><span class="glyphicon glyphicon-question-sign"></span></a>
                    <label for="nome">Nome</label>
                  </b>
                  <input type="text" class="form-control" name="nome_disciplina" id="nome_disciplina" data-required="Informe o nome da disciplina" value="<?php echo @$disciplina['nome_disciplina'] ?>">
              </div>
              <div class="form-group col-md-4">
                <b class="text-left" style="font-size:13px">
                    <a target="_self" data-toggle="tooltip" data-placement="top" title="Está disciplina possui a carga horária de quanto? Use somente números"><span class="glyphicon glyphicon-question-sign"></span></a>
                    <label for="nome">Carga horária</label>
                  </b>
                  <select class="form-control" name="cargaHoraria" id="cargaHoraria" data-required="Selecione a carga horária">
                    <option value="<?php echo @$disciplina['cargahoraria_disciplina'] ?>"><?php echo @$disciplina['cargahoraria_disciplina'] ?></option>
                    <option value="10">10h</option>
                    <option value="20">20h</option>
                    <option value="30">30h</option>
                    <option value="40">40h</option>
                    <option value="60">60h</option>
                    <option value="80">80h</option>
                    <option value="100">100h</option>
                    <option value="120">120h</option>
                    <option value="160">160h</option>
                  </select>
              </div>
          </div>
          <div class="col-lg-12">
            <div class="form-group col-md-12">
              <b class="text-left" style="font-size:13px">
                &nbsp; <a target="_self" data-toggle="tooltip" data-placement="top" title="Procure e adicione o(os) professores desta disciplina"><span class="glyphicon glyphicon-question-sign"></span></a>
                Professores
              </b> 
              <input autocomplete="off" type="text" class="form-control" name="professores" id="professores" data-required="Informe os professores deste curso" value="<?php echo @$disciplina['id_usuario'] ?>">
            </div>
          </div> 
          <div class="col-lg-12">
            <div class="form-group col-md-12">
              <b class="text-left" style="font-size:13px">
                &nbsp; <a target="_self" data-toggle="tooltip" data-placement="top" title="Procure e adicione o(os) curso(s) que oferecem esta disciplina. Caso não apareça a disciplina cadastre-a"><span class="glyphicon glyphicon-question-sign"></span></a>
                Cursos
              </b> 
              <input autocomplete="off" type="text" class="form-control" name="cursos" id="cursos" data-required="Informe a qual curso(s) esta disciplina pertence." value="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function () {
    $("#professores").tokenInput("<?php echo base_url(); ?>usuario/buscar/tokenInput", {
      tokenLimir    :2,
      method        : 'POST',
      queryParam      : 'busca',
      searchingText     : "Procurando...",
      hintText        : "Quem você procura?",
      noResultsText     : "Nenhum resultado encontrado.",
      theme           : "facebook",
      <?php 
      if(!empty($curso[$CURCod]['CURCoordenador'])) {
        echo ',prePopulate: [';
        foreach($curso[$CURCod]['CURCoordenador'] as $key => $value) {
          $coordenadores[] = '{
              USUCod: '.$curso[$CURCod]['CURCoordenador'][$key]['USUCod'].',
              USUNome: "'.$curso[$CURCod]['CURCoordenador'][$key]['USUNome'].'",
              USUEmail: "'.$curso[$CURCod]['CURCoordenador'][$key]['USUEmail'].'",
              USUImagem: "'.$curso[$CURCod]['CURCoordenador'][$key]['USUImagem'].'"
            }';
        }
        echo implode(',',$coordenadores).']';
      }
      ?>
      tokenFormatter    : function(usuario) {
        return '<li><div style="display:inline-block;padding-left:10px;"><div class="full_name">'+usuario.nome+'</div><div class="email>'+usuario.email+'</div></div></li>';
      },
      resultsFormatter  : function(usuario) {
        return '<li><div style="display:inline-block;padding-left:10px;"><div class="full_name">'+usuario.nome+'</div><div class="email">'+usuario.email+'</div></div></li>';
      },
      preventDuplicates : true,
      propertyToSearch : 'id_usuario',
      tokenValue : 'id_usuario'
    });

    $("#cursos").tokenInput("<?php echo base_url(); ?>curso/buscar/tokenInput", {
      method        : 'POST',
      queryParam      : 'busca',
      searchingText     : "Procurando...",
      hintText        : "Qual curso você procura?",
      noResultsText     : "Nenhum resultado encontrado.",
      theme           : "facebook",
      tokenFormatter    : function(curso) {
        return '<li><div style="display:inline-block;padding-left:10px;"><div class="full_name">'+curso.nome_curso+'</div></div></li>';
      },
      resultsFormatter  : function(curso) {
        return '<li><div style="display:inline-block;padding-left:10px;"><div class="full_name">'+curso.nome_curso+'</div></div></li>';
      },
      preventDuplicates : true,
      propertyToSearch : 'id_curso',
      tokenValue : 'id_curso'
    });
  });
</script>