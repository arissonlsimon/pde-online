
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Calendário</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel panel-heading">
              Períodos
              <div class="pull-right">
                <button type="button" data-toggle="modal" data-target="#modalCadastraNovo" class="btn btn-primary btn-xs"><span class="fa fa-plus"></span> Cadastrar</button>
              </div>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Nome</th>
                      <th>Código</th>
                      <th>Inicio</th>
                      <th>Fim</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      if (isset($periodo)) {
                        if(is_array($periodo)){
                          foreach ($periodo as $key => $value) {
                            // echo '
                            //   <tr>
                            //     <td>
                            //       '.$value['nome_periodo'].'
                            //     </td>
                            //     <td>
                            //     '.$value['abreviacao_periodo'].'
                            //     </td>
                            //     <td>
                            //       '.date('d/m/Y',($value['data_inicio_periodo'])).'
                            //     </td>
                            //     <td>
                            //         '.date('d/m/Y',($value['data_final_periodo'])).'
                            //     </td>
                            //     <td>
                            //         <button class="btn btn-link fa fa-pencil" data-toggle="tooltip" data-placement="top" data-idperiodo="'.$value['id_periodo'].'" data-original-title="Editar período"/>
                            //         <form action="'.base_url().'calendario/explorarPeriodo" method="POST">
                            //           <input type="text" value="'.$value['id_periodo'].'" name="idPeriodo" hidden="true" />
                            //           <button type="submit" class="btn btn-link fa fa-search" data-toggle="tooltip" data-placement="top" data-original-title="Visualizar Datas do período"/>
                            //         </form>
                            //           <button type="submit" class="btn btn-link fa fa-trash" data-toggle="tooltip" data-placement="top" data-original-title="Remover período" style="color:red;" data-idperiodo="'.$value['id_periodo'].'"/>
                            //     </td>
                            //   </tr>
                            // '; 
                            echo '
                            <tr>
                              <td>
                                '.$value['nome_periodo'].'
                              </td>
                              <td>
                              '.$value['abreviacao_periodo'].'
                              </td>
                              <td>
                                '.($value['data_inicio_periodo']).'
                              </td>
                              <td>
                                  '.$value['data_final_periodo'].'
                              </td>
                              <td>
                                  <button class="btn btn-link fa fa-pencil" data-toggle="tooltip" data-placement="top" data-idperiodo="'.$value['id_periodo'].'" data-original-title="Editar período"/>
                                  <form action="'.base_url().'calendario/explorarPeriodo" method="POST">
                                    <input type="text" value="'.$value['id_periodo'].'" name="idPeriodo" hidden="true" />
                                    <button type="submit" class="btn btn-link fa fa-search" data-toggle="tooltip" data-placement="top" data-original-title="Visualizar Datas do período"/>
                                  </form>
                                    <button type="submit" class="btn btn-link fa fa-trash" data-toggle="tooltip" data-placement="top" data-original-title="Remover período" style="color:red;" data-idperiodo="'.$value['id_periodo'].'"/>
                              </td>
                            </tr>
                          ';
                          }
                        }else{
                          echo '
                          <tr>
                            <td colspan="5">'.$periodo.'</td>
                          </tr>
                          ';
                        }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->


<form onsubmit="return false;" name="cadastroUsuario" id="cadastroUsuario" method="POST" data-ajax="true" action="<?php echo base_url(); ?>calendario/cadastraPeriodo">
  <div class="modal fade" id="modalCadastraNovo" tabindex="-1" role="dialog" aria-labelledby="modalCadastraRegraLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="modalCadastraRegraLabel">Cadastro de Período</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row">
              <div class="form-group col-md-12">
                <b class="text-left" style="font-size:13px">
                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Informe o nome do Período"><span class="glyphicon glyphicon-question-sign"></span></a>
                  <label for="nome_periodo">Nome do Período</label>
                </b>
                <input type="nome_periodo" class="form-control" name="nome_periodo" id="nome_periodo" data-required="Informe o nome do Período" value="<?php echo @$altData['nome_periodo']?>" required>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-12">
                <b class="text-left" style="font-size:13px">
                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Código do período ex.: '2018/1', '2018/2'"><span class="glyphicon glyphicon-question-sign"></span></a>
                  <label for="abreviacao_periodo">Código do Período</label>
                </b>
                <input type="text" class="form-control" name="abreviacao_periodo" id="abreviacao_periodo" data-required="Informe o Código do período" value="<?php echo @$altData['abreviacao_periodo'] ?>">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-12">
                <b class="text-left" style="font-size:13px">
                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Informe a data de início do período"><span class="glyphicon glyphicon-question-sign"></span></a>
                  <label for="data_inicio_periodo">Data de Início</label>
                </b>
                <input type="text" class="form-control" name="data_inicio_periodo" id="data_inicio_periodo" data-required="Informe a data de início do período" value="<?php echo @$altData['data_inicio_periodo'] ?>">
              </div>
            </div>

            <div class="row">
              <div class="form-group col-md-12">
                <b class="text-left" style="font-size:13px">
                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Informe a data de início do período"><span class="glyphicon glyphicon-question-sign"></span></a>
                  <label for="data_final_periodo">Data de Fim</label>
                </b>
                <input type="text" class="form-control" name="data_final_periodo" id="data_final_periodo" data-required="Informe a data de início do período" value="<?php echo @$altData['data_final_periodo'] ?>">
              </div>
            </div>

          </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" name="btnCancelar" id="btnCancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-success">Salvar</button>
        </div>
      </div>
    </div>
  </div>
</form>
  
<script>    
  $("#data_inicio_periodo").datepicker({
      showOtherMonths: true,
    selectOtherMonths: true,
    dateFormat: 'dd/mm/yy'
  });
  $("#data_final_periodo").datepicker({
      showOtherMonths: true,
    selectOtherMonths: true,
    dateFormat: 'dd/mm/yy'
  });
  $(".fa-trash").click(function(){
    sendTo(base_url + 'calendario/deletarPeriodo',{id_periodo:$(this).data("idperiodo")},"POST");
  });

  $(".fa-pencil").click(function(){
    var dados = <?php echo json_encode($periodo);?>;
    var i = $(this).data('idperiodo');
    
    $.each(dados,function(index,data){
      if(data.id_periodo == i){
        $("#nome_periodo").val(data.nome_periodo);
        $("#abreviacao_periodo").val(data.abreviacao_periodo);
        $("#data_inicio_periodo").val(data.data_inicio_periodo);
        $("#data_final_periodo").val(data.data_final_periodo);
        $("#cadastroUsuario").attr('action',base_url+'calendario/alterar').append('<input type="hidden" name="id_periodo" id="id_periodo" value="'+i+'" />');
        $("#modalCadastraNovo").modal('show');
      }
    })
  })
</script>
