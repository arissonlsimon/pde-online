<?php
    if(!$this -> functions -> checkPermissao(array('Administrador', 'Pedagógico', 'Coordenador'),$this -> session -> userdata("nome_regra"))){
        redirect(base_url());
    }
    $size_sem_avalicao = count($sem_avaliacao);
    $size_publicados = count($publicados);
    $size_pendentes = count($pendentes);
    $size_em_criacao = count($em_criacao);

    $da_do_s[0] = $sem_avaliacao;
    $da_do_s[1] = $publicados;
    $da_do_s[2] = $pendentes;
    $da_do_s[3] = $em_criacao;
    // $this -> functions -> pre($da_do_s);
    // die('dfhsdh');
    $titulo = '';
    if($this -> functions -> checkPermissao(array('Administrador'),$this -> session -> userdata("nome_regra"))){
        $titulo = 'Administrador';
    }
    if($this -> functions -> checkPermissao(array('Coordenador'),$this -> session -> userdata("nome_regra"))){
        $titulo = 'Coordenador';
    }
    if($this -> functions -> checkPermissao(array('Pedagógico'),$this -> session -> userdata("nome_regra"))){
        $titulo = 'Pedagógico';
    }
?>
<div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <br/>
                    <div class="alert alert-info alert-dismissable">
                        <p style="text-align:center;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle" aria-hidden="true" style="float:left;font-size:20px;"></i>
                            Baixe o manual de uso do sistema clicando <a href="<?php echo public_url;?>/manual_do_sistema_PdE.pdf" target="_blank" class="alert-link">AQUI!</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Painel <?php echo $titulo;?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6" >
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-book fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $size_sem_avalicao;?></div>
                                    <div>Não revisados</div>
                                </div>
                            </div>
                        </div>
                        <a href="#n_revisados" data-toggle="collapse" id="Nrevisado" role="button" aria-controls="n_revisados" aria-expanded="false">
                            <div class="panel-footer">
                                <span class="pull-left">Visualizar</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-check fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $size_publicados;?></div>
                                    <div>Planos Publicados</div>
                                </div>
                            </div>
                        </div>
                        <a href="#avaliados" data-toggle="collapse" id="avaliados-coll" role="button" aria-controls="avaliados" aria-expanded="false">
                            <div class="panel-footer">
                                <span class="pull-left">Visualizar</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-warning fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $size_pendentes;?></div>
                                    <div>
                                        <?php
                                        if($this -> functions -> checkPermissao(array('Administrador'),$this -> session -> userdata("nome_regra"))){
                                            echo 'Pendentes Pedagógico';
                                        }else{
                                            echo 'Planos pendentes';
                                            }?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="#pendentes" data-toggle="collapse" id="pendentes-coll" role="button" aria-controls="pendentes" aria-expanded="false">
                            <div class="panel-footer">
                                <span class="pull-left">Visualizar</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-th-large  fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $size_em_criacao;?></div>
                                    <div>Planos em criação</div>
                                </div>
                            </div>
                        </div>
                        <a href="#e_desenvolvimento" data-toggle="collapse" id="desenvolvimento" role="button" aria-controls="e_desenvolvimento" aria-expanded="false">
                            <div class="panel-footer">
                                <span class="pull-left">Visualizar</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                        <div class="tab-content">
                            <div class="collapse" id="n_revisados" aria-labelledby="Nrevisado">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Não revisados   
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                            if(!empty($sem_avaliacao)){
                                                echo '
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Plano</th>
                                                                <th>Disciplina</th>
                                                                <th>Professor</th>
                                                                <th>Data de recebimento<th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                ';
                                                $count = 1;
                                                foreach ($sem_avaliacao as $key => $value) {
                                                    echo '<tr>';
                                                    echo '
                                                        <td>'.$count.'</td>
                                                        <td>'.$value['nome_plano'].'</td>
                                                        <td>'.$value['nome_disciplina'].'</td>
                                                        <td>'.$value['nome'].'</td>
                                                        <td>'.(!empty($value['data_alteracao_plano'])?date('d/m/Y',$value['data_alteracao_plano']):'').'</td>
                                                        <td>
                                                            '.($this->functions->checkPermissao(array('Pedagógico'),$this->session->nome_regra)?'':'<a href="'.base_url().'plano/EditarPlano/'.$value['id_plano'].'" class="btn btn-primary fa fa-pencil"></a>').'
                                                        </td>
                                                    ';
                                                    echo '</tr>';
                                                    $count++;
                                                }
                                                echo '  </tbody>
                                                    </table>
                                                </div>
                                                ';
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="collapse" id="avaliados" aria-labelledby="avaliados-coll">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Avaliados   
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                            if(!empty($publicados)){
                                                echo '
                                                <div class="table-responsive">
                                                    <table class="table tab-table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Plano</th>
                                                                <th>Disciplina</th>
                                                                <th>Professor</th>
                                                                <th>Data de publicação</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                ';
                                                $count = 1;
                                                foreach ($publicados as $key => $value) {
                                                    echo '<tr>';
                                                    echo '
                                                        <td>'.$count.'</td>
                                                        <td>'.$value['nome_plano'].'</td>
                                                        <td>'.$value['nome_disciplina'].'</td>
                                                        <td>'.$value['nome'].'</td>
                                                        <td>'.(!empty($value['data_alteracao_plano'])?date('d/m/Y',$value['data_alteracao_plano']):'').'</td>
                                                    ';
                                                    echo '</tr>';
                                                    $count++;
                                                }
                                                echo '  </tbody>
                                                    </table>
                                                </div>
                                                ';
                                            }
                                        ?>  
                                    </div>
                                </div>
                            </div>
                            <div class="collapse" id="pendentes" aria-labelledby="pendentes-coll">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Pendentes   
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                            if(!empty($pendentes)){
                                                echo '
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Plano</th>
                                                                <th>Disciplina</th>
                                                                <th>Professor</th>
                                                                <th>Data da primeira avaliação</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                ';
                                                $count = 1;
                                                foreach ($pendentes as $key => $value) {
                                                    echo '<tr>';
                                                    echo '
                                                        <td>'.$count.'</td>
                                                        <td>'.$value['nome_plano'].'</td>
                                                        <td>'.$value['nome_disciplina'].'</td>
                                                        <td>'.$value['nome'].'</td>
                                                        <td>'.(!empty($value['data_alteracao_plano'])?date('d/m/Y',$value['data_alteracao_plano']):'').'</td>
                                                        <td>
                                                            <a href="'.base_url().'plano/EditarPlano/'.$value['id_plano'].'" class="btn btn-primary fa fa-pencil"></a>
                                                        </td>
                                                    ';
                                                    echo '</tr>';
                                                    $count++;
                                                }
                                                echo '  </tbody>
                                                    </table>
                                                </div>
                                                ';
                                            }
                                        ?>  
                                    </div>
                                </div>
                            </div>
                            <div class="collapse" id="e_desenvolvimento" aria-labelledby="desenvolvimento">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Em desenvovlimento  
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                            if(!empty($em_criacao)){
                                                echo '
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Plano</th>
                                                                <th>Disciplina</th>
                                                                <th>Professor</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                ';
                                                $count = 1;
                                                foreach ($em_criacao as $key => $value) {
                                                    echo '<tr>';
                                                    echo '
                                                        <td>'.$count.'</td>
                                                        <td>'.$value['nome_plano'].'</td>
                                                        <td>'.$value['nome_disciplina'].'</td>
                                                        <td>'.$value['nome'].'</td>
                                                    ';
                                                    echo '</tr>';
                                                    $count++;
                                                }
                                                echo '  </tbody>
                                                    </table>
                                                </div>
                                                ';
                                            }
                                        ?>  
                                    </div>
                                </div>    
                            </div>
                        </div>
                    
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Status de serviço
                        </div>
                        <div class="panel-body">
                            <div id="grafico-planos"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <!-- /.col-lg-8 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<script>
$(function() {

    Morris.Donut({
        element: 'grafico-planos',
        data: [{
            label: "Publicados",
            value: <?php echo $size_publicados;?>
        }, {
            label: "Pendentes",
            value: <?php echo $size_pendentes;?>
        }, {
            label: "Em desenvolvimento",
            value: <?php echo $size_em_criacao;?>
        }, {
            label: "Sem avaliação",
            value: <?php echo $size_sem_avalicao;?>
        }],
        resize: true
    });
}) 
</script>