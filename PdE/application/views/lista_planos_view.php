<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
        <div class="col-lg-12">
          <h1 class="page-header">Planos de Ensino da disciplina : <?php echo $nome_disciplina;?></h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <?php 
        if($this->session->has_userdata('erro_print'))
        echo '<div class="alert alert-warning">
          '.$this->session->erro_print.'
        </div>';
      ?>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel panel-heading">
              Lista de Planos de Ensino
            </div>
            <div class="panel-body">
              <table class="table table-striped table-bordered table-hover" width="100%" id="dataTable-plano">
                <thead>
                  <tr>
                    <th>
                      Nome
                    </th>
                    <th>
                      Periodo
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    if(is_array($planos)){
                      foreach($planos as $value){
                        echo '<tr>
                              <td>';
                        echo $value['nome_plano'];
                        echo '</td>
                              <td>'
                              .$value['nome_periodo'];
                        echo '</td>
                              <td style="width:5px; text-align:center;">
                                <a href="'.base_url().'inicio/impressaoPlano/'.$value['id_plano'].'" class="btn btn-link fa fa-download" target="_blank"></a>
                              </td>
                              </tr>';
                      }
                    }else{
                      echo '
                        <tr>
                          <td colspan="3">
                            '.$planos.'
                          </td>
                        </tr>
                      ';
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
