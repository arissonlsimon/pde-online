<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	// if (!$this->functions->checkPermissao('ADM', $_SESSION) || !$this->functions->checkPermissao('PRF', $_SESSION)) {
	// 	header(base_url());
	// }
?>
<div id="wrapper">
  <!-- Page Content -->
	<div id="page-wrapper">
	    <div class="container-fluid">
			<div class="row">
	        	<div class="col-lg-12">
	        		<h1 class="page-header">Editando Plano de Ensino: <?php echo @$nome_plano?></h1>
	    		</div>
	    	</div>
	    	<?php
	    		if(!empty($modelo)){
	    			// $this -> functions-> pre($modelo);
	    			$modelo = json_decode($modelo['ar_configuracao']);
	    			foreach ($modelo as $key => $value) {
	    				$ar_order[$key] = $value->ordem;
	    			}
	    			asort($ar_order);
	    			//$this -> functions-> pre($modelo);
	    			foreach ($ar_order as $key => $value) {

	    				$arIds[] = $modelo->$key->iddiv;

	    				if($modelo->$key->iddiv == 'cabecalio'){
	    					echo '<div class="row">
				          <div class="col-lg-12" id="'.$modelo->$key->iddiv.'">
				            <div class="panel panel-default">
				              <div class="panel panel-heading">
				                Cabeçalho
				              </div>
											<div class="panel-body" id="cabecalho">
												<div class="row">
													<div class="col-lg-12">
														<div class="form-group col-md-12">
															<label for="text">CURSO:</label>
															<select name="cursoID">';
																
																if (isset($curso)) {
																	foreach ($curso as $key => $value) {
																		echo '<option value="'.$value['id_curso'].'">'.$value['nome_curso'].'</option>';
																	}
																}
							echo'
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-lg-12">
														<div class="form-group col-md-4">
															<label for="text">ANO/SEMESTRE:</label>
															<select name="periodoANO">';
															
																if (isset($periodo)) {
																	foreach ($periodo as $key => $value) {
																		echo '<option value="'.$value['id_periodo'].'">'.$value['abreviacao_periodo'].'</option>';
																	}
																}
															
							echo'								</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-lg-12">
														<div class="form-group col-md-4">
															<label for="text">PROFESSOR:</label>
															<input type="text" name="nomeProfessor" value="'.$this->session->nome.'" disabled>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-lg-12">
														<div class="form-group col-md-4">
															<label for="text">DISCIPLINA:</label>
															<select name="disciplina">';

																if (isset($disciplina)) {
																	foreach ($disciplina as $key => $value) {
																		if ($value['id_disciplina'] == $plano['id_disciplina']) {
																			echo '<option value="'.$value['id_disciplina'].'" selected>'.$value['nome_disciplina'].'</option>';
																		}else {
																			echo '<option value="'.$value['id_disciplina'].'">'.$value['nome_disciplina'].'</option>';
																		}
																	}
																}
							echo'					
															</select>
														</div>
													</div>
												</div>
											</div>
				            </div>
				          </div>
				        </div>';
				      
	    				}
	    				if(@$modelo->$key->tipo == 'texto')	{
	    					echo '
	    						<div class="row">
		          			<div class="col-lg-12">
		            			<div class="panel panel-default">
	              				<div class="panel panel-heading">
	                				'.@$modelo->$key->title.'
					            	</div>
												<div class="panel-body">
													<div class="row">
														<div class="col-lg-12" id="'.$modelo->$key->iddiv.'" >
															<div class="editable" contenteditable="true"  >
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
							';
	    				}
	    				if(@$modelo->$key->tipo == 'tabela'){
	    					echo'
	    						<div class="row">
				          			<div class="col-lg-12">
				            			<div class="panel panel-default">
				              				<div class="panel panel-heading">
				                				'.@$modelo->$key->title.'
								            </div>
											<div class="panel-body">';
							if($modelo->$key->iddiv=='SiAva'){
								echo '
												<div class="row">
													<div class="col-lg-12">
														<input type="hidden" name="num-coluna" value="'.$modelo->$key->colnumber.'" data-howstable="'.$modelo->$key->iddiv.'">
														<select class="form-control" id="diaSemana">
															<option value="">Selecione o dia da semana</option>
															<option value="1">Segunda Feira</option>
															<option value="2">Terça Feira</option>
															<option value="3">Quarta Feira</option>
															<option value="4">Quinta Feira</option>
															<option value="5">Sexta Feira</option>
														</select>
													</div>
												</div>	
								';
							}
							echo'				
												<div class="row">
													<div class="col-lg-12" id="'.$modelo->$key->iddiv.'">
							    						<table class="table table-bordered" >
														<thead>
														';
							foreach ($modelo->$key->coltitle as $value) {
								echo' <th> ';
									echo $value;
								echo '</th>';
							}														
							echo'
														</thead>
														<tbody class="'.$modelo->$key->iddiv.'">
														</tbody>
														</table>
														<div class="pull-left">
															<button type="button" class="btn btn-primary"n onclick="openModalRows(`'.$modelo->$key->iddiv.'`,'.$modelo->$key->colnumber.');">
																<i class="fa fa-plus"></i>
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
	    					';	
	    				}


	    			}
	    		}
	    	?>
	    	<div class="row">
	    		<div class="col-md-12">
		    		<div class="panel pull-right">
		    			<button class="btn btn-info fa fa-save">
		    				Salvar
		    			</button>
		    		</div>
		    	</div>
	    	</div>
	    </div>
	</div>
</div>

<!-- MODAL PARA NOVO PLANO EM BRANCO -->
    <div class="modal fade" id="modalNamePlano" role="dialog" aria-labelledby="modalNamePlano" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="row">
              <div class="col-sm-5">
                <h5 class="modal-title">NOVO PLANO DE ENSINO</h5>
              </div>
              <div class="col-sm-7 pull-right">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <form >
              <div class="form-group">
                <label for="text" class="col-form-label">Nome do Plano:</label>
                <input type="text" name="nomePlano"class="form-control" id="nomePlano" required>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-primary" id="criaPlano" >Criar</button>
          </div>
        </div>
      </div>
    </div>
<!-- MODAL NOVO PLANO END -->

<script>

	function openModalRows(thisTable,numCols){
		AddTableRow(numCols,thisTable);
	}
	function AddTableRow(numCols,thisTable,dados=null) {
	    var newRow = $("<tr>");
	    var cols = "";
	    for (var i = 0 ; i < numCols ; i++) {
	    	if(i == 0){
	    		if(dados){
	    			cols += '<td><br>'+dados+'</td>';
	    		}else{
	    			cols += '<td><textarea class="form-control"></textarea></td>'
	    		}
	    	}else{
		    	cols += '<td><textarea class="form-control"></textarea></td>';
	    	}
	    }
	    cols += '<td><br>';
	    cols += '<button onclick="RemoveTableRow(this)" type="button" class="btn btn-danger btn-circle fa fa-trash"></button>';
	    cols += '</td>';

	    newRow.append(cols);
	    $("."+thisTable).append(newRow);

	    return false;
	}
	function RemoveTableRow(handler) {
		var tr = $(handler).closest('tr');

		tr.fadeOut(400, function(){
		tr.remove();
		});

		return false;
	}
	$("#diaSemana").change(function(){
		var id_periodo = $("[name=periodoANO]").val();
		if(id_periodo != undefined){
			$.post(base_url+"calendario/changeDatas",{id_periodo:id_periodo,dia_semana:$(this).val()},function(response){
				var aux = JSON.parse(response);
				var table = $('[name=num-coluna]');
				$("." + table.data('howstable')).empty();
				$.each(aux,function(key,values){
					AddTableRow($('[name=num-coluna]').val(),table.data('howstable'),values);
				});
			}).fail(function(){
				alert("Erro ao carregar as datas");
			});
		}
	});
	var ar_plano = {};
	$(".fa-save").click(function(){
		var ar_id_div = new Array;
		var data_plano = new Array;

		ar_id_div = <?php echo json_encode($arIds);?>;
		$.each(ar_id_div,function(key,values){
			ar_plano[values] = ({iddiv:values,dados:$("#"+values).html()});
		});

		console.log(ar_plano);

		$("#modalNamePlano").modal('show');
	});

	$("#criaPlano").click(function(){
		sendTo(base_url + "plano/EditarPlano",{ar_plano:ar_plano,nome_plano:$("#nomePlano").val(),id_periodo:$("[name=periodoANO]").val(),id_disciplina:$("[name=disciplina]").val()},"POST");
	});

</script>
