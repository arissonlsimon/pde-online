<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	if (!$this->functions->checkPermissao('Administrador', $_SESSION) || !$this->functions->checkPermissao('Professor', $_SESSION)) {
		header(base_url());
	}
	// $this -> functions-> pre(($modelo['ar_configuracao']));
	// die();
?>
<form action="<?php echo base_url();?>plano/novoPlano" method="POST" >
  <div class="form-group col-lg-12">
    <label for="text" class="col-form-label">Nome do Plano:</label>
    <input type="text" name="nome_plano"class="form-control" id="nome_plano" data-required="Nome do Plano" required oninvalid="this.setCustomValidity('Preencha esse campo')">
  </div>
  <div class="form-group col-lg-12">
    <label for="text" class="col-form-label">Curso:</label>
    <select class="form-control" name="curso" id="curso" data-required="Selecione o curso" required oninvalid="this.setCustomValidity('Selecione esse campo')">
      <option value="">Selecione</option>
      <?php
        if(!empty($curso)) {
          foreach($curso as $key => $value) {
            echo '<option value="'.$value['id_curso'].'">'.$value['nome_curso'].'</option>';
          }
        }
      ?>
    </select>
  </div>
  <div class="form-group col-lg-12">
    <label for="text" class="col-form-label">Disciplina:</label>
    <select class="form-control" name="disciplina" id="disciplina" data-required="Selecione A disciplina" required oninvalid="this.setCustomValidity('Selecione esse campo')">
      
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="text" class="col-form-label">Período:</label>
    <select class="form-control" name="periodo" id="periodo" data-required="Selecione o periodo" required oninvalid="this.setCustomValidity('Selecione esse campo')">
      <option value="">Selecione</option>
      <?php
        if(!empty($periodo)) {
          foreach($periodo as $key => $value) {
            echo '<option value="'.$value['id_periodo'].'">'.$value['nome_periodo'].'</option>';
          }
        }
      ?>
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="text" class="col-form-label">Categoria:</label>
    <select class="form-control" name="categoria" id="categoria" data-required="Selecione uma categoria de ensino!" required oninvalid="this.setCustomValidity('Campo obrigatório')"> 
      <option value="Graduação">Graduação</option>
      <option value="Pós-Graduação">Pós-Graduação</option>
      <option value="Ensino Médio">Ensino Médio</option>
      <option value="Técnico">Técnico</option>
    </select>
  </div>
  <div class="form-group col-md-2 col-md-offset-5">
    <button class="btn btn-success btn-block" type="submit" id="manda">Criar</button>
  </div>
</form>  
<script>
$("#curso").change(function(){
  var val = $(this).val();
  if('' != val){
    $("#disciplina").empty();
    $.post(base_url + "disciplina/getDisciplinaByCourse",{id_curso:val},function(resp){
      var response = JSON.parse(resp);
      $.each(response,function(key,value){
        $("#disciplina").append("<option value='"+value.id_disciplina+"'>"+value.nome_disciplina+"</option>")
      });
    });
  }
})
$(document).ready(function(){
  
})
</script>