<?php
  
  if(!$this -> session -> has_userdata('id_usuario')){
    header("Location:".base_url());
  }
  if(!empty($plano)){
    $ar_dados = json_decode($plano['dados_plano']);
  }
  
  $doc_html = "";

  $doc_html .= '
    <div class="paginas">
      <div class="inicio">
        <img src="'.public_url.'/imagens/logo_ifrs_documento.png" width="29%">
        <br/>
        <br/>
        <br/>
        <h3>
          PLANO DE ENSINO
        </h3>
        <br/>
        <br/>
      </div>
  ';
  if(!empty($ar_dados)){
    $doc_html .= '
      <div class="conteudo">
    ';
    foreach ($ar_dados as $key => $value) {
      $aux = $value->iddiv;
      if($value->iddiv == 'cabecalho'){
        $doc_html .= '
            <table class="cabecalho" >
        ';
        $doc_html .='
          <tr>
            <td valign="top" style="border: 1px solid #000000; padding: 0in 0.08in">
              <p style="margin-bottom: 0in; orphans: 2; widows: 2">
                <font face="Arial, serif">
                  <font size="2" style="font-size: 10pt">
                    <b>CURSO:</b> '.$curso['curso']['nome_curso'].' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>ANO/SEMESTRE:</b> '.$periodo['abreviacao_periodo'].' 
                  </font>
                </font>
              </p>
            </td>
            
          </tr>
          <tr>
            <td width="571" valign="top" style="border: 1px solid #000000; padding: 0in 0.08in">
              <p style="orphans: 2; widows: 2">
                <font face="Arial, serif">
                  <font size="2" style="font-size: 10pt">
                    <b>PROFESSOR:</b> '.$this->session->nome.'
                  </font>
                </font>
              </p>
            </td>
          </tr>
          <tr style="border:none;">
            <td width="571" valign="top" style="border: 1px solid #000000; padding: 0in 0.08in">
              <p style="orphans: 2; widows: 2">
                <font face="Arial, serif">
                  <font size="2" style="font-size: 10pt">
                    <b>PROCOMPONENTE CURRICULAR:</b> '.$disciplina['disciplina']['nome_disciplina'].'
                    </font>
                </font>
              </p>
            </td>
          </tr>
          <tr style="border:none;">
            <td width="571" valign="top" style="border: 1px solid #000000; padding: 0in 0.08in">
              <p style="orphans: 2; widows: 2">
                <font face="Arial, serif">
                  <font size="2" style="font-size: 10pt">
                    <b>PPC/RESOLUÇÃO Nº:</b> 109 de 13 de dezembro de 2016
                    </font>
                </font>
              </p>
            </td>
          </tr>
          <tr>
            <td width="571" valign="top" style="border: 1px solid #000000; padding: 0in 0.08in">
              <p style="orphans: 2; widows: 2">
                <font face="Arial, serif">
                  <font size="2" style="font-size: 10pt">
                    <b>CARGA HORÁRIA SEMESTRAL:</b> '.$disciplina['disciplina']['cargahoraria_disciplina'].' h/r
                  </font>
                </font>
              </p>
            </td>
          </tr>
          <tr>
            <td width="571" valign="top" style="border: 1px solid #000000; padding: 0in 0.08in">
              <p style="orphans: 2; widows: 2">
                <font face="Arial, serif">
                  <font size="2" style="font-size: 10pt">
                    <b>PERÍODO:</b> '.$periodo['nome_periodo'].'
                  </font>
                </font>
              </p>  
            </td>
          </tr>
        ';
        $doc_html .='
            </table>
        ';
      }else{
        if($value->tipo == 'texto'){
          $doc_html .= 	'
            <br/>
            <table class="texto">
              <tr >
                <td width="571" valign="top" bgcolor="#e0e0e0" style="background: #e0e0e0" style="border: 1px solid #000000; padding: 0in 0.08in">
                  <p style="orphans: 2; widows: 2">
                    <font face="Arial, serif">
                      <font size="2" style="font-size: 10pt">
                        <b style="text-transform: uppercase;">
                        '.@$modelo['ar_configuracao']->$aux->title.' 
                        </b>
                      </font>
                    </font>
                  </p>
                </td>
              </tr>
              <tr >
              <td width="571" valign="top" style="border: 1px solid #000000; padding: 0in 0.08in">
                <p align="justify">
                  <font face="Times New Roman, serif">
          ';
          $doc_html .=	@$value->dados;
          $doc_html .= '
                    </font>
                  </p>
                </td>
              </tr>
            </table>
          ';
        }
        if($value->tipo == 'tabela'){
          $doc_html .= '
            <br/>
            <table class="tabela" >
              <tr >
                <td width="571" valign="top" bgcolor="#e0e0e0" style="background: #e0e0e0" style="border: 1px solid #000000; padding: 0in 0.08in">
                  <p style="orphans: 2; widows: 2">
                    <font face="Arial, serif">
                      <font size="2" style="font-size: 10pt">
                        <b style="text-transform: uppercase;">
                        '.@$modelo['ar_configuracao']->$aux->title.' 
                        </b>
                      </font>
                    </font>
                  </p>
                </td>
              </tr>
            
          ';
          $doc_html .='
              <tr>
              <td width="571" valign="top" style=" padding:0;">
                <p align="justify">
                <table>
                  <tr>
          ';
          foreach ($value->dados->nome_col as $th) {
            $doc_html .= '
                    <td width="100%" valign="top" bgcolor="#e0e0e0"  style="background: #e0e0e0; border: 1px solid #000000; padding: 0in 0.08in">
                      <p align="justify">
                        <font face="Times New Roman, serif">
                          <b style="text-transform: uppercase;">
            ';
            $doc_html .= $th;
            $doc_html .= '
                          </b>
                        </font>
                      </p>
                    </td>
            ';
          }
          $doc_html .= '
                  </tr>
          ';
          foreach ($value->dados->val_row as $arr_cols) {
            $doc_html .= '
              <tr>
            ';
            for ($i=0; $i < count($value->dados->nome_col); $i++) { 
              $doc_html .= '
                <td width="100%" valign="top" style="border: 1px solid #000000; padding: 0in 0.08in">
                  <p style="orphans: 2; widows: 2">
                    <font face="Arial, serif">
                      <font size="2" style="font-size: 10pt">
                        '.$arr_cols[$i].'
                      </font>
                    </font>
                  </p>
                </td>
              ';
            }
            $doc_html .= '
              </tr>
            ';
          }
          $doc_html .= '
                </table>
                </p>
                </td>
              </tr>
            </table>
            <br/>
            <br/>
            <br/>
            <br/>
        </div>
        
          ';
        }
      }
    }
  
    // die('CRY');
    $doc_html .= '
    <p style="text-align:center;">
    ______________________________________________________________
    <br/>
    Professor
    <br/>

    ______________________________________________________________
    <br/>
    Coordenação de Curso
    <br/>

    ______________________________________________________________
    <br/>
    Supervisão Pedagógica
    </p>
    ';
  }
  $doc_html .='
    </div>
  ';
// $this->functions->pre($plano,true);
// echo $doc_html; die();

$mpdf = new mPDF('','', 0, '', 9, 9, 25, 5, 5, 5, 'L');
$mpdf->SetFont('Arial');
$mpdf -> debug = true;
$mpdf -> allow_output_buffering = true;
$css = file_get_contents(public_url.'/css/print.css');
$mpdf -> WriteHTML($css,1);
$mpdf -> WriteHTML($doc_html);
ob_clean();
$nomeArquivo =  $plano['nome_plano']."_PFD";
$mpdf -> Output($nomeArquivo, 'I');
?>