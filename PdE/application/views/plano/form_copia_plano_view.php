<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	if (!$this->functions->checkPermissao('Administrador', $_SESSION) || !$this->functions->checkPermissao('Professor', $_SESSION)) {
		header(base_url());
	}
	// $this -> functions-> pre(($modelo['ar_configuracao']));
	// die();
?>
<form action="<?php echo base_url();?>plano/copiaPlano" method="POST">
  <div class="form-group col-lg-12">
    <label for="text" class="col-form-label">Nome do Plano:</label>
    <input type="text" name="nome_plano"class="form-control" id="nome_plano" data-required="Nome do Plano" required>
  </div>
  <div class="form-group col-lg-12">
    <label for="text" class="col-form-label">Curso:</label>
    <select class="form-control" name="curso" id="curso" data-required="Selecione o curso" required>
      <option value="">Selecione</option>
      <?php
        if(!empty($curso)) {
          foreach($curso as $key => $value) {
            echo '<option value="'.$value['id_curso'].'">'.$value['nome_curso'].'</option>';
          }
        }
      ?>
    </select>
  </div>
  <div class="form-group col-lg-12">
    <label for="text" class="col-form-label">Disciplina:</label>
    <select class="form-control" name="disciplina" id="disciplina" data-required="Selecione A disciplina" required>
      
    </select>
  </div>
  <div class="form-group col-lg-6">
    <label for="text" class="col-form-label">Período:</label>
    <select class="form-control" name="periodo" id="periodo" data-required="Selecione o periodo" required>
      <option value="">Selecione</option>
      <?php
        if(!empty($periodo)) {
          foreach($periodo as $key => $value) {
            echo '<option value="'.$value['id_periodo'].'">'.$value['nome_periodo'].'</option>';
          }
        }
      ?>
    </select>
  </div>
  <div class="form-group col-lg-2 ">
    <label for="text">Dia da semana:</label>
    <select class="form-control" name="diasemana" id="diasemana" data-required="Selecione o dia da semana!" required>
      <option value="segunda">Segunda</option>
      <option value="terça">Terça</option>
      <option value="quarta">Quarta</option>
      <option value="quinta">Quinta</option>
      <option value="sexta">Sexta</option>
    </select>
  </div>
  <div class="form-group col-lg-4">
    <label for="text" class="col-form-label">Categoria:</label>
    <select class="form-control" name="categoria" id="categoria" data-required="Selecione uma categoria de ensino!" required>
      <option value="Graduação">Graduação</option>
      <option value="Pós-Graduação">Pós-Graduação</option>
      <option value="Ensino Médio">Ensino Médio</option>
      <option value="Técnico">Técnico</option>
    </select>
  </div>
  <div class="form-group col-lg-12">
    <label for="text" class="col-form-label">Plano:</label>
    <select class="form-control" name="plano_old" id="plano_old" data-required="Selecione uma plano de ensino!" required>
      <?php
        if(!empty($plano)) {
          foreach($plano as $key => $value) {
            echo '<option value="'.$value['id_plano'].'">'.$value['nome_plano'].'</option>';
          }
        }
      ?>
    </select>
  </div>
  <div class="form-group col-md-2 col-md-offset-5">
    <button class="btn btn-success btn-block" type="submit">Criar novo plano</button>
  </div>
</form>  
<script>
$("#curso").change(function(){
  var val = $(this).val();
  if('' != val){
    $("#disciplina").empty();
    $.post(base_url + "disciplina/getDisciplinaByCourse",{id_curso:val},function(resp){
      var response = JSON.parse(resp);
      $.each(response,function(key,value){
        $("#disciplina").append("<option value='"+value.id_disciplina+"'>"+value.nome_disciplina+"</option>")
      });
    });
  }
})
</script>