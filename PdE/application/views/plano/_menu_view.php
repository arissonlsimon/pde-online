<div id="wrapper">
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Criar plano de ensino</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- Menu button -->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="col-lg-6">
                <button type="button" name="button" onclick="configNewPlano();" class="btn btn-info btn-block">Plano em Branco</button>
              </div>
              <div class="col-lg-6">
                <button type="button" name="button" onclick="newPlano('copy')" class="btn btn-info btn-block">Usar plano antigo como base</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end Menu Button -->
      <!-- <?php $this->functions->pre($periodo) ?> -->
      <div class="row">
        <div class="col-lg-12" id="configurationes" style="display: none;">
         
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  function configNewPlano(){
    $.ajax({
      url:base_url+"plano/getMenuConfigNewPlano",
      type:"post",
      cache: false,
      dataType: "json",
      success:function(response){
      $("#configurationes").css("display", "block");
      $("#configurationes").empty();
      $("#configurationes").append(response);
      }
    });
  }
</script>