<!-- MODAL PARA NOVO PLANO EM BRANCO -->
    <div class="modal fade" id="modalNewPlano" role="dialog" aria-labelledby="modalLogin" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="row">
              <div class="col-sm-5">
                <h5 class="modal-title">NOVO PLANO DE ENSINO</h5>
              </div>
              <div class="col-sm-7 pull-right">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <form >
              <div class="form-group">
                <label for="text" class="col-form-label">Nome do Plano:</label>
                <input type="text" name="nomePlano"class="form-control" id="nomePlano" required>
              </div>
              <div class="form-group">
                <label for="text" class="col-form-label">Disciplina:</label>
                <select class="form-control" name="disciplina" id="disciplina" data-required="Selecione o grupo">
                  <option value="">Selecione</option>
                  <?php
                    if(!empty($disciplina)) {
                      foreach($disciplina as $key => $value) {
                        echo '<option value="'.$value['id_disciplina'].'">'.$value['nome_disciplina'].'</option>';
                      }
                    }
                  ?>
                </select>
              </div>
              <div class="form-group">
                <label for="text" class="col-form-label">Período:</label>
                <select class="form-control" name="periodo" id="periodo" data-required="Selecione o grupo">
                  <option value="">Selecione</option>
                  <?php
                    if(!empty($periodo)) {
                      foreach($periodo as $key => $value) {
                        echo '<option value="'.$value['id_periodo'].'">'.$value['nome_periodo'].'</option>';
                      }
                    }
                  ?>
                </select>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-primary" onclick="plano('new')">Criar</button>
          </div>
        </div>
      </div>
    </div>
<!-- MODAL NOVO PLANO END -->
