<div id="wrapper">
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Menu Plano de Ensino</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <div class="row">
      <div class="col-lg-12">
          <div class="panel panel-body">
            <!-- Nav tabs -->
            <!-- <ul class="nav nav-pills ">
              <li> -->
              <div class="col-md-6">
                <a href="#plano_branco" onclick="novoPlano();" data-toggle="tab" class="btn btn-primary btn-block">Plano em Branco</a>
              </div>
              <div class="col-md-6">
                <a href="#copia_plano" onclick="copiaPlano()" data-toggle="tab" class="btn btn-primary btn-block">Usar plano antigo como base</a>
              </div>
          </div>
        </div>
      </div>
      <?php
        if ($this->session->has_userdata("erro_cria_plano")) {
          echo '
          <div class="row">
            <div class="col-lg-12">
              <div class="alert alert-danger">
                '.$this -> session -> erro_cria_plano.'
              </div>
            </div>
          </div>
          ';  
          $this->session->unset_userdata('erro_cria_plano');
        }
      ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-body">
            <div class="tab-content">
              <!-- Novo plano BEGGIN -->
              <div class="tab-pane fade" id="plano_branco">
                
              </div>
              <!-- Novo plano END -->
              <!-- Copiar plano BEGGIN -->
              <div class="tab-pane fade" id="copia_plano">
                      
              </div>
              <!-- Copiar plano END -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  function novoPlano() {
    $.get(base_url + 'plano/getForm/novo',function(response){
      var fm = JSON.parse(response);
      $("#plano_branco").empty();
      $("#plano_branco").append(fm);
    });
  }

  function copiaPlano() {
    $.get(base_url + 'plano/getForm/copia',function(response){
      var fm = JSON.parse(response);
      $("#copia_plano").empty();
      $("#copia_plano").append(fm);
    });
  }
</script>