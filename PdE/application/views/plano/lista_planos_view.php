<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Lista os planos</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel panel-heading">
              Meus Planos
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nome</th>
                      <th>Disciplina</th>
                      <th>Modelo</th>
                      <th>Data de Criação</th>
                      <th>Validado</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      if (isset($dados)) {
                        if(is_array($dados)){
                          foreach ($dados as $key => $value) {
                            echo '
                              <tr>
                                <td>
                                  '.$value['id_plano'].'
                                </td>
                                <td>
                                  '.$value['nome_plano'].'
                                </td>
                                <td>
                                  '.$value['nome_disciplina'].'
                                </td>
                                <td>
                                  '.$value['nome_modelo'].'
                                </td>
                                <td>
                                  '.date('d/m/Y',$value['data_criacao_plano']).'
                                </td>
                                <td style="text-align:center;">';
                               if($this->functions->checkPermissao(array('Professor','Administrador'),$this->session->nome_regra)){
                                if($value['avaliado_coord']){
                                  echo '<span class="fa fa-check" style="color:green;"  data-toggle="tooltip" title="Avaliação do Coordenador"></span>';
                                }else{
                                  echo '<span class="fa fa-close" style="color:red;"  data-toggle="tooltip" title="Avaliação do Coordenador"></span>';
                                }
                                echo' &nbsp';
                                if($value['avaliado_pedagogico']){
                                  echo '<span class="fa fa-check" style="color:green;"  data-toggle="tooltip" title="Avaliação do Setor Pedagógico"></span>';
                                }else{
                                  echo '<span class="fa fa-close" style="color:red;"  data-toggle="tooltip" title="Avaliação do Setor Pedagógico"></span>';
                                }
                                echo' &nbsp';
                                if($value['publicado_plano']){
                                  echo '<span class="fa fa-check" style="color:green;"  data-toggle="tooltip" title="Plano Publicado"></span>';
                                }else{
                                  echo '<span class="fa fa-close" style="color:red;"  data-toggle="tooltip" title="Plano não Publicado"></span>';
                                }
                                echo' &nbsp';
                                if ($value['revisao']) {
                                  echo '<span class="glyphicon glyphicon-warning-sign" style="color:yellow;" data-toggle="tooltip" title="Plano necessita de correção"></span>';
                                }
                                echo'
                                    </td>
                                    <td style="text-align:center;">
                                  ';
                                if(($value['locked'] == 0) || ($value['revisao'])){
                                  echo '<a href="'.base_url("plano/EditarPlano/".$value['id_plano']).'" class="btn btn-link btn-info fa fa-pencil" data-toggle="tooltip" title="Editar Plano"></a>
                                      &nbsp';
                                }
                                if(($value['submit_plano'] == 0) || ($value['revisao'])){
                                  echo '<button class="btn btn-link fa fa-send" data-idplano="'.$value['id_plano'].'" data-toggle="tooltip" title="Submeter plano" name="submit-pde" id="submitpde"></button>
                                      &nbsp';
                                }
                                echo '</td>
                                  <td>
                                    <a href="'.base_url("plano/imprimir/".$value['id_plano']).'" class="btn btn-link fa fa-download" data-toggle="tooltip" title="Baixar como PDF" name="baixaPlano" id="baixarPlano" />
                                  </td>
                                  </tr>
                                ';
                              }
                              if($this->functions->checkPermissao(array('Coordenador'),$this->session->nome_regra)){
                                if($value['avaliado_coord']){
                                  echo '<span class="fa fa-check" style="color:green;"  data-toggle="tooltip" title="Avaliação do Coordenador"></span>';
                                }else{
                                  echo '<span class="fa fa-close" style="color:red;"  data-toggle="tooltip" title="Avaliação do Coordenador"></span>';
                                }
                                echo'
                                    </td>
                                    <td style="text-align:center;">
                                  ';
                                if(($value['avaliado_coord'] == 0) && ($value['submit_plano'])){
                                  echo '
                                    <a href="'.base_url("plano/EditarPlano/".$value['id_plano']).'" class="btn btn-link btn-info fa fa-pencil" data-toggle="tooltip" title="Editar Plano"></a>
                                  ';
                                }
                                if($value['publicado_plano'] || ($value['avaliado_coord'] == 0) || ($value['submit_plano'])){
                                  echo '
                                    &nbsp
                                    <a href="'.base_url("plano/imprimir/".$value['id_plano']).'" class="btn btn-link fa fa-download" data-toggle="tooltip" title="Baixar como PDF" name="baixaPlano" id="baixarPlano" />
                                  ';
                                }
                                echo '</td>
                                  </tr>
                                ';
                              }
                              if($this->functions->checkPermissao(array('Pedagógico'),$this->session->nome_regra)){
                                if($value['avaliado_coord']){
                                  echo '<span class="fa fa-check" style="color:green;"  data-toggle="tooltip" title="Avaliação do Coordenador"></span>&nbsp';                                
                                }else{
                                  echo '<span class="fa fa-close" style="color:red;"  data-toggle="tooltip" title="Avaliação do Coordenador"></span>&nbsp';
                                }
                                if($value['avaliado_pedagogico']){
                                  echo '<span class="fa fa-check" style="color:green;"  data-toggle="tooltip" title="Avaliação do Coordenador"></span>';
                                }else{
                                  echo '<span class="fa fa-close" style="color:red;"  data-toggle="tooltip" title="Avaliação do Coordenador"></span>';
                                }
                                echo'
                                    </td>
                                    <td style="text-align:center;">
                                  ';
                                if(($value['avaliado_pedagogico'] == 0) && ($value['submit_plano']) && ($value['avaliado_coord'])){
                                  echo '<a href="'.base_url("plano/EditarPlano/".$value['id_plano']).'" class="btn btn-link btn-info fa fa-pencil" data-toggle="tooltip" title="Editar Plano"></a>
                                     ';
                                }
                                if($value['publicado_plano'] || ($value['avaliado_pedagogico'] == 0) && ($value['submit_plano'])){
                                echo '
                                    &nbsp
                                    <a href="'.base_url("plano/imprimir/".$value['id_plano']).'" class="btn btn-link fa fa-download" data-toggle="tooltip" title="Baixar como PDF" name="baixaPlano" id="baixarPlano" />
                                  ';
                                }
                                echo '</td>
                                  </tr>
                                ';
                              }
                          }
                        }else{
                          echo '
                          <tr>
                            <td colspan="6">'.$dados.'</td>
                          </tr>
                          ';
                        }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<script>
  $(".fa-send").click(function(){
    sendTo(base_url + "plano/SubmitPlano",{id_plano:$(this).data('idplano')},"POST");
  });
</script>