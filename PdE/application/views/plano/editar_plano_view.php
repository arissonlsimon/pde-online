<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	if (!$this->session->has_userdata('id_usuario')) {
		header(base_url());
	}
	// $this -> functions-> pre(($plano),true);
	// die();
?>

<div id="wample">
    <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Plano de Ensino => <?php echo (!empty($plano))? $plano['nome_plano'] : "<?>"; ?></h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
    </div>
		<?php
			if(!empty($plano)){
				echo '<input type="hidden" name="periodoID" value="'.$plano['id_periodo'].'" >';
				$ar_dados = json_decode($plano['dados_plano']);
				$ar_coment = json_decode($plano['observacao']);
				if($ar_dados){
					foreach ($ar_dados as $key => $value) {
						// $arIds[] = $ar_dados->$key->iddiv;
						$arIds[] = array('iddiv' => $ar_dados->$key->iddiv, 'tipo' => (empty($ar_dados->$key->tipo))? ' ': $ar_dados->$key->tipo );
					}
				}
			}
			
			// die('asdasdas');
			if(!empty($ar_dados)){
				// $this->functions->pre($ar_dados);
				// die('');
				foreach ($ar_dados as $key => $value) {

					if($value->iddiv != 'cabecalho'){
						$aux = $value->iddiv;
						echo '
						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel panel-heading">
										'.@$modelo['ar_configuracao']->$aux->title.'
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-lg-12" id="'.$value->iddiv.'" >';
											if($value->tipo == 'texto'){
												if($this -> functions -> checkPermissao(array('Administrador','Coordenador','Pedagógico'),$this -> session->nome_regra)){
													echo	'<div name="'.$value->iddiv.'" >';												
													echo 				@$value->dados;
													echo '</div	>';
												}else{
													echo	'<textarea class="editable" contenteditable="true" name="'.$value->iddiv.'" >';
													echo 				@$value->dados;
													echo '</textarea	>';
												}
											}
											if($value->tipo == 'tabela'){
												if(is_object($value->dados)){

													if($value->iddiv=='cronograma' && ($this -> functions -> checkPermissao(array('Professor'),$this -> session->nome_regra))){
														echo '
																		<div class="row">
																			<div class="col-lg-12">
																				<input type="hidden" name="num-coluna" value="'.count($value->dados->nome_col).'" data-howstable="'.$value->iddiv.'">
																				<select class="form-control" id="diaSemana">
																					<option value="">Selecione o dia da semana</option>
																					<option value="1">Segunda Feira</option>
																					<option value="2">Terça Feira</option>
																					<option value="3">Quarta Feira</option>
																					<option value="4">Quinta Feira</option>
																					<option value="5">Sexta Feira</option>
																				</select>
																			</div>
																		</div>	
														';
													}

													echo '<table class="table table-bordered " >
														<thead>
														<tr>
													';
													foreach ($value->dados->nome_col as $th) {
														echo '<th>';
														echo $th;
														echo '</th>';
													}
													echo '</tr></thead>
															<tbody class="'.$value->iddiv.'">';
													if(isset($value->dados->val_row)){
														foreach ($value->dados->val_row as $tr) {
															echo '<tr class="row_data">';
															if($this -> functions -> checkPermissao(array('Administrador','Coordenador','Pedagógico'),$this -> session->nome_regra)){
																for ($i=0; $i < count($tr) -1 ; $i++) { 
																	echo '<td class="colns_data">';
																	echo $tr[$i];
																	echo '</td>';
																}
															}else{
																for ($i=0; $i < count($tr) -1 ; $i++) { 
																	echo '<td class="colns_data">
																		<textarea class="form-control">';
																	echo $tr[$i];
																	echo '</textarea></td>';
																}
															}
															if($this -> functions -> checkPermissao(array('Professor'),$this -> session->nome_regra)){
																echo '<td class="text-center">
																				<button onclick="RemoveTableRow(this)" type="button" class="btn btn-danger btn-circle fa fa-trash"></button>
																			</td>
																';
															}
															echo '</tr>';
														}
													}
													if($this -> functions -> checkPermissao(array('Administrador','Coordenador','Pedagógico'),$this -> session->nome_regra)){
														echo '</tbody>
																</table>
														';
													}else{	
														echo '</tbody>
																</table>
														<div class="pull-left">
															<button type="button" class="btn btn-primary"n onclick="openModalRows(`'.$value->iddiv.'`,'.count($value->dados->nome_col).');">
																<i class="fa fa-plus"></i>
															</button>
														</div>';
													}
												}
											}
						echo '		
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
								';
						if(!empty($ar_coment)){
							echo '<div class="alert alert-warning" >';
							if($this -> session -> has_userdata("nome_regra") && $this->functions->checkPermissao(array('Pedagógico','Coordenador','Administrador'),$this->session->nome_regra)){
								if (!empty($ar_coment)) {
								echo '<div class="editable '.$aux.'-coment" contenteditable="true">';
									foreach ($ar_coment as $key_coment => $val_coment) {
										if($value->iddiv == $val_coment->iddiv){
											echo @$val_coment->dados;
										}
									}
								}
							}else{
								echo '<div class="'.$aux.'-coment">';
								if (!empty($ar_coment)) {
									foreach ($ar_coment as $key_coment => $val_coment) {
										if($value->iddiv == $val_coment->iddiv){
											echo @$val_coment->dados;
										}
									}
								}
							}
							echo '
									</div>
							</div>			
							';
						}else{
							if($this -> session -> has_userdata("nome_regra") && $this->functions->checkPermissao(array('Pedagógico','Coordenador','Administrador'),$this->session->nome_regra)){
								echo '<div class="alert alert-warning" >';
								echo '<div class="editable '.$aux.'-coment" contenteditable="true" >
											</div>
											</div>';
							}
						}
					}else{
						echo '
							<div class="row">
								<div class="col-lg-12" id="'.$value->iddiv.'">
									<div class="panel panel-default">
										<div class="panel panel-heading">
											Cabeçalho
											<div class="pull-right">
												<button class="btn btn-primary fa fa-pencil" onclick="editarCabecalho();"> Editar </button>
											</div>
										</div>
										<div class="panel-body" id="cabecalho">
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group col-lg-8">
														<label for="text">CURSO:</label>
														<input type="text" name="curso" size="40" value="'.$curso['curso']['nome_curso'].'" disabled>
													</div>
													<div class="form-group col-lg-4">
														<label for="text">ANO/SEMESTRE:</label>
														<input type="text" name="peridoAno" value="'.$periodo['abreviacao_periodo'].'" disabled>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group col-md-4">
														<label for="text">PROFESSOR:</label>
														<input type="text" name="nomeProfessor" value="'.$this->session->nome.'" disabled>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group col-md-12">
														<label for="text">COMPONENTE CURRICULAR:</label>
														<input type="text" name="disciplina" value="'.$disciplina['disciplina']['nome_disciplina'].'" disabled>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group col-md-12">
														<label for="text">PPC/RESOLUÇÃO Nº:</label>
														<input type="text" name="ppc" size="40" value="109 de 13 de dezembro de 2016" disabled>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group col-md-12">
														<label for="text">CARGA HORÁRIA SEMESTRAL:</label>
														<input type="text" name="periodo" value="'.$disciplina['disciplina']['cargahoraria_disciplina'].' h/r" disabled>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group col-md-12">
														<label for="text">PERÍODO:</label>
														<input type="text" name="periodo" value="'.$periodo['nome_periodo'].'" disabled>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>';
					}
				}
			}else{
				if(!empty($modelo)){
					// $this -> functions-> pre($modelo);
					$modelo = ($modelo['ar_configuracao']);
					foreach ($modelo as $key => $value) {
						$ar_order[$key] = $value->ordem;
					}
					asort($ar_order);
					//$this -> functions-> pre($modelo);
					foreach ($ar_order as $key => $value) {

						$arIds[] = array('iddiv' => $modelo->$key->iddiv, 'tipo' => @$modelo->$key->tipo);

						if($modelo->$key->iddiv == 'cabecalho'){
							echo '
							<div class="row">
								<div class="col-lg-12" id="'.$modelo->$key->iddiv.'">
									<div class="panel panel-default">
										<div class="panel panel-heading">
											Cabeçalho
											<div class="pull-right">
												<button class="btn btn-primary fa fa-pencil" onclick="editarCabecalho();"> Editar </button>
											</div>
										</div>
										<div class="panel-body" id="cabecalho">
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group col-lg-8">
														<label for="text">CURSO:</label>
														<input type="text" name="curso" size="40" value="'.$curso['curso']['nome_curso'].'" disabled>
													</div>
													<div class="form-group col-lg-4">
														<label for="text">ANO/SEMESTRE:</label>
														<input type="text" name="peridoAno" value="'.$periodo['abreviacao_periodo'].'" disabled>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group col-md-4">
														<label for="text">PROFESSOR:</label>
														<input type="text" name="nomeProfessor" value="'.$this->session->nome.'" disabled>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group col-md-12">
														<label for="text">COMPONENTE CURRICULAR:</label>
														<input type="text" name="disciplina" value="'.$disciplina['disciplina']['nome_disciplina'].'" disabled>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group col-md-12">
														<label for="text">PPC/RESOLUÇÃO Nº:</label>
														<input type="text" name="ppc" size="40" value="109 de 13 de dezembro de 2016" disabled>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group col-md-12">
														<label for="text">CARGA HORÁRIA SEMESTRAL:</label>
														<input type="text" name="periodo" value="'.$disciplina['disciplina']['cargahoraria_disciplina'].' h/r" disabled>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group col-md-12">
														<label for="text">PERÍODO:</label>
														<input type="text" name="periodo" value="'.$periodo['nome_periodo'].'" disabled>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>';
						
						}
						if(@$modelo->$key->tipo == 'texto')	{
							if($this -> functions -> checkPermissao(array('Administrador','Coordenador','Pedagógico'),$this -> session->nome_regra)){
								echo '
									<div class="row">
										<div class="col-lg-12">
											<div class="panel panel-default">
												<div class="panel panel-heading">
													'.@$modelo->$key->title.'
												</div>
												<div class="panel-body">
													<div class="row">
														<div class="col-lg-12" id="'.$modelo->$key->iddiv.'" >
															
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								';
							}else{
								echo '
									<div class="row">
										<div class="col-lg-12">
											<div class="panel panel-default">
												<div class="panel panel-heading">
													'.@$modelo->$key->title.'
												</div>
												<div class="panel-body">
													<div class="row">
														<div class="col-lg-12" id="'.$modelo->$key->iddiv.'" >
															<textarea class="editable" contenteditable="true" name="'.$modelo->$key->iddiv.'" >
															</textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								';
							}
						}
						if(@$modelo->$key->tipo == 'tabela'){
							echo'
								<div class="row">
											<div class="col-lg-12">
												<div class="panel panel-default">
														<div class="panel panel-heading">
															'.@$modelo->$key->title.'
													</div>
										<div class="panel-body">';
						if($modelo->$key->iddiv=='cronograma'){
							echo '
											<div class="row">
												<div class="col-lg-12">
													<input type="hidden" name="num-coluna" value="'.$modelo->$key->colnumber.'" data-howstable="'.$modelo->$key->iddiv.'">
													<select class="form-control" id="diaSemana">
														<option value="">Selecione o dia da semana</option>
														<option value="1">Segunda Feira</option>
														<option value="2">Terça Feira</option>
														<option value="3">Quarta Feira</option>
														<option value="4">Quinta Feira</option>
														<option value="5">Sexta Feira</option>
													</select>
												</div>
											</div>	
							';
						}
						echo'				
											<div class="row">
												<div class="col-lg-12" id="'.$modelo->$key->iddiv.'">
														<table class="table table-bordered" >
													<thead>
													';
						foreach ($modelo->$key->coltitle as $value) {
							echo' <th> ';
								echo $value;
							echo '</th>';
						}														
						echo'
													</thead>
													<tbody class="'.$modelo->$key->iddiv.'">
													</tbody>
													</table>
													<div class="pull-left">
														<button type="button" class="btn btn-primary"n onclick="openModalRows(`'.$modelo->$key->iddiv.'`,'.$modelo->$key->colnumber.');">
															<i class="fa fa-plus"></i>
														</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							';	
						}


					}
				}
			}
			echo '<input type="hidden" name="id-plano" id="id-plano" value="'.$id_plano.'">'
		?>
		<div class="row">
  		<div class="col-md-12">
    		<div class="panel default" style="margin-bottom:10px;">
					<div class="col-md-4">
					<?php 
						if($this -> session -> has_userdata("nome_regra") 
							&& 
							$this->functions->checkPermissao(array('Pedagógico','Coordenador','Administrador'),$this->session->nome_regra)){
							echo '
								<button class="btn btn-danger btn-block btn-lg fa fa-refresh" id="solicitarCorreção">
									Enviar Correções
								</button>
							';
						}
					?>
					</div>
					<div class="col-md-4">
					<?php 
						if($this -> session -> has_userdata("nome_regra") 
							&& 
							$this->functions->checkPermissao(array('Professor','Administrador'),$this->session->nome_regra)){
							echo '
								<button class="btn btn-primary btn-block btn-lg fa fa-save" id="salvar">
									Salvar
								</button>
							';
						}
					?>
					</div>
					<div class="col-md-4">
					<?php 
						if($this -> session -> has_userdata("nome_regra") 
							&& 
							$this->functions->checkPermissao(array('Pedagógico','Coordenador','Administrador'),$this->session->nome_regra)){
							echo '
								<button class="btn btn-success btn-block btn-lg fa fa-check" id="aprovar">
									Aprovar
								</button>
							';
						}else{
							echo '
								<button class="btn btn-success btn-block btn-lg fa fa-send" id="submit-ava">
									Submeter 
								</button>
							';
						}
					?>
					</div>
    		</div>
    	</div>
  	</div>
		<br/>
  </div>
</div>
</div>
</div>



	<?php 
		if($this -> functions -> checkPermissao(array('Professor','Administrador'),$this->session->nome_regra)){
			// die(print_r($this -> session ->nome_regra).'TOBOCO');
		?>
	<script>
		window.onload = function() {
			CKEDITOR.config.removePlugins = "elementspath";
			CKEDITOR.config.resize_enabled = false;
			
			CKEDITOR.replaceAll();
    };
		function editarCabecalho(){
			var btn = $("#cabecalho").find("button");
			if(btn.hasClass("btn-primary")){
				btn.removeClass("btn-primary");
				btn.addClass("btn-success");
				$.each($("#cabecalho").find("input"), function(){
					$(this).removeAttr("disabled");
				});
			}else{
				btn.removeClass("btn-success");
				btn.addClass("btn-primary");
				$.each($("#cabecalho").find("input"), function(){
					$(this).attr("disabled",true);
				});
			}
		}
		function openModalRows(thisTable,numCols){
			AddTableRow(numCols,thisTable);
		}
		function AddTableRow(numCols,thisTable,dados=null) {	
				var newRow = $('<tr class="row_data">');
				var cols = "";
				for (var i = 0 ; i < numCols ; i++) {
					if(i == 0){
						if(dados){
							cols += '<td class="colns_data"><textarea class="form-control">'+dados+'</textarea></td>';
						}else{
							cols += '<td class="colns_data"><textarea class="form-control"></textarea></td>'
						}
					}else{
						cols += '<td class="colns_data"><textarea class="form-control"></textarea></td>';
					}
				}
				cols += '<td><br>';
				cols += '<button onclick="RemoveTableRow(this)" type="button" class="btn btn-danger btn-circle fa fa-trash"></button>';
				cols += '</td>';

				newRow.append(cols);
				$("."+thisTable).append(newRow);

				return false;
		}

		function RemoveTableRow(handler) {
			var tr = $(handler).closest('tr');

			tr.fadeOut(400, function(){
			tr.remove();
			});

			return false;
		}
		$("#diaSemana").change(function(){
			var id_periodo = <?php echo $plano['id_periodo']?>;
			if(id_periodo != undefined){
				$.post(base_url+"calendario/changeDatas",{id_periodo:id_periodo,dia_semana:$(this).val()},function(response){
					var aux = JSON.parse(response);
					var table = $('[name=num-coluna]');
					$("." + table.data('howstable')).empty();
					$.each(aux,function(key,values){
						AddTableRow($('[name=num-coluna]').val(),table.data('howstable'),values);
					});
				}).fail(function(){
					alert("Erro ao carregar as datas");
				});
			}
		});	
	<?php
		}else{
			echo '<script>
			';
		}
	?>
	var ar_plano = {};

	$("#salvar").click(function(){
		var ar_id_div = new Array;
		var data_plano = {};

		ar_id_div = <?php echo json_encode($arIds);?>;
		$.each(ar_id_div,function(key,values){
			if(values.tipo == 'texto'){
				// ar_plano[values.iddiv] = ({iddiv:values.iddiv, tipo:values.tipo , dados:$("#"+values.iddiv).val()});
				ar_plano[values.iddiv] = ({iddiv:values.iddiv, tipo:values.tipo , dados:CKEDITOR.instances[values.iddiv].getData()});
			}
			else if(values.tipo == 'tabela'){
				var name_col = new Array ;
				var val_row = new Array;
				var tds_data = new Array;
				$("#"+values.iddiv).find('th').each(function(){
					name_col.push($(this).text());
				});
				var counter = 0;
				$("#"+values.iddiv).find('.row_data').each(function(){
					var tds = $(this).children();
					console.log(tds.length);
					if(tds.length != 0 ){ 
						tds.each(function(){
							tds_data.push($(this).children().val());
						})
						val_row.push(tds_data);
						tds_data = [];
					}
					counter++;
				});

				ar_plano[values.iddiv] = ({iddiv:values.iddiv, tipo:values.tipo , dados:{'nome_col': name_col, 'val_row': val_row}});
			}else{
				ar_plano[values.iddiv] = ({iddiv:values.iddiv, tipo:values.tipo , dados:$("#"+values.iddiv).text()});				
			}
			data_plano[values.iddiv] = ({iddiv:values.iddiv,dados:$("." + values.iddiv + "-coment").html()});
		});

		console.log( data_plano);
		console.log( ar_plano);

		// $("#modalNamePlano").modal('show');

		sendTo(base_url + "plano/EditarPlano",{ar_plano: ar_plano, ar_coment: data_plano, id_plano: $("#id-plano").val(), id_periodo: $("[name='periodoID']").val()},"POST");
	});

	$("#submit-ava").click(function(){
		var ar_plano = {};
		var ar_id_div = new Array;

		ar_id_div = <?php echo json_encode($arIds);?>;
		$.each(ar_id_div,function(key,values){
			if(values.tipo == 'texto'){
				// ar_plano[values.iddiv] = ({iddiv:values.iddiv, tipo:values.tipo , dados:$("#"+values.iddiv).text()});
				ar_plano[values.iddiv] = ({iddiv:values.iddiv, tipo:values.tipo , dados:CKEDITOR.instances[values.iddiv].getData()});
			}
			else if(values.tipo == 'tabela'){
				var name_col = new Array ;
				var val_row = new Array;
				var tds_data = new Array;
				$("#"+values.iddiv).find('th').each(function(){
					name_col.push($(this).text());
				});
				var counter = 0;
				$("#"+values.iddiv).find('.row_data').each(function(){
					var tds = $(this).children();
					console.log(tds.length);
					if(tds.length != 0 ){ 
						tds.each(function(){
							tds_data.push($(this).children().val());
						})
						val_row.push(tds_data);
						tds_data = [];
					}
					counter++;
				});

				ar_plano[values.iddiv] = ({iddiv:values.iddiv, tipo:values.tipo , dados:{'nome_col': name_col, 'val_row': val_row}});
			}else{
				ar_plano[values.iddiv] = ({iddiv:values.iddiv, tipo:values.tipo , dados:$("#"+values.iddiv).text()});				
			}
		});

		console.log(ar_plano);
		sendTo(base_url + "plano/SubmitPlano",{id_plano:$("#id-plano").val(),ar_plano:ar_plano},"POST");
	});
	
	$("#solicitarCorreção").click(function(){
		var data_plano = {};
			ar_id_div = <?php echo json_encode($arIds);?>;
		$.each(ar_id_div,function(key,values){
			data_plano[values.iddiv] = ({iddiv:values.iddiv,dados:$("." + values.iddiv + "-coment").html()});
		});
		console.log(data_plano);
		sendTo(base_url + "plano/Correcoes",{ar_coment: data_plano, id_plano:$("#id-plano").val()},"POST");
	});

	$("#aprovar").click(function(){
		sendTo(base_url + "plano/aprovarPlano",{id_plano:$("#id-plano").val()},"POST");
	});



</script>