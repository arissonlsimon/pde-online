<form role="form" onsubmit="return false;">
	<div class="panel panel-default">
	    <div class="panel panel-heading">
	      Configurações
	    </div>
	    <div class="panel-body">
	    	<div class="row">
	    		<div class=" form-group col-sm-12">
	    			<b class="text-left" style="font-size:13px">
                        <a target="_self" data-toggle="tooltip" data-placement="top" title="Nome do Plano.">
                        	<span class="glyphicon glyphicon-question-sign"></span>
                        </a>
                        <label for="nome">Nome Plano</label>
                  	</b>
                  	<input type="text" class="form-control" name="nome_plano" id="nome_plano" data-required="Informe o nome do curso">
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col-sm-12">
	    			<div class="form-group">
	    				<b class="text-left" style="font-size:13px">
	                        <a target="_self" data-toggle="tooltip" data-placement="top" title="Selecione a disciplina referente ao plano que está criando.">
	                        	<span class="glyphicon glyphicon-question-sign"></span>
	                        </a>
		             	   <label for="text" class="col-form-label">Disciplina:</label>
	                  	</b>
		                <select class="form-control" name="disciplina" id="disciplina" data-required="Selecione o grupo">
		                  <option value="">Selecione</option>
		                  <?php
		                    if(!empty($disciplina)) {
		                      foreach($disciplina as $key => $value) {
		                        echo '<option value="'.$value['id_disciplina'].'">'.$value['nome_disciplina'].'</option>';
		                      }
		                    }
		                  ?>
		                </select>
		            </div>
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col-sm-12">
	    			<div class="form-group">
	    				<b class="text-left" style="font-size:13px">
	                        <a target="_self" data-toggle="tooltip" data-placement="top" title="Selecione a disciplina referente ao plano que está criando.">
	                        	<span class="glyphicon glyphicon-question-sign"></span>
	                        </a>
			                <label for="text" class="col-form-label">Período:</label>
	                  	</b>
		                <select class="form-control" name="periodo" id="periodo" data-required="Selecione o grupo">
		                  <option value="">Selecione</option>
		                  <?php
		                    if(!empty($periodo)) {
		                      foreach($periodo as $key => $value) {
		                        echo '<option value="'.$value['id_periodo'].'">'.$value['nome_periodo'].'</option>';
		                      }
		                    }
		                  ?>
		                </select>
		            </div>
	    		</div>
	    	</div>
	    </div>
	    <div class="panel-footer text-right">
	      <button class="btn btn-success" type="submit">
	        Cadastrar
	      </button>
		</div>
	</div>
</form>