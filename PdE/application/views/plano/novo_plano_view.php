<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	if (!$this->functions->checkPermissao('Administrador', $_SESSION) || !$this->functions->checkPermissao('Professor', $_SESSION)) {
		header(base_url());
	}
	// $this -> functions-> pre(($modelo['ar_configuracao']));
	// die();
?>
<div id="wample">
    <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Editando o Plano de Ensino: <?php echo (!empty($plano))? $plano['nome_plano'] : "<?>"; ?></h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
    </div>
		<?php
			if(!empty($plano)){
				$ar_dados = json_decode($plano['dados_plano']);
			}
			foreach ($ar_dados as $key => $value) {

				if($value->iddiv != 'cabecalio'){
					$aux = $value->iddiv;
					echo '
					<div class="row">
	    			<div class="col-lg-12">
	      			<div class="panel panel-default">
	      				<div class="panel panel-heading">
	        				'.@$modelo['ar_configuracao']->$aux->title.'
	            	</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-12" id="'.$value->iddiv.'" >';
					echo 				$value->dados;
					echo '		</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
					';
				}else{
					echo 				$value->dados;
				}
			}
		?>
		<div class="row">
  		<div class="col-md-12">
    		<div class="panel pull-right">
    			<button class="btn btn-info fa fa-save">
    				Salvar
    			</button>
    		</div>
    	</div>
  	</div>
  </div>
</div>


<script>

	function openModalRows(thisTable,numCols){
		AddTableRow(numCols,thisTable);
	}
	function AddTableRow(numCols,thisTable,dados=null) {	
	    var newRow = $("<tr>");
	    var cols = "";
	    for (var i = 0 ; i < numCols ; i++) {
	    	if(i == 0){
	    		if(dados){
	    			cols += '<td><br>'+dados+'</td>';
	    		}else{
	    			cols += '<td><textarea class="form-control"></textarea></td>'
	    		}
	    	}else{
		    	cols += '<td><textarea class="form-control"></textarea></td>';
	    	}
	    }
	    cols += '<td><br>';
	    cols += '<button onclick="RemoveTableRow(this)" type="button" class="btn btn-danger btn-circle fa fa-trash"></button>';
	    cols += '</td>';

	    newRow.append(cols);
	    $("."+thisTable).append(newRow);

	    return false;
	}
	function RemoveTableRow(handler) {
		var tr = $(handler).closest('tr');

		tr.fadeOut(400, function(){
		tr.remove();
		});

		return false;
	}
	$("#diaSemana").change(function(){
		var id_periodo = $("[name=periodoANO]").val();
		if(id_periodo != undefined){
			$.post(base_url+"calendario/changeDatas",{id_periodo:id_periodo,dia_semana:$(this).val()},function(response){
				var aux = JSON.parse(response);
				var table = $('[name=num-coluna]');
				$("." + table.data('howstable')).empty();
				$.each(aux,function(key,values){
					AddTableRow($('[name=num-coluna]').val(),table.data('howstable'),values);
				});
			}).fail(function(){
				alert("Erro ao carregar as datas");
			});
		}
	});
	var ar_plano = {};
	$(".fa-save").click(function(){
		var ar_id_div = new Array;
		var data_plano = new Array;

		ar_id_div = <?php echo json_encode($arIds);?>;
		$.each(ar_id_div,function(key,values){
			ar_plano[values] = ({iddiv:values,dados:$("#"+values).html()});
		});

		console.log(ar_plano);

		$("#modalNamePlano").modal('show');
	});

	$("#criaPlano").click(function(){
		sendTo(base_url + "plano/EditarPlano",{ar_plano:ar_plano,nome_plano:$("#nomePlano").val(),id_periodo:$("[name=periodoANO]").val(),id_disciplina:$("[name=disciplina]").val()},"POST");
	});

</script>