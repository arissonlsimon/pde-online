<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Lista os modelos de plano de ensino</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel panel-heading">
              Modelos de planos de ensino
              <div class="pull-right">
                <a href="<?php echo base_url();?>plano/modelo/cadastro" class="btn btn-primary btn-xs"><span class="fa fa-plus"></span> Cadastrar</a>
              </div>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nome</th>
                      <th>Composição</th>
                      <th>Validado</th>
                      <th>Categoria</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      if (isset($modelo)) {
                        if(is_array($modelo)){
                          foreach ($modelo as $key => $value) {
                            echo '
                              <tr>
                                <td>
                                  '.$value['id_modelo'].'
                                </td>
                                <td>
                                '.$value['nome_modelo'].'
                                </td>
                                <td>
                                  '.$value['ar_configuracao'].'
                                </td>
                                <td>';
                            if ($value['usual'] == 1) {
                              echo '<button id="status" data-toggle="status" data-status="Ativo" data-categoria="'.$value['categoria'].'" data-idmodelo="'.$value['id_modelo'].'" class="btn btn-success btn-xs">Ativo</button>';
                            }else{
                              echo '<button id="status"  data-toggle="status" data-status="Suspenso" data-categoria="'.$value['categoria'].'" data-idmodelo="'.$value['id_modelo'].'" class="btn btn-danger btn-xs">Suspenso</button>';
                            }
                          echo '</td>
                                <td>
                                  '.$value['categoria'].'
                                </td>';
                          if($value['num_plano'] == 0){
                            echo '<td>
                                <button name="removeModelo" data-idmodelo="'.$value['id_modelo'].'" data-toggle="tooltip" data-placement="top" data-original-title="Remover modelo" class="btn btn-link fa fa-trash" style="color:#d9534f"></button>
                            </td>';
                          }else{
                            echo '<td></td>';
                          }
                          echo '      
                              </tr>
                            ';
                          }
                        }else{
                          echo '
                          <tr>
                            <td colspan="5">'.$modelo.'</td>
                          </tr>
                          ';
                        }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<script>
  $("button[data-toggle=status]").click(function(){
    sendTo(base_url+'plano/setActiveModelo',{id_modelo:$(this).data('idmodelo'),categoria:$(this).data('categoria')},'POST');
  });
  $("[name=removeModelo]").click(function(){
    var id = $(this).data('idmodelo');
    if(id != '' || id != undefined ){
      sendTo(base_url + 'plano/removeModelo',{id:id},'POST');
    }
  });
</script>