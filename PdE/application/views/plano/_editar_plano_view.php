<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	// if (!$this->functions->checkPermissao('ADM', $_SESSION) || !$this->functions->checkPermissao('PRF', $_SESSION)) {
	// 	header(base_url());
	// }
?>

<div id="wample">
    <!-- Page Content -->
    <div id="page-wrapper">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">Plano de Ensino => <?php echo (!empty($plano))? $plano['nome_plano'] : "<?>"; ?></h1>
          </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel panel-heading">
                Cabeçalho
                <!-- <div class="pull-right">
                  <button type="button" data-toggle="modal" data-target="#modalCadastraNovo" class="btn btn-primary btn-xs"><span class="fa fa-plus"></span> Cadastrar</button>
                </div> -->
              </div>
							<div class="panel-body" id="cabecalho">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group col-md-12">
											<label for="text">CURSO:</label>
											<select name="cursoID">
												<?php
												if (isset($curso)) {
													foreach ($curso as $key => $value) {
														echo '<option value="'.$value['id_curso'].'">'.$value['nome_curso'].'</option>';
													}
												}
											 	?>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group col-md-4">
											<label for="text">ANO/SEMESTRE:</label>
											<select name="periodoANO">
												<?php
												if (isset($periodo)) {
													foreach ($periodo as $key => $value) {
														echo '<option value="'.$value['id_periodo'].'">'.$value['abreviacao_periodo'].'</option>';
													}
												}
											 	?>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group col-md-4">
											<label for="text">PROFESSOR:</label>
											<input type="text" name="nomeProfessor" value="<?php echo $this->session->nome; ?>" disabled>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group col-md-4">
											<label for="text">DISCIPLINA:</label>
											<select name="periodoANO">
												<?php
												if (isset($disciplina)) {
													foreach ($disciplina as $key => $value) {
														if ($value['id_disciplina'] == $plano['id_disciplina']) {
															echo '<option value="'.$value['id_disciplina'].'" selected>'.$value['nome_disciplina'].'</option>';
														}else {
															echo '<option value="'.$value['id_disciplina'].'">'.$value['nome_disciplina'].'</option>';
														}
													}
												}
											 	?>
											</select>
										</div>
									</div>
								</div>
							</div>
            </div>
          </div>
        </div>
				<!-- EMENTAA -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel panel-heading">
								EMENTA
							</div>
							<div class="panel-body">
								<div class="editable" id="EMENTA" contenteditable="true" >
									<?php
									echo @$dados->datas->ementa;
									?>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- OBJETIVO -->
		<div class="row">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th scope="col">
							OBJETIVO
						</th>
					</tr>
				</thead>
				<tbody >
					<tr>
						<td>
							<div class="editable" id="OBJETIVO" contenteditable="true"  >

									<?php
										echo @$objetos;
									?>

							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="row">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th scope="col">
							CONTEÚDOS
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<div class="editable" contenteditable="true" id="CONTEUDOS">
								<?php
									echo @$dados->datas->conteudo;
								?>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="row">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th scope="col">
							PONTOS INTEGRADORES
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<div class="editable" contenteditable="true" id="PONTOS_INTEGRADORES">
								<?php
									echo @$dados->datas->points;
								?>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="row">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th scope="col">
							METODOLOGIA DE ENSINO DA DISCIPLINA
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<div class="editable" contenteditable="true" id="METODOLOGIA_ENSINO">
								<?php
									echo @$dados->datas->metensi;
								?>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="row">
			<table class="table table-bordered" id="tabela_cronograma">
				<thead>
					<tr>
						<th>
							PREVISÃO DE DATA DAS AVALIAÇÕES
						</th>
						<th>
							INSTRUMENTO DE AVALIAÇÃO
						</th>
						<th class="text-center">
							CRITÉRIOS DE AVALIAÇÃO
						</th>
						<th>
							COMPOSIÇÃO DA NOTA
						</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
				<tfoot>
					<tr>
						<td colspan="5" style="text-align: left;">
							<button onclick="AddTableRow('tabela_cronograma')" type="button" class="btn btn-primary">Adicionar</button>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>

		<div class="row">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th scope="col">
							COMPOSIÇÃO DA MÉDIA FINAL DO ESTUDANTE NO SEMESTRE
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<div class="editable" contenteditable="true" id="MEDIA_FINAL">

							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="row">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th scope="col">
							CRONOGRAMA DE EXECUÇÃO DO PLANO DE ENSINO
						</th>
					</tr>
				</thead>
				<tbody>

						<?php
							if($periodo){
								echo '<tr><td>';
								echo '<select class="form-control" id="periodoSelect" title="selecione um periodo">';
								echo '<option>Selecione um periodo...</option>';
								foreach ($periodo as $key => $value) {
									echo '<option value="'.$value['id'].'">'.$value['nome_periodo'].'('.$value['nome_curto'].')</option>';
								}
								echo '</tr></td>
									<tr>
										<select class="form-control" id="diaSemanaSelect" title="Selecione o dia da semana de aula" style="display:none">
									</tr>
								';
							}
					 	?>

					<tr>
						<td>
							<table class="table table-bordered" id="tabela1">
								<thead>
									<tr>
										<th>
											DATA
										</th>
										<th>
											CONTEÚDOS
										</th>
										<th class="text-center">
											METODOLOGIA
										</th>
										<th>
											AVALIAÇÃO
										</th>
									</tr>
								</thead>
								<tbody>

								<!-- contudo da tabela -->

								</tbody>

								<tfoot>
									<tr>
										<td colspan="5" style="text-align: left;">
											<button onclick="AddTableRow('tabela1')" type="button" class="btn btn-primary" title="Adicionar Linha"><i class="fas fa-plus"></i></button>
										</td>
									</tr>
								</tfoot>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="row">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th scope="col">
							HORÁRIO EXTRACLASSE PARA REALIZAÇÃO DOS ESTUDOS ORIENTADOS
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<div class="editable" contenteditable="true" id="">

							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

	<div class="row">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th scope="col">
							REFERÊNCIAS
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<div class="editable" contenteditable="true" id="">

							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
				<!--  -->
      </div>
    </div>
</div>
