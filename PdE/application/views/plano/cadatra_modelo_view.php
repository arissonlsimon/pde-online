<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
    	
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Cadatrar um novo modelo</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <!-- <form role="form" onsubmit="return false;"> -->
        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel panel-heading">
                Informações do Modelo
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="row">
                      <div class="form-group col-md-8">
                          <b class="text-left" style="font-size:13px">
                            <a target="_self" data-toggle="tooltip" data-placement="top" title="Nome do modelo!"><span class="glyphicon glyphicon-question-sign"></span></a>
                            <label for="nome">Nome</label>
                          </b>
                          <input type="text" class="form-control" name="nome_modelo" id="nome_modelo" data-required="Informe o nome do modelo" value="<?php echo @$_POST['nome_modelo'] ?>">
                      </div>
                      <div class="form-group col-md-4">
                        <b class="text-left" style="font-size:13px">
                          <a target="_self" data-toggle="tooltip" data-placement="top" title="Selecione uma Categoria!"><span class="glyphicon glyphicon-question-sign"></span></a>
                          <label for="nome">Categoria</label>
                        </b>
                        <select name="categoria" id="categoria" class="form-control">
                          <option value="Graduação">Graduação</option>
                          <option value="Pós-Graduação">Pós-Graduação</option>
                          <option value="Ensino Médio">Ensino Médio</option>
                        </select>
                      </div>
                    </div>
                     <div class="row">
                      <div class="col-lg-12">
                        <div class="row">
                          <div class="form-group col-md-12">
                              <b class="text-left" style="font-size:13px">
                                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Selecione os itens que deseja incluir no modelo de Plano de ensino"><span class="glyphicon glyphicon-question-sign"></span></a>
                                  <label for="nome">Configurações do modelo</label>
                                </b>
                          </div>
                        </div>
                        <!-- aqui começa o Fucking setting do modelo -->
                        <div class="row">
                          <div class="form-group col-lg-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="chk[]" value="cabecalho" data-ordemnar="1" data-titulos="Cabeçalho" data-required="Necessário selecionar esse campo" >Cabeçalho
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="chk[]" data-ordemnar="2" data-titulos="Ementa" value="ementa">Ementa 
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="chk[]" data-ordemnar="3" data-titulos="Objetivo" value="objetivo">Objetivo
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="chk[]" data-ordemnar="4" data-titulos="Conteúdo" value="conteudo">Conteúdo
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="chk[]" data-ordemnar="5" data-titulos="Pontos Integradores" value="PtInte">Pontos Integradores
                                </label>
                            </div>
                             <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="chk[]" data-ordemnar="6" data-titulos="Metodologia de Ensino" value="MetEnsi">Metodologia de Ensino da Disciplina
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="chk[]" data-ordemnar="7" data-titulos="Sistema de Avaliação" value="SiAva">Sistema de Avaliação
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="chk[]" data-ordemnar="8" data-titulos="Composição da Média Final" value="CompMedFim">Composição da Média Final
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="chk[]" data-ordemnar="9" data-titulos="Cronograma" value="cronograma">Cronograma
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="chk[]" data-ordemnar="10" data-titulos="Horário Extra Classe" value="HrEx">Horário Extra Classe
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="chk[]" data-ordemnar="11" data-titulos="Referências" value="referencia">Referencias
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="chk[]" data-ordemnar="12" data-titulos="Vazio" value="empti">Vazio
                                </label>
                            </div>
                          </div>
                        </div>
                        <!-- aqui acaba o fudido confdo modelo  -->
                      </div>
                    </div>    
                  </div> 
                </div>
               
              </div>
            </div>
          </div>

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-body text-right">
                <button class="btn btn-success" type="submit">
                  Cadastrar
                </button>
              </div>
            </div>
          </div>
        </div>
      <!-- /.row -->
      <!-- </form> -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

  <div class="modal fade" id="modalConfElemnt" tabindex="-1" role="dialog" aria-labelledby="modalConfElemnt" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="modalConfElemntTitle"></h4>
        </div>
      <form id="formInfos">
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row">
              <div class="form-group col-md-12">
                <b class="text-left" style="font-size:13px">
                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Informe o Titulo do bloco"><span class="glyphicon glyphicon-question-sign"></span></a>
                  <label for="titulo">Titulo</label>
                </b>
                <input type="text" class="form-control" name="titulo" id="titulo" data-required="Informe o Titulo do bloco" value="<?php echo @$altData['titulo']?>" required>
              </div>
              <div class="form-group col-lg-12">
                <b class="text-left" style="font-size:13px">
                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Informe o posicionamento do bloco"><span class="glyphicon glyphicon-question-sign"></span></a>
                  <label for="ordem">Orden</label>
                </b>
                <input type="number" class="form-control" name="ordem" id="ordem" data-required="Informe o posicionamento do bloco" value="<?php echo @$altData['ordem']?>" required>
              </div>
              <div class="form-group col-lg-12 tipo">
                <b class="text-left" style="font-size: 13px;">
                  <a target="_self" data-toggle="tooltip" data-placement="top" title="Informe o tipo do bloco"><span class="glyphicon glyphicon-question-sign"></span></a>
                  <label>Tipo</label>
                  <div class="radio">
                    <label>
                      <input type="radio" name="tipo[]" id="text1" value="texto" checked>Texto
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="tipo[]" id="tabela1" value="tabela">Tabela
                    </label>
                  </div>
                </b>
              </div>
              <div class="form-group col-lg-12 colunasTabela">
                <div class="row">
                  <div class="col-md-3">
                    <b class="text-left" style="font-size:13px">
                      <a target="_self" data-toggle="tooltip" data-placement="top" title="Informe a quantidade de colunas"><span class="glyphicon glyphicon-question-sign"></span></a>
                      <label for="numcolunas">Colunas</label>
                    </b>
                    <input type="number" class="form-control" name="numcolunas" id="numcolunas" data-required="Informe a quantidade de colunas" value="<?php echo @$altData['numcolunas']?>" required>
                  </div>
                </div>
              </div>
              <div class="form-group col-lg-12 colunasTabela">
                <div class="row" id="nomeColunas">
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
        <div class="modal-footer">
          
          <!-- <button type="button" class="btn btn-primary fa fa-save" id="salvarConfig"> Salvar</button> -->
        </div>
      </div>
    </div>
  </div>



<script>
var ar_conf_div = {};
var ar_name_col = new Array;
  $("#tabela1").click(function(){
    $(".colunasTabela").show();
    $("#numcolunas").val('');
    $("#nomeColunas").empty();
  });
  $("#text1").click(function(){
    $("#numcolunas").val('');
    $(".colunasTabela").hide();
  });
  $("#numcolunas").change(function(){
    $("#nomeColunas").empty();
    for (var i = 0; i < $(this).val(); i++) {
      $("#nomeColunas").append('<label> Coluna'+i+' </label> <input type="text" name="colunas[]">');
    }
  });
  $("input[name='chk[]']").click(function(i){
    var i = $(this);
    if(!ar_conf_div[$(this).val()]){
      if($(this).val() == 'cabecalho'){
        $(".tipo").hide().val('');
      }else{
        $(".tipo").show();
      }
      $("#titulo").empty();
      $("#titulo").val(i.data('titulos'));
      $("#ordem").empty();
      $("#ordem").val(i.data('ordemnar'));
      $(".colunasTabela").hide();
      $(".modal-footer").empty().append('<button type="button" class="btn btn-danger fa fa-close" data-dismiss="modal"></button> <button type="button" class="btn btn-primary fa fa-save" onclick="salvarConfig(`'+$(this).val()+'`);"> Salvar</button>');
      $("#modalConfElemnt").modal('show');
      $("#modalConfElemntTitle").empty().append($(this).val());
    }else{
      if(ar_conf_div[i.val()]){
        delete ar_conf_div[i.val()];
      }else{
        alert("fon");
      }
    }
    console.log(ar_conf_div);
  });
  function salvarConfig(elements){
    var el = $(this);
    var tipo = $("[name='tipo[]']:checked").val();
    var ordem = $("[name='ordem']").val();
    var title = $("[name='titulo']").val();
    var colnumber = $("#numcolunas").val();
    var aux = ar_conf_div.elements;
    
    if(tipo == 'tabela'){
      $("[name='colunas[]']").each(function(index){
        ar_name_col.push($(this).val());
      });
      ar_conf_div[elements]=({iddiv:elements,tipo:tipo,ordem:ordem,title:title,colnumber:colnumber,coltitle:ar_name_col});
      ar_name_col = [];
    }else{
      ar_conf_div[elements] = ({iddiv:elements,tipo:tipo,ordem:ordem,title:title});
    }

    $("#modalConfElemnt").modal('hide');
    document.getElementById("formInfos").reset();
    
  }

  $('.form-group').tooltip({
      selector: "[data-toggle=tooltip]",
      container: "body"
  });

  $("button[type='submit']").click(function(){
    sendTo(base_url + 'plano/modelo',{nome_modelo:$("#nome_modelo").val(),ar_configuracao: ar_conf_div, categoria:$("#categoria").val()},'POST');
  });
</script>