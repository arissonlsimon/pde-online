<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Cursos</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel panel-heading">
              Cursos
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Nome</th>
                      <th>Formação</th>
                      <th>Coordenador</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      if (isset($dados)) {
                        if(is_array($dados)){
                          $cont =0;
                          foreach ($dados as $key => $value) {
                            echo '
                              <tr role="tab" data-toggle="collapse" data-target=".multi'.$cont.'">
                                <td>
                                '.$value['nome_curso'].'
                                </td>
                                <td>
                                  '.$value['categoria_curso'].'
                                </td>
                                <td>
                                '.$value['nome'].' 
                                </td>
                                <td>
                                  <button class=" btn btn-info btn-outline btn-circle fa fa-info" data-toggle="tooltip" title="Mostrar disciplinas"></button>
                                </td>
                              </tr>';
                              if(!empty($value['disciplinas'])){
                                foreach($value['disciplinas'] as $disciplinas){
                                  echo '
                                  <tr>
                                    <td class="panel-collapse collapse multi'.$cont.'" role="tabpanel" id="tab'.$cont.'">
                                      <a href="'.base_url().'listaPlano/'.$disciplinas['id_disciplina'].'">'.$disciplinas['nome_disciplina'].'</a>
                                    </td>
                                  </tr>
                                ';
                                }
                              }else{
                                echo '
                                <tr>
                                  <td class="panel-collapse collapse multi'.$cont.'" role="tabpanel" id="tab'.$cont.'">
                                    Não há disciplinas ainda
                                  </td>
                                </tr>
                              ';
                              }
                            $cont++;
                          }
                        }else{
                          echo '
                          <tr>
                            <td colspan="5">'.$dados.'</td>
                          </tr>
                          ';
                        }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->