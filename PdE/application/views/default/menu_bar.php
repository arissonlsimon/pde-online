<?php require("modais.php") ?>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <?php
          if ($this -> session -> has_userdata("nome")) {
            echo '<a class="navbar-brand" href="'.base_url().'">Painel '.$this->session->userdata("nome").'</a>';
          }else{
            echo '<a class="navbar-brand" href="'.base_url().'">Painel Plano de ensino</a>';
          }
         ?>
    </div>

    <!-- /.navbar-header -->
    <ul class="nav navbar-top-links navbar-right">

<?php if ($this -> session -> has_userdata("nome_regra") && $this->functions->checkPermissao(array('Professor','Pedagógico','Coordenador','Administrador'),$this->session->nome_regra)){ ?>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="<?php echo base_url("/meuperfil"); ?>"><i class="fa fa-user fa-fw"></i> Perfil do usuário</a>
                </li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url("/usuario/logout"); ?>"><i class="fa fa-sign-out fa-fw"></i> Sair</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->


<!-- MENU LATERAL  -->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
            
                    <li>
                        <a href="<?php echo base_url();?>avaliacoes"><i class="fa fa-dashboard fa-fw"></i> Início</a>
                    </li>
            
            <?php
                if ($this->functions->checkPermissao(array('Administrador'), $this->session->nome_regra)) {
            ?>
                <li>
                    <a href="#"><i class="fa fa-graduation-cap fa-fw"></i> Pedagógico<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="#">Cursos <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="<?php echo base_url();?>curso/cadastra"><span class="fa fa-plus"></span> Cadastrar</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>curso/lista"><span class="fa fa-list"></span> Listar</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Disciplinas <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="<?php echo base_url();?>disciplina/cadastra"><span class="fa fa-plus"></span> Cadastrar</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>disciplina/lista"><span class="fa fa-list"></span> Listar</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
            <?php
                }
            ?>
                <li>
                    <a href="#"><i class="fa fa-book fa-fw"></i> Plano<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <?php 
                            if($this -> functions -> checkPermissao(array('Professor','Administrador'),$this -> session->nome_regra)){
                        ?>
                        <li>
                            <a href="<?php echo base_url();?>plano/menuPlano"><span class="fa fa-plus"></span> Cadastrar</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>plano/lista"><span class="fa fa-list"></span> Meus Planos</a>
                        </li>
                            <?php } else{ ?>
                                <li>
                                    <a href="<?php echo base_url();?>plano/lista"><span class="fa fa-list"></span> Lista Planos</a>
                                </li>
                            <?php } ?>
                        <?php 
                            if($this->functions->checkPermissao(array('Administrador'),$this->session->nome_regra)){
                        ?>
                                <li>
                                    <a href="<?php echo base_url();?>plano/modelo"><span class="fa fa-clipboard"></span> Modelo</a>
                                </li>
                        <?php
                            }
                        ?>
                    </ul>
                </li>
            <?php
                if($this->functions->checkPermissao(array('Administrador'),$this->session->nome_regra)){
            ?>    
                    <li>
                        <a href="<?php echo base_url();?>calendario"><i class="fa fa-calendar fa-fw"></i> Calendário</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>usuario/lista"><i class="fa  fa-users fa-fw"></i> Administrar docentes</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>usuario/regra"><i class="fa  fa-sign-in fa-fw"></i> Regras</a>
                    </li>
            <?php
                }   
            ?>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
<?php }else{ ?>
          <!-- /.dropdown -->
          <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
              </a>
              <ul class="dropdown-menu dropdown-user">
                  <li><button class="btn btn-link" id="btnLogin"><i class="fa fa-sign-in fa-fw"></i> Login</button>
                  </li>
              </ul>
              <!-- /.dropdown-user -->
          </li>
          <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->


  <div class="navbar-default sidebar" role="navigation">
      <div class="sidebar-nav navbar-collapse">
          <ul class="nav" id="side-menu">
            <li class="sidebar-search">
              </li>
              <li>
                  <a href="<?php echo base_url();?>"><i class="fa fa-dashboard fa-fw"></i>Início</a>
              </li>
              <li>
                  <a href="<?php echo base_url();?>listaCursos/gra"><i class="fa fa-graduation-cap fa-fw"></i> Graduação </a>
              </li>
              <li>
                  <a href="<?php echo base_url();?>listaCursos/esm"><i class="fa fa-graduation-cap fa-fw"></i> Ensino Médio </a>
              </li>
          </ul>
      </div>
      <!-- /.sidebar-collapse -->
  </div>
  <!-- /.navbar-static-side -->
<?php } ?>
</nav>

<script type="text/javascript">
  $("#btnLogin").click(function(){
    $("#modalLogin").modal("show");
  });

  $("#entrar").click(function(){
    $.ajax({
      url:"<?php echo base_url(); ?>inicio/login",
      type:"POST",
      dataType:"JSON",
      data:{email:$("#modalLogin").find('#email').val(), senha:$("#modalLogin").find('#senha').val()},
      success:function(response){
        if(response.redirect == 'rSenhaNew'){
          redefineSenha(response.redirect);
        }else{
          if(response.status == 'success'){
            alertify.success(response.alertify.mensagem);
            if(response.reload){
              setTimeout(function(){ location.reload(); }, 2000);
            }
            if(response.redirect){
                setTimeout(function(){ location.href=response.redirect; }, 2000);
            }
          }else{
            alertify.error(response.alertify.mensagem);
          }
        }
      }
    });
  });

</script>
