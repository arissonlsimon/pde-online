<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html mlns="http://www.w3.org/1999/xhtml" lang="pt_BR">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>PdE</title>

		<link type="image/x-icon" rel="icon" href="<?php echo public_url; ?>/imagens/logoi.png">

	    <!-- Bootstrap Core CSS -->
	    <link href="<?php echo public_url; ?>/frameworks/bootstrap/css/bootstrap.css" rel="stylesheet">

	    <!-- MetisMenu CSS -->
	    <link href="<?php echo public_url; ?>/frameworks/metisMenu/metisMenu.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="<?php echo public_url; ?>/frameworks/dist/css/sb-admin-2.css" rel="stylesheet">

	    <!-- Morris Charts CSS -->
	    <link href="<?php echo public_url; ?>/frameworks/morrisjs/morris.css" rel="stylesheet">

	    <!-- Custom Fonts -->
	    <link href="<?php echo public_url; ?>/frameworks/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<!-- Alertify -->
		<link rel="stylesheet" type="text/css" href="<?php echo public_url; ?>/frameworks/alertifyjs/css/alertify.css">
		<link rel="stylesheet" type="text/css" href="<?php echo public_url; ?>/frameworks/alertifyjs/css/themes/default.css">

		<!-- Tokeninput-jq -->
		<link rel="stylesheet" type="text/css" href="<?php echo public_url; ?>/frameworks/jquery-tokeninput/styles/token-input-facebook.css">

		<!-- jQuery -->
	    <script src="<?php echo public_url; ?>/frameworks/jquery/jquery.min.js"></script>
			<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 

  	<!-- Bootstrap Core JavaScript -->
	    <script src="<?php echo public_url; ?>/frameworks/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
	    <script src="<?php echo public_url; ?>/frameworks/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
	    <script src="<?php echo public_url; ?>/frameworks/raphael/raphael.min.js"></script>
	    <script src="<?php echo public_url; ?>/frameworks/morrisjs/morris.min.js"></script>
	    <!-- <script src="<?php echo public_url; ?>/frameworks/data/morris-data.js"></script> -->

    <!-- Custom Theme JavaScript -->
	    <script src="<?php echo public_url; ?>/frameworks/dist/js/sb-admin-2.js"></script>

		<script type="text/javascript" charset="utf-8" src="<?php echo public_url; ?>/frameworks/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" charset="utf-8" src="<?php echo public_url; ?>/frameworks/ckeditor/styles.js"></script>
		<script type="text/javascript" charset="utf-8" src="<?php echo public_url; ?>/frameworks/alertifyjs/alertify.js"></script>

		<!-- JQ-Tokeninput -->
		<script type="text/javascript" charset="utf-8" src="<?php echo public_url; ?>/frameworks/jquery-tokeninput/src/jquery.tokeninput.js"></script>

		<!-- Custom JavaScript functions -->
		<script src="<?php echo public_url; ?>/javascript/custom.js"></script>
		<script type="text/javascript">
			var base_url = "<?php echo base_url(); ?>";
		</script>

		<style type="text/css">
			.glyphicon.fast-right-spinner {
		    -webkit-animation: glyphicon-spin-r 1s infinite linear;
		    animation: glyphicon-spin-r 1s infinite linear;
			}
			@-webkit-keyframes glyphicon-spin-r {
		    0% {
		        -webkit-transform: rotate(0deg);
		        transform: rotate(0deg);
		    }

		    100% {
		        -webkit-transform: rotate(359deg);
		        transform: rotate(359deg);
		    }
			}
		</style>
	</head>
	<body>
<!-- MODAL DE LOADING -->
	<div class="modal fade" id="modalLoading" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false" style="padding-top: 15%;">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	      <div class="modal-content" style="text-align: center; background-color: rgba(0,0,0,0);-webkit-box-shadow: 0 5px 15px rgba(0,0,0,0);-moz-box-shadow: 0 5px 15px rgba(0,0,0,0);-o-box-shadow: 0 5px 15px rgba(0,0,0,0);box-shadow: 0 5px 15px rgba(0,0,0,0); border: none;">
          <span style="font-size: 28px; color: white;" class="glyphicon glyphicon-repeat fast-right-spinner"></span>
        </div>
	  </div>
	</div>
<!-- MODAL DE LOADING END -->