

<!-- MODAL PARA LOGIN -->
    <div class="modal fade" id="modalLogin" role="dialog" aria-labelledby="modalLogin" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="row">
              <div class="col-sm-3">
                <h5 class="modal-title">LOGIN</h5>
              </div>
              <div class="col-sm-9 pull-right">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            </div>
          </div>
          <form >
          <div class="modal-body">
              <div class="form-group">
                <label for="email" class="col-form-label">Email:</label>
                <input type="email" name="email"class="form-control" id="email" required>
              </div>
              <div class="form-group">
                <label for="password" class="col-form-label">Senha:</label>
                <input type="password" name="senha"class="form-control" id="senha">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              <button type="button" class="btn btn-primary" id="entrar">Entrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
<!-- MODAL LOGIN END -->

<!-- MODAL PARA TROCA DE SENHA -->
    <div class="modal fade" id="modalPass" role="dialog" aria-labelledby="modalLogin" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="row">
              <div class="col-sm-5">
                <h5 class="modal-title">TROCA DE SENHA</h5>
              </div>
              <div class="col-sm-7 pull-right">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

            </div>
          </div>
          <div class="modal-body">
            <form >
              <div class="form-group">
                <label for="email" class="col-form-label">Senha padrão:</label>
                <input type="password" name="senhaOld"class="form-control" id="senhaOld" required>
              </div>
              <div class="form-group">
                <label for="password" class="col-form-label">Nova senha:</label>
                <input type="password" name="senhaNew"class="form-control" id="senhaNew">
              </div>
              <div class="form-group">
                <label for="password" class="col-form-label">Repita a nova senha:</label>
                <input type="password" name="rsenhaNew"class="form-control" id="rsenhaNew">
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-primary" onclick="redefinir()">Redefinir</button>
          </div>
        </div>
      </div>
    </div>
<!-- MODAL TROCA DE SENHA END -->
