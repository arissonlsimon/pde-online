<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="wrapper">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Bem-vindo!!</h1>
        </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->

      <div id="body">
        <p>Olá!!</p>
        <p>Seja bem vindo ao sistema de Plano de Ensino.</p>
        <p>Nesse sistema poderá verificar todos os planos de ensino das disciplinas do Campus IFRS OSÓRIO.</p>
        <p>Utilize o menu lateral para navegar pelo site.</p>
      </div>

      <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds.</p>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->