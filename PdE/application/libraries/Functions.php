<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Functions {

	public function __construct() {}


  function pre($array, $exit = false) {
		echo '<pre>';
		print_r($array);
		echo '</pre>';
		if ($exit) exit;
	}

  function logs($title = null, $content = null, $folder = "logs/") {
		if($title == true) $title = " ".$title;
	    $Security_Logs = fopen($_SERVER['DOCUMENT_ROOT'].'/avaliacoes/'.APPPATH.$folder.date('d-m-Y').$title.".txt", "a");
		@fwrite($Security_Logs, date('d/m/Y H:i:s')." | ".$content."\r\n");
		@fclose($Security_Logs);
	}

  function checkPermissao($regras = null, $usuario = null) {

		if(!empty($regras) && !empty($usuario)) {

			if(is_array($regras)) {

				foreach ($regras as $key => $regra) {
					if(is_numeric($regra)) {
						if(@array_key_exists($regra, $usuario)) {
							return true;
						}
					} else {
						if(in_array($regra, $usuario) ){
							return true;
						}
					}
				}
			} else {
				// if(is_numeric($regras)) {
				// 	if(@array_key_exists($regras, $usuario['usuario']['permissoes'])) {
				// 		return true;
				// 	}
				// } else {
				// 	if(@array_search($regras, $usuario['usuario']['permissoes'])) {
				// 		return true;
				// 	}
				// }
			}
		}
		return false;
	}

  function enviaEmail($email, $nome, $msg, $titulo = "[CNEC Avaliações] ") {

		require_once BASEPATH.'libraries/email/mailer/class.phpmailer.php';

	    // Inicia a classe PHPMailer
	    $mail = new PHPMailer();
	    $mail -> IsSMTP();                              // Define que a mensagem sera SMTP
	    $mail -> Host         = "smtp.gmail.com";       // Enderea§o do servidor SMTP
	    $mail -> SetLanguage("br");
	    $mail -> Port         = 587;
	    $mail -> SMTPSecure   = "tls";                  // sets the prefix to the servier
	    $mail -> SMTPAuth     = true;                   // Usa autenticacao SMTP? (opcional)
	    $mail -> Username     = "1905.noreply@cnec.br"; // SMTP username
	    $mail -> Password     = "n0-r3ply";             // SMTP password

	    // Define o remetente
	    $mail -> Sender   = "1905.noreply@cnec.br";
	    $mail -> From     = "1905.noreply@cnec.br";
	    $mail -> FromName = "1905.noreply@cnec.br";

	    // Define os destinata¡rio(s)
	    $mail -> AddAddress($email, $nome);

	    // Define os dados ta©cnicos da Mensagem
	    $mail -> IsHTML(true); // Define que o e-mail sera¡ enviado como HTML

	    // Define a mensagem (Texto e Assunto)
	    $mail -> Subject  = utf8_decode($titulo); // Assunto da mensagem
	    $mail -> MsgHTML($msg);

	    // Envia o e-mail
	    $enviado = $mail->Send();

	    // Limpa os destinata¡rios e os anexos
	    $mail -> ClearAllRecipients();
	    $mail -> ClearAttachments();

	    if ($enviado) {
	        return true;
	    } else {
	        return false;
	        //return "Nao foi possa­vel enviar o e-mail.<br /><br /><b>Inforcoes do erro:</b> <br />" . $mail->ErrorInfo;
	    }

	}

	function trducaoMes($mes){
		$arIng = array('01','02','03','04','05','06','07','08','09','10','11','12');
		$arPtBr = array('Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Des');
		$key = array_search($mes, $arIng);
		return $arPtBr[$key];
	}

	function changeDayWeekInt($dia_semana){
		$arInt = array(1,2,3,4,5,6,7);
		$ar_string = array('segunda','terça','quarta','quinta','sexta','sabado','domingo');
		$key = array_search($dia_semana,$ar_string);
		return $arInt[$key];
	}

	function date_converter($_date = null) {
		$format = '/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/';
		if ($_date != null && preg_match($format, $_date, $partes)) {
			return $partes[3].'-'.$partes[2].'-'.$partes[1];
		}

		return false;
	}


}
?>
